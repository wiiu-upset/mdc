<?php

/**
 * @file
 * Contains \Mdc\Theme\Theme.
 */

namespace Mdc\Theme;

class Theme {

  /**
   * Gettext localization domain.
   *
   * @var string
   */
  const L10N = 'mdc';

  /**
   * @var array
   */
  private static $templateVariables = [];

  /**
   * @var array
   */
  private static $baseUrl;

  /**
   * @implements init
   */
  public static function init() {
    if (is_admin()) {
      // @see Admin::init()
      return;
    }

    add_action('wp_enqueue_scripts', __CLASS__ . '::enqueueAssets', 100);

    add_filter('gettext', __CLASS__ . '::gettext', 10, 3);
  }

  /**
   * Enqueues styles and scripts.
   *
   * @implements wp_enqueue_scripts
   */
  public static function enqueueAssets() {
    $git_version = NULL;
    if (is_dir(ABSPATH . '.git')) {
      $ref = trim(file_get_contents(ABSPATH . '.git/HEAD'));
      if (strpos($ref, 'ref:') === 0) {
        $ref = substr($ref, 5);
        if (file_exists(ABSPATH . '.git/' . $ref)) {
          $ref = trim(file_get_contents(ABSPATH . '.git/' . $ref));
        }
        else {
          $ref = substr($ref, 11);
        }
      }
      $git_version = substr($ref, 0, 8);
    }
    wp_enqueue_style('tm-robin-parent-style',  get_template_directory_uri() . '/style.css');

    wp_enqueue_style('tm-robin-child-style', static::getBaseUrl() . '/dist/styles/main.min.css', ['tm-robin-main-style'], $git_version);

    wp_enqueue_script('tm-robin-child-script', static::getBaseUrl() . '/dist/scripts/libs.min.js', ['jquery'], $git_version, TRUE);
    wp_enqueue_script('tm-robin-child-libs-script', static::getBaseUrl() . '/dist/scripts/main.min.js', ['jquery, libs'], $git_version, TRUE);
  }

  /**
   * Returns the base URL of this theme.
   *
   * @return string
   */
  public static function getBaseUrl() {
    if (!isset(self::$baseUrl)) {
      self::$baseUrl = get_stylesheet_directory_uri();
    }
    return self::$baseUrl;
  }

  /**
   * @implements gettext
   */
  public static function gettext($translation, $text, $domain) {
    if ($domain === 'insight-core') {
      if ($text === 'Never see this message again') {
        $translation = 'Non mostrare più questo messaggio';
      }
    }
    return $translation;
  }

  /**
   * The absolute filesystem base path of this theme.
   *
   * @return string
   */
  public static function getBasePath() {
    return dirname(__DIR__);
  }

}
