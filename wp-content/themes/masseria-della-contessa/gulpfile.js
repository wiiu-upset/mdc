var gulp = require('gulp')
var plugins = require('gulp-load-plugins')()

var pkg = require('./package.json')

var argv = require('minimist')(process.argv.slice(2))
var babel = require('gulp-babel')
var cleanCSS = require('gulp-clean-css')
var eslint = require('gulp-eslint')
var gulpif = require('gulp-if')
var runSequence = require('run-sequence')

var copyrightPlaceholder = '/*! #copyright DO NOT REMOVE# */'
var copyrightNotice = ['/*!',
  ' * Theme Name: ' + pkg.name,
  ' * Description: ' + pkg.description,
  ' * Version: ' + pkg.version,
  ' * Author: ' + pkg.author,
  ' * Author URI ' + pkg.homepage,
  ' */',
  ''].join('\n')

var paths = {
  src: './assets/',
  tmp: './.tmp/',
  dist: './dist/'
}

var options = {
  maps: !argv.production
}

// -----------------------------------------------------------------------------
//   Style pipeline
// -----------------------------------------------------------------------------
gulp.task('lint:sass', function () {
  return gulp.src(paths.src + 'styles/**/*.scss', {base: './'})
    .pipe(plugins.stylelint({
      syntax: 'scss',
      reporters: [{
        formatter: 'string',
        console: true
      }]
    }))
    .pipe(gulp.dest('./'))
})

gulp.task('compile:sass', function () {
  return gulp.src(paths.src + 'styles/**/*.scss')
    .pipe(plugins.sass({outputStyle: 'expanded'}).on('error', plugins.sass.logError))
    .pipe(plugins.autoprefixer(pkg.browserslist))
    .pipe(plugins.replace(copyrightPlaceholder, copyrightNotice))
    .pipe(gulp.dest(paths.tmp + 'styles/'))
})

gulp.task('minify:css', function () {
  return gulp.src(paths.tmp + 'styles/*.css')
    .pipe(gulpif(options.maps, plugins.sourcemaps.init()))
    .pipe(cleanCSS({format: { breaks: { afterComment: true } }}))
    .pipe(plugins.rename({suffix: '.min'}))
    .pipe(plugins.eol('\n'))
    .pipe(gulpif(options.maps, plugins.sourcemaps.write('.', {sourceRoot: paths.src + 'styles/'})))
    .pipe(gulp.dest(paths.dist + 'styles/'))
})

gulp.task('clean:styles', require('del').bind(null, [
  paths.tmp + 'styles/',
  paths.dist + 'styles/'
]))

gulp.task('build:wordpressCSSEntry', function () {
  return gulp.src(paths.src + 'style.css')
    .pipe(plugins.replace(copyrightPlaceholder, copyrightNotice))
    .pipe(gulp.dest('./'))
})

gulp.task('build:styles', ['clean:styles'], function (callback) {
  runSequence(
    'build:wordpressCSSEntry',
    'lint:sass',
    'compile:sass',
    'minify:css',
    callback)
})

// -----------------------------------------------------------------------------
//   Libs pipeline
// -----------------------------------------------------------------------------
gulp.task('libs:js', function () {
  return gulp.src(paths.src + 'scripts/libs.js')
    .pipe(plugins.browserify({
      insertGlobals: true
    }))
    .pipe(gulpif(options.maps, plugins.sourcemaps.init()))
    .pipe(plugins.uglify({
      preserveComments: 'license'
    }))
    .pipe(plugins.rename({suffix: '.min'}))
    .pipe(plugins.eol('\n'))
    .pipe(gulpif(options.maps, plugins.sourcemaps.write('.', {sourceRoot: paths.src + 'scripts/'})))
    .pipe(gulp.dest(paths.dist + 'scripts/'))
})

gulp.task('build:libs', function (callback) {
  runSequence(
    'libs:js',
    callback)
})

// -----------------------------------------------------------------------------
//   Script pipeline
// -----------------------------------------------------------------------------
gulp.task('lint:js', function () {
  return gulp.src(paths.src + 'scripts/**/*.js')
    .pipe(eslint())
    .pipe(eslint.format())
})

gulp.task('transpile:es2016', function () {
  return gulp.src([paths.src + 'scripts/**/*.js', '!' + paths.src + 'scripts/**/libs.js'])
    .pipe(babel())
    .pipe(gulp.dest(paths.tmp + 'scripts/'))
})

gulp.task('minify:js', function () {
  return gulp.src(paths.tmp + 'scripts/*.js')
    .pipe(gulpif(options.maps, plugins.sourcemaps.init()))
    .pipe(plugins.uglify({
      preserveComments: 'license'
    }))
    .pipe(plugins.rename({suffix: '.min'}))
    .pipe(plugins.eol('\n'))
    .pipe(gulpif(options.maps, plugins.sourcemaps.write('.', {sourceRoot: paths.src + 'scripts/'})))
    .pipe(gulp.dest(paths.dist + 'scripts/'))
})

gulp.task('clean:scripts', require('del').bind(null, [
  paths.tmp + 'scripts/',
  paths.dist + 'scripts/'
]))

gulp.task('build:scripts', function (callback) {
  runSequence(
    'lint:js',
    'transpile:es2016',
    'minify:js',
    callback)
})

// -----------------------------------------------------------------------------
//   Images pipeline
// -----------------------------------------------------------------------------
gulp.task('images', function () {
  return gulp.src(paths.src + 'images/**/*')
    .pipe(plugins.imagemin([
      plugins.imagemin.jpegtran({progressive: true}),
      plugins.imagemin.gifsicle({interlaced: true}),
      plugins.imagemin.svgo({plugins: [{removeUnknownsAndDefaults: false}, {cleanupIDs: false}]})
    ]))
    .pipe(gulp.dest(paths.dist + 'images/'))
})

// -----------------------------------------------------------------------------
//   Fonts pipeline
// -----------------------------------------------------------------------------
gulp.task('fonts', function () {
  return gulp.src(paths.src + 'fonts/**/*')
    .pipe(plugins.flatten())
    .pipe(gulp.dest(paths.dist + 'fonts/'))
})

// -----------------------------------------------------------------------------
//   Tasks
// -----------------------------------------------------------------------------
gulp.task('clean', require('del').bind(null, [
  paths.tmp,
  paths.dist
]))

gulp.task('default', function () {
  gulp.start('build')
})

gulp.task('build', ['clean'], function (callback) {
  runSequence(
    'build:libs',
    'build:styles',
    'build:scripts',
    ['images', 'fonts'],
    callback)
})

gulp.task('watch', function () {
  gulp.watch([paths.src + 'styles/**/*'], ['build:styles'])
  gulp.watch([paths.src + 'scripts/libs.js'], ['build:libs'])
  gulp.watch([paths.src + 'scripts/**/*.js'], ['build:scripts'])
  gulp.watch([paths.src + 'images/**/*'], ['images'])
  gulp.watch([paths.src + 'fonts/**/*'], ['fonts'])
})
