<?php

/**
 * Define constants
 *
 * @since 1.0
 */
$insight_theme = wp_get_theme();

if ( ! empty( $insight_theme['Template'] ) ) {
	$insight_theme = wp_get_theme( $insight_theme['Template'] );
}

if ( ! defined( 'DS' ) ) {
	define( 'DS', DIRECTORY_SEPARATOR );
}

define( 'TM_ROBIN_THEME_NAME', $insight_theme['Name'] );
define( 'TM_ROBIN_THEME_SLUG', $insight_theme['Template'] );
define( 'TM_ROBIN_THEME_VERSION', $insight_theme['Version'] );
define( 'TM_ROBIN_THEME_DIR', get_template_directory() );
define( 'TM_ROBIN_THEME_URI', get_template_directory_uri() );
define( 'TM_ROBIN_CHILD_THEME_URI', get_stylesheet_directory_uri() );
define( 'TM_ROBIN_CHILD_THEME_DIR', get_stylesheet_directory() );

define( 'TM_ROBIN_IMAGES', TM_ROBIN_THEME_URI . '/assets/images' );
define( 'TM_ROBIN_ADMIN_IMAGES', TM_ROBIN_THEME_URI . '/assets/admin/images' );

define( 'TM_ROBIN_INC_DIR', TM_ROBIN_THEME_DIR . DS . 'includes' );
define( 'TM_ROBIN_INC_URI', TM_ROBIN_THEME_URI . '/includes' );
define( 'TM_ROBIN_FRAMEWORK_DIR', TM_ROBIN_THEME_DIR . DS . 'framework' );
define( 'TM_ROBIN_FRAMEWORK_URI', TM_ROBIN_THEME_URI . '/framework' );

define( 'TM_ROBIN_OPTIONS_DIR', TM_ROBIN_INC_DIR . DS . 'theme-options' );

/**
 * Load Insight Framework.
 *
 * @since 1.0
 */
require_once( TM_ROBIN_FRAMEWORK_DIR . DS . 'class-compatible.php' );
require_once( TM_ROBIN_FRAMEWORK_DIR . DS . 'class-enqueue.php' );
require_once( TM_ROBIN_FRAMEWORK_DIR . DS . 'class-extras.php' );
require_once( TM_ROBIN_FRAMEWORK_DIR . DS . 'class-helper.php' );
require_once( TM_ROBIN_FRAMEWORK_DIR . DS . 'class-import.php' );
require_once( TM_ROBIN_FRAMEWORK_DIR . DS . 'class-init.php' );
require_once( TM_ROBIN_FRAMEWORK_DIR . DS . 'class-instagram.php' );
require_once( TM_ROBIN_FRAMEWORK_DIR . DS . 'class-metabox.php' );
require_once( TM_ROBIN_FRAMEWORK_DIR . DS . 'class-tgm-plugin-activation.php' );
require_once( TM_ROBIN_FRAMEWORK_DIR . DS . 'class-plugins.php' );
require_once( TM_ROBIN_FRAMEWORK_DIR . DS . 'class-post-types.php' );
require_once( TM_ROBIN_FRAMEWORK_DIR . DS . 'class-sercurity.php' );
require_once( TM_ROBIN_FRAMEWORK_DIR . DS . 'class-templates.php' );
require_once( TM_ROBIN_FRAMEWORK_DIR . DS . 'class-walker-nav-menu.php' );
require_once( TM_ROBIN_FRAMEWORK_DIR . DS . 'class-walker-nav-menu-edit.php' );

if ( class_exists( 'Redux' ) ) {
	require_once( TM_ROBIN_FRAMEWORK_DIR . DS . 'class-redux.php' );
}

if ( class_exists( 'WooCommerce' ) ) {
	require_once( TM_ROBIN_FRAMEWORK_DIR . DS . 'class-woo.php' );
}

if ( class_exists( 'WPBakeryVisualComposerAbstract' ) ) {
	require_once( TM_ROBIN_FRAMEWORK_DIR . DS . 'class-vc.php' );
}

/**
 * Widgets.
 *
 * @since 1.0
 */
require_once( TM_ROBIN_INC_DIR . DS . 'widgets' . DS . 'wph-widget-class.php' );
require_once( TM_ROBIN_INC_DIR . DS . 'widgets' . DS . 'widget-contact-info.php' );

if ( class_exists( 'WooCommerce' ) ) {
	require_once( TM_ROBIN_INC_DIR . DS . 'widgets' . DS . 'widget-layered-nav.php' );
}

/**
 * Get option from Theme Options
 */
if ( ! function_exists( 'tm_robin_get_option' ) ) {
	function tm_robin_get_option( $option ) {
		return TM_Robin_Helper::get_option( $option );
	}
}
