<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Robin
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>

	<?php echo TM_Robin_Templates::favicon(); ?>

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<?php echo TM_Robin_Templates::mobile_menu(); ?>

<?php do_action( 'tm_robin_site_before' ); ?>

<div id="page-container">

	<?php do_action( 'tm_robin_page_container_top' ); ?>


	<?php if ( ! TM_Robin_Helper::is_maintenance_page() ) { ?>

		<?php
		global $tm_robin_options;

		if ( tm_robin_get_option( 'topbar_on' ) ) {
			get_template_part( 'components/header/topbar' );
		} ?>

		<?php

		$header = apply_filters( 'tm_robin_header_layout', tm_robin_get_option( 'header' ) );

		$header_classes   = array( 'site-header' );
		$header_classes[] = 'header-' . $header;

		?>
		<!-- Header -->
		<header class="<?php echo implode( ' ', $header_classes ); ?>">
			<?php get_template_part( 'components/header/header-' . tm_robin_get_option( 'header' ) ); ?>
		</header>
		<!-- End Header -->

	<?php } ?>

	<?php
	$remove_whitespace = tm_robin_get_option( 'remove_whitespace' );

	$container_class = array( 'main-container' );

	if ( $remove_whitespace ) {
		$container_class[] = 'no-whitespace';
	}

	?>

	<div class="<?php echo implode( ' ', $container_class ); ?>">

		<?php
		do_action( 'tm_robin_main_container_top' );
		echo TM_Robin_Templates::page_title();
		?>
