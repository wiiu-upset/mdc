<?php

/**
 * Class Robin_Chosen
 *
 * @package Robin
 */

/* Example
array(
  'type'       => 'chosen',
  'heading'    => esc_html__( 'Categories', 'tm-robin' ),
  'param_name' => 'ajax',
	'options'     => array(
		'multiple'  => true, // multiple or not
		'type'      => 'taxonomy', // taxonomy or post_type
		'get'       => 'product_cat', // term or post type name, split by comma
		'field'     => 'slug', // slug or id
	),
  ),
*/

if ( ! class_exists( 'Robin_Chosen' ) ) {

	class Robin_Chosen {

		public function __construct() {

			if ( class_exists( 'WpbakeryShortcodeParams' ) ) {
				WpbakeryShortcodeParams::addField( 'chosen', array( $this, 'render' ) );
			}

			add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
		}

		public function admin_scripts() {
			wp_enqueue_style( 'tm-robin-chosen',
				TM_ROBIN_THEME_URI . '/includes/vc-extend/vc-params/robin-chosen/chosen.min.css' );
			wp_enqueue_script( 'tm-robin-chosen',
				TM_ROBIN_THEME_URI . '/includes/vc-extend/vc-params/robin-chosen/chosen.jquery.min.js',
				array( 'jquery' ),
				TM_ROBIN_THEME_VERSION,
				true );
		}

		public function render( $settings, $value ) {

			$param_name = isset( $settings['param_name'] ) ? $settings['param_name'] : '';
			$options    = isset( $settings['options'] ) ? $settings['options'] : array();
			$multiple   = isset( $options['multiple'] ) ? $options['multiple'] : true;
			$type       = isset( $options['type'] ) ? $options['type'] : 'post_type';
			$get        = isset( $options['get'] ) ? $options['get'] : 'post';
			$field      = isset( $options['field'] ) ? $options['field'] : 'id';
			$limit      = isset( $options['limit'] ) ? $options['limit'] : - 1;
			$post_arr   = array();

			if ( $type == 'post_type' ) {

				$params = array(
					'posts_per_page'      => $limit,
					'post_type'           => explode( ',', $get ),
					'ignore_sticky_posts' => 1,
				);

				$loop = new WP_Query( $params );
				if ( $loop->have_posts() ) {
					while ( $loop->have_posts() ) {
						$loop->the_post();
						$post_arr[] = array(
							'id'   => get_the_ID(),
							'name' => get_the_title(),
						);
					}
				}

				wp_reset_postdata();

			} elseif ( $type == 'taxonomy' ) {

				$terms = get_terms( array(
					'taxonomy'   => $get,
					'hide_empty' => false,
				) );
			}

			$id = uniqid( 'chosen-' );

			$output = '<div class="robin-chosen">';
			$output .= '<input name="' . $param_name . '" class="wpb_vc_param_value ' . $param_name . ' robin_chosen_items_' . $param_name . '" id="robin-chosen-items" type="hidden" value="' . $value . '" />';
			$output .= '<select name="' . $param_name . '"' . ' id="' . $id . '" class="wpb_vc_param_value wpb-select ' . $param_name . ' ' . $settings['type'] . '_field"' . ( $multiple ? ' multiple="multiple"' : '' ) . ' data-class="robin_chosen_items_' . $param_name . '">';

			$values = explode( ',', $value );

			if ( $type == 'post_type' ) {

				foreach ( $post_arr as $item ) {

					$selected = '';
					if ( is_array( $values ) && in_array( $item['id'], $values ) ) {
						$selected = 'selected';
					}

					$output .= '<option value="' . $item['id'] . '" ' . $selected . '>';
					$output .= $item['name'];
					$output .= '</option>';
				}

			} elseif ( $type == 'taxonomy' ) {

				foreach ( $terms as $term ) {

					$selected = '';

					if ( is_array( $values ) && ( ( $field == 'id' && in_array( $term->term_id,
									$values ) ) || ( $field == 'slug' && in_array( $term->slug, $values ) ) )
					) {
						$selected = 'selected';
					}

					$output .= '<option value="' . ( $field == 'id' ? $term->term_id : $term->slug ) . '" ' . $selected . '>';
					$output .= $term->name . ' (' . $term->count . ')';
					$output .= '</option>';

				}
			}

			$output .= '</select>';
			$output .= '</div>';
			$output .= '<script type="text/javascript">jQuery(document).ready(function() {' . 'jQuery("#' . $id . '").chosen();' . 'jQuery("#' . $id . '").on(\'change\', function(){' . 'var element_target="." + jQuery(this).attr(\'data-class\');' . 'jQuery(element_target).val(jQuery(this).val())' . '});' . '});</script>';

			return $output;
		}
	}

	new Robin_Chosen();
}
