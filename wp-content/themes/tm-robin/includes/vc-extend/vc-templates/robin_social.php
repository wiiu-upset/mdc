<?php
/**
 * Shortcode attributes
 *
 * @var $icon_color
 * @var $icon_shape
 * @var $icon_colorful
 * @var $icon_title_on
 * @var $link_new_page
 * @var $social_links
 * @var $atts
 * @var $el_class
 * @var $css
 * Shortcode class
 * @var $this WPBakeryShortCode_Robin_Social
 */

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$el_class  = $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'tm-shortcode tm-robin-social ' . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );

$social_links_arr = $this->getSocialLinks( $atts );
$social_icon_arr  = TM_Robin_Helper::social_icons( false, true );

if ( 'none' != $icon_shape ) {
	$css_class .= ' shape-' . $icon_shape;
}

if ( 'yes' == $icon_colorful ) {
	$css_class .= ' icon-colorful';
}

$css_id = uniqid( 'tm-robin-social-' );
$this->shortcode_css( $css_id );

?>
<div class="<?php echo esc_attr( $css_class ); ?>" id="<?php echo esc_attr( $css_id ); ?>">
	<?php
	if ( ! empty( $social_links_arr ) ) { ?>
		<ul class="social-links">
			<?php foreach ( $social_links_arr as $key => $link ) {
				$social = $social_icon_arr[ $key ];
				?>
				<li>
					<a href="<?php echo esc_url( $link ); ?>"
					   target="<?php echo esc_attr( $link_new_page == 'yes' ? '_blank' : '_self' ); ?>">
						<i class="fa fa-<?php echo esc_attr( $key ); ?>"
							<?php if ( 'yes' == $icon_colorful && 'none' != $icon_shape ) { ?> style="background-color:<?php echo esc_attr( $social['color'] );?>"<?php } ?>
							<?php if ( 'yes' == $icon_colorful && 'none' == $icon_shape ) { ?> style="color:<?php echo esc_attr( $social['color'] );?>"<?php } ?>>
							<?php if ('none' != $icon_shape ) { ?>
								<span class="hover-mask"<?php if ( 'yes' == $icon_colorful && 'none' != $icon_shape ) { ?> style="background-color:<?php echo esc_attr( $social['color'] );?>"<?php } ?>></span>
							<?php } ?>
						</i>
						<?php if ( 'yes' == $icon_title_on ) { ?>
							<span class="icon-title"><?php echo esc_html( $social['label'] ); ?></span>
						<?php } ?>
					</a>
				</li>
			<?php } ?>
		</ul>
	<?php } ?>
</div>
