<?php
/**
 * Shortcode attributes
 *
 * @var $atts
 * @var $source
 * @var $image
 * @var $custom_src
 * @var $img_size
 * @var $external_img_size
 * @var $first_line
 * @var $second_line
 * @var $third_line
 * @var $animation
 * @var $banner_link
 * @var $button_text
 * @var $button_style
 * @var $button_color
 * @var $button_color_hover
 * @var $button_bg_color
 * @var $button_bg_color_hover
 * @var $button_border_color
 * @var $button_border_color_hover
 * @var $el_class
 * @var $css
 * Shortcode class
 * @var $this WPBakeryShortCode_Robin_Simple_Banner
 */

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$el_class  = $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'tm-shortcode tm-robin-simple-banner' . ' ' . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );

$css_class .= $this->getCSSAnimation( $animation );

$img = false;

// parse banner link
if ( ! empty( $banner_link ) && "||" !== $banner_link && "|||" !== $banner_link ) {
	$banner_link = vc_build_link( $banner_link );

	$banner_href   = $banner_link['url'];
	$banner_title  = $banner_link['title'];
	$banner_target = $banner_link['target'];
	$banner_rel    = $banner_link['rel'];

	if ( $banner_title && ! $button_text ) {
		$button_text = $banner_title;
	}
}

// Get banner image
switch ( $source ) {
	case 'media_library':

		if ( ! $img_size ) {
			$img_size = 'full';
		}

		if ( $image ) {
			$dimensions = vcExtractDimensions( $img_size );
			$hwstring   = $dimensions ? image_hwstring( $dimensions[0], $dimensions[1] ) : '';

			$img = array(
				'thumbnail' => '<img ' . $hwstring . ' src="' . wp_get_attachment_image_src( $image, $img_size )[0] . '" alt="" />',
				'src'       => wp_get_attachment_image_src( $image, $img_size )[0],
			);
		}

		break;

	case 'external_link':


		$dimensions = vcExtractDimensions( $external_img_size );
		$hwstring   = $dimensions ? image_hwstring( $dimensions[0], $dimensions[1] ) : '';

		$custom_src = $custom_src ? esc_attr( $custom_src ) : $default_src;

		$img = array(
			'thumbnail' => '<img ' . $hwstring . 'src="' . $custom_src . '" alt="" />',
			'src'       => $custom_src,
		);

		break;

	default:
		break;
}

$css_id        = uniqid( 'tm-simple-banner-' );
$shortcode_css = $this->shortcode_css( $css_id );

?>

<div class="<?php echo esc_attr( $css_class ); ?>" id="<?php echo esc_attr( $css_id ); ?>">
	<?php if ( ! empty( $banner_link ) && "||" !== $banner_link ) { ?>
		<a class="banner-link" href="<?php echo esc_url( $banner_href ); ?>"
		   title="<?php echo esc_attr( $banner_title ); ?>"
		   target="<?php echo esc_attr( $banner_target ); ?>"
           rel="<?php echo esc_attr( $banner_rel ); ?>"
		><?php echo esc_attr( $banner_title ); ?></a>
	<?php }
	echo '' . $img['thumbnail'];
	?>

	<div class="banner-content">
		<div class="first-line"><?php echo '' . $first_line; ?></div>
		<div class="second-line"><?php echo '' . $second_line; ?></div>
		<div class="third-line"><?php echo '' . $third_line; ?></div>
		<?php if ( ! empty( $banner_link ) && "||" !== $banner_link ) { ?>
			<a class="tm-robin-button banner-button <?php echo esc_attr( $button_style ); ?>"
			   href="<?php echo esc_url( $banner_href ); ?>"><?php echo '' . $button_text; ?></a>
		<?php } else { ?>
			<span class="tm-robin-button banner-button <?php echo esc_attr( $button_style ); ?>"><?php echo '' . $button_text; ?></span>
		<?php } ?>
	</div>

</div>
