<?php
/**
 * Shortcode attributes
 *
 * @var $atts
 * @var $username
 * @var $number_items
 * @var $item_width_md
 * @var $show_follow_text
 * @var $spacing
 * @var $show_likes_comments
 * @var $link_new_page
 * @var $square_media
 * @var $el_class
 * @var $css
 *
 * Shortcode class
 * @var $this WPBakeryShortCode_Robin_Instagram
 */

$atts   = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$el_class = $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'tm-shortcode tm-robin-instagram '  . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );

$class = 'col-md-' . $item_width_md . ' col-sm-6 col-xs-6';

?>

<div class="tm-instagram container <?php echo esc_attr( $css_class ); ?>">
	<?php if ( ! empty( $username ) ) {

		$media_array = TM_Robin_Instagram::scrape_instagram( $username, $number_items, $square_media );

		if ( is_wp_error( $media_array ) ) { ?>
			<div class="tm-instagram--error"><p><?php echo esc_attr( $media_array->get_error_message() ); ?></p></div>
		<?php } else { ?>
			<ul class="tm-instagram-pics row">
				<?php foreach ( $media_array as $item ) { ?>
					<li class="item<?php echo ' ' . $class; ?>"
					    style="padding: 0 <?php echo esc_attr( $spacing ); ?>px <?php echo esc_attr( $spacing ); ?>px 0;">
						<?php if ( 1 == $show_likes_comments ) { ?>
							<div class="item-info"
							    style="width:calc(100% - <?php echo esc_attr( $spacing ) . 'px'; ?>);">
								<div class="likes">
									<a href="<?php echo esc_url( $item['link'] ) ?>"
									   target="<?php echo esc_attr( $link_new_page == 1 ? '_blank' : '_self' ); ?>"
									   title="<?php $item['description']; ?>"><span><?php echo esc_attr( $item['likes'] ); ?>
									</a></span></div>
								<div class="comments">
									<a href="<?php echo esc_url( $item['link'] ) ?>"
									   target="<?php echo esc_attr( $link_new_page == 1 ? '_blank' : '_self' ); ?>"
									   title="<?php $item['description']; ?>"><span><?php echo esc_attr( $item['comments'] ); ?>
									</a></span></div>
							</div>
						<?php } ?>

						<img src="<?php echo esc_url( $item['thumbnail'] ); ?>"
						     alt="" class="item-image"/>
						<?php if ( 'video' == $item['type'] ) { ?>
							<span class="play-button"></span>
						<?php } ?>

						<div class="overlay"
						     style="margin: 0 <?php echo esc_attr( $spacing ); ?>px <?php echo esc_attr( $spacing ); ?>px 0;">
							<a href="<?php echo esc_url( $item['link'] ) ?>"
							   target="<?php echo esc_attr( $link_new_page == 1 ? '_blank' : '_self' ); ?>">See on
								Instagram</a>
						</div>

					</li>
				<?php } ?>
			</ul>
		<?php } ?>
		<?php if ( 1 == $show_follow_text ) { ?>
			<div class="tm-instagram-follow-links">
				<?php echo $content; ?>
			</div>
		<?php }
	} ?>
</div>
