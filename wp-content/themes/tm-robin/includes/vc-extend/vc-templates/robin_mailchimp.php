<?php

/**
 * Shortcode attributes
 *
 * @var $atts
 * @var $title
 * @var $form_id
 * @var $title_color
 * @var $text_color
 * @var $button_text_color
 * @var $button_text_color_hover
 * @var $button_bg_color
 * @var $button_bg_color_hover
 * @var $button_border_color
 * @var $button_border_color_hover
 * @var $el_class
 * @var $css
 * @var $css_id
 *
 * Shortcode class
 * @var $this WPBakeryShortCode_Robin_MailChimp
 */
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$align_class = $align_center ? 'text-xs-center' : '';
$el_class    = $this->getExtraClass( $el_class );
$css_class   = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'tm-shortcode tm-robin-mailchimp newsletter ' . $align_class . ' ' . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );

$mc_forms = mc4wp_get_forms();

if ( $form_id === '' ) {
	$mc_forms = mc4wp_get_forms();
	if ( count( $mc_forms ) > 0 ) {
		$form_id = $mc_forms[0]->ID;
	}
}

$css_id        = uniqid( 'tm-robin-mailchimp-' );
$shortcode_css = $this->shortcode_css( $css_id );

?>

<div class="<?php echo esc_attr( $css_class ) ?>" id="<?php echo esc_attr( $css_id ); ?>">
	<?php if ( $title ) : ?>
		<h3 class="title"><?php echo '' . $title; ?></h3>
	<?php endif; ?>
	<?php if ( $form_id ) {
		mc4wp_show_form( $form_id );
	} ?>
</div>
