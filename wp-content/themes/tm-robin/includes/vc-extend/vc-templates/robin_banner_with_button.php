<?php
/**
 * Shortcode attributes
 *
 * @var $atts
 * @var $source
 * @var $image
 * @var $custom_src
 * @var $img_size
 * @var $external_img_size
 * @var $first_line
 * @var $second_line
 * @var $description
 * @var $alignment
 * @var $use_theme_fonts
 * @var $google_fonts_1
 * @var $google_fonts_2
 * @var $font_size_1
 * @var $line_height_1
 * @var $font_size_2
 * @var $line_height_2
 * @var $hover_style
 * @var $animation
 * @var $text_animation
 * @var $color_first_line
 * @var $color_second_line
 * @var $color_description
 * @var $button_text
 * @var $button_link
 * @var $button_style
 * @var $button_color
 * @var $button_color_hover
 * @var $button_bg_color
 * @var $button_bg_color_hover
 * @var $button_border_color
 * @var $button_border_color_hover
 * @var $el_class
 * @var $css
 * Shortcode class
 * @var $this WPBakeryShortCode_Robin_Banner_With_Button
 */
extract( $this->getAttributes( $atts ) );

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

extract( $this->getStyles( $el_class, $css, $google_fonts_data, $atts ) );

$settings = get_option( 'wpb_js_google_fonts_subsets' );
if ( is_array( $settings ) && ! empty( $settings ) ) {
	$subsets = '&subset=' . implode( ',', $settings );
} else {
	$subsets = '';
}

if ( isset( $google_fonts_data['values']['font_family'] ) ) {
	wp_enqueue_style( 'vc_google_fonts_' . vc_build_safe_css_class( $google_fonts_data['values']['font_family'] ), '//fonts.googleapis.com/css?family=' . $google_fonts_data['values']['font_family'] . $subsets );
}

if ( ! empty( $styles ) ) {
	$style = 'style="' . esc_attr( implode( ';', $styles ) ) . '"';
} else {
	$style = '';
}

// Get text style for the second line
extract( $this->getAttributes( $atts, 'second' ) );
extract( $this->getStyles( $el_class, $css, $google_fonts_data_2, $atts, 'second' ) );

if ( isset( $google_fonts_data_2['values']['font_family'] ) ) {
	wp_enqueue_style( 'vc_google_fonts_' . vc_build_safe_css_class( $google_fonts_data_2['values']['font_family'] ), '//fonts.googleapis.com/css?family=' . $google_fonts_data_2['values']['font_family'] . $subsets );
}

if ( ! empty( $styles ) ) {
	$style_2 = 'style="' . esc_attr( implode( ';', $styles ) ) . '"';
} else {
	$style_2 = '';
}

$img = false;

// parse button link
if ( ! empty( $button_link ) && "||" !== $button_link && "|||" !== $button_link ) {
	$button_link = vc_build_link( $button_link );

	$a_href   = $button_link['url'];
	$a_title  = $button_link['title'];
	$a_target = $button_link['target'];
	$a_rel    = $button_link['rel'];

	if ( $a_title && ! $button_text ) {
		$button_text = $a_title;
	}
}

// Get banner image
switch ( $source ) {
	case 'media_library':

		if ( ! $img_size ) {
			$img_size = 'full';
		}

		if ( $image ) {
			$dimensions = vcExtractDimensions( $img_size );
			$hwstring   = $dimensions ? image_hwstring( $dimensions[0], $dimensions[1] ) : '';

			$img = array(
				'thumbnail' => '<img ' . $hwstring . ' src="' . wp_get_attachment_image_src( $image, $img_size )[0] . '" alt="" />',
				'src'       => wp_get_attachment_image_src( $image, $img_size )[0],
			);
		}

		break;

	case 'external_link':


		$dimensions = vcExtractDimensions( $external_img_size );
		$hwstring   = $dimensions ? image_hwstring( $dimensions[0], $dimensions[1] ) : '';

		$custom_src = $custom_src ? esc_attr( $custom_src ) : $default_src;

		$img = array(
			'thumbnail' => '<img ' . $hwstring . 'src="' . $custom_src . '" alt="" />',
			'src'       => $custom_src,
		);

		break;

	default:
		break;
}

$banner_content_class = $this->getCSSAnimation( $text_animation );
$banner_content_class .= ( $alignment ) ? ' ' . $alignment : ' center';

$css_id        = uniqid( 'tm-robin-banner-with-button-' );
$shortcode_css = $this->shortcode_css( $css_id );

?>

<div class="<?php echo esc_attr( $css_class ); ?>" id="<?php echo esc_attr( $css_id ); ?>">

	<?php echo '' . $img['thumbnail']; ?>
	<div class="banner-content">
		<div class="<?php echo esc_attr( ' ' . $banner_content_class ); ?>">
			<div class="first-line" <?php echo '' . $style; ?>><?php echo '' . $first_line; ?></div>
			<div class="second-line" <?php echo '' . $style_2; ?>><?php echo '' . $second_line; ?></div>
			<div class="description"><?php echo '' . $description; ?></div>
			<?php if ( ! empty( $button_link ) && "||" !== $button_link ) { ?>
				<a class="tm-robin-button banner-button <?php echo esc_attr( $button_style ); ?>"
				   href="<?php echo esc_url( $a_href ); ?>"
				   title="<?php echo esc_attr( $a_title ); ?>" <?php echo esc_attr( $a_target ? 'target="' . $a_target . '"' : '' ); ?>
				   rel="<?php echo esc_attr( $a_rel ); ?>"><?php echo '' . $button_text; ?></a>
			<?php } else { ?>
				<span class="tm-robin-button banner-button <?php echo esc_attr( $button_style ); ?>"><?php echo '' . $button_text; ?></span>
			<?php } ?>
		</div>
	</div>

</div>
