<?php

/**
 * Social Links Shortcode
 *
 * @version 1.0
 * @package Robin
 */
class WPBakeryShortCode_Robin_Social extends WPBakeryShortCode {

	public function getSocialLinks( $atts ) {
		$social_links     = preg_split( '/\s+/', $atts['social_links'] );
		$social_links_arr = array();

		foreach ( $social_links as $social ) {
			$pieces = explode( '|', $social );
			if ( count( $pieces ) == 2 ) {
				$key                      = $pieces[0];
				$link                     = $pieces[1];
				$social_links_arr[ $key ] = $link;
			}
		}

		return $social_links_arr;
	}

	public function shortcode_css( $css_id ) {

		$atts = vc_map_get_attributes( $this->getShortcode(), $this->getAtts() );
		$css  = '';
		$css_id = '#' . $css_id;

		$css .= $css_id . ' li,' . $css_id . ' i' . '{font-size:' . $atts['icon_font_size'] . 'px}';
		$css .= $css_id . ' i{color:' . $atts['icon_color'];

		if ( 'yes' != $atts['icon_colorful'] ) {
			$css .= ';background-color:' . $atts['icon_bgcolor'];
		}

		$css .= '}';

		$css = TM_Robin_Helper::text2line( $css );

		global $tm_robin_shortcode_css;
		$tm_robin_shortcode_css .= $css;
	}
}

vc_map( array(
	        'name'                    => esc_html__( 'Social Links', 'tm-robin' ),
	        'base'                    => 'robin_social',
	        'icon'                    => 'robin-icon-social',
	        'show_settings_on_create' => false,
	        'category'                => sprintf( esc_html__( 'by %s', 'tm-robin' ), TM_ROBIN_THEME_NAME ),
	        'params'                  => array(
				array(
			        'type'       => 'number',
			        'param_name' => 'icon_font_size',
			        'heading'    => esc_html__( 'Icon font size', 'tm-robin' ),
			        'value'      => 24,
			        'min'        => 10,
			        'max'        => 50,
			        'step'       => 1,
			        'suffix'     => 'px',
		        ),
				array(
			        'type'        => 'dropdown',
			        'param_name'  => 'icon_shape',
			        'heading'     => esc_html__( 'Icon background shape', 'tm-robin' ),
			        'value'       => array(
				        esc_html__( 'None', 'tm-robin' )   => 'none',
				        esc_html__( 'Circle', 'tm-robin' ) => 'circle',
				        esc_html__( 'Square', 'tm-robin' ) => 'square',
			        ),
			        'std'         => 'circle',
			        'description' => esc_html__( 'Select background shape and style for icons', 'tm-robin' ),
		        ),
				array(
			        'type'       => 'checkbox',
			        'param_name' => 'icon_colorful',
			        'value'      => array( esc_html__( 'Colorful icons', 'tm-robin' ) => 'yes' ),
			        'std'        => 'yes',
		        ),
				array(
			        'type'       => 'colorpicker',
			        'param_name' => 'icon_color',
			        'heading'    => esc_html__( 'Icon color', 'tm-robin' ),
			        'value'      => '#ffffff',
		        ),
				array(
			        'type'       => 'colorpicker',
			        'param_name' => 'icon_bgcolor',
			        'heading'    => esc_html__( 'Icon background color', 'tm-robin' ),
			        'value'      => '#d5d5d5',
			        'dependency' => array(
				        'element'            => 'icon_colorful',
				        'value_not_equal_to' => 'yes',
			        ),
		        ),
				array(
			        'type'       => 'checkbox',
			        'param_name' => 'icon_title_on',
			        'value'      => array( esc_html__( 'Show the social network\'s name', 'tm-robin' ) => 'yes' ),
		        ),
				array(
			        'group'      => esc_html__( 'Social', 'tm-robin' ),
			        'type'       => 'checkbox',
			        'param_name' => 'link_new_page',
			        'value'      => array( esc_html__( 'Open links in new tab', 'tm-robin' ) => 'yes' ),
		        ),
				array(
			        'group'      => esc_html__( 'Social', 'tm-robin' ),
			        'type'       => 'social_links',
			        'heading'    => esc_html__( 'Social links', 'tm-robin' ),
			        'param_name' => 'social_links',
		        ),
				TM_Robin_VC::get_param( 'el_class' ),
				TM_Robin_VC::get_param( 'css' ),
	        ),
        )
);
