<?php

/**
 * Robin MailChimp Maps Shortcode
 *
 * @version 1.0
 * @package Robin
 */
class WPBakeryShortCode_Robin_MailChimp extends WPBakeryShortCode {

	public function shortcode_css( $css_id ) {

		$atts   = vc_map_get_attributes( $this->getShortcode(), $this->getAtts() );
		$css_id = '#' . $css_id;

		$title_color = $atts['title_color'];
		$text_color  = $atts['text_color'];

		$input_border_color       = $atts['input_border_color'];
		$input_bg_color           = $atts['input_bg_color'];
		$input_border_color_focus = $atts['input_border_color_focus'];

		$button_text_color         = $atts['button_text_color'];
		$button_text_color_hover   = $atts['button_text_color_hover'];
		$button_bg_color           = $atts['button_bg_color'];
		$button_bg_color_hover     = $atts['button_bg_color_hover'];
		$button_border_color       = $atts['button_border_color'];
		$button_border_color_hover = $atts['button_border_color_hover'];

		$css = '';

		$css .= $css_id . ' .title{color:' . $title_color . ';}';

		$css .= $css_id . ' input[type="text"],' . $css_id . ' input[type="email"]{color:' . $text_color . '}';
		$css .= $css_id . ' input[type="text"],' . $css_id . ' input[type="email"]{border-color:' . $input_border_color . '}';
		$css .= $css_id . ' input[type="text"],' . $css_id . ' input[type="email"]{background-color:' . $input_bg_color . '}';
		$css .= $css_id . ' input[type="text"]:focus, ' . $css_id . ' input[type="email"]:focus{border-color:' . $input_border_color_focus . '}';

		$css .= $css_id . ' button[type="submit"]{';
		$css .= 'color:' . $button_text_color . ';';
		$css .= 'background-color:' . $button_bg_color . ';';
		$css .= 'border-color:' . $button_border_color;
		$css .= '}';

		$css .= $css_id . ' button[type="submit"]:hover{';
		$css .= 'color:' . $button_text_color_hover . ';';
		$css .= 'background-color:' . $button_bg_color_hover . ';';
		$css .= 'border-color:' . $button_border_color_hover;
		$css .= '}';

		global $tm_robin_shortcode_css;
		$tm_robin_shortcode_css .= $css;
	}
}

// Mapping shortcode.
vc_map( array(
	        'name'        => esc_html__( 'MailChimp for WP', 'tm-robin' ),
	        'base'        => 'robin_mailchimp',
	        'icon'        => 'robin-icon-mailchimp',
	        'category'    => sprintf( esc_html__( 'by %s', 'tm-robin' ), TM_ROBIN_THEME_NAME ),
	        'description' => esc_html__( 'Displays your MailChimp for WordPress sign-up form', 'tm-robin' ),
	        'params'      => array(
				array(
			        'type'        => 'textfield',
			        'heading'     => esc_html__( 'Title', 'tm-robin' ),
			        'admin_label' => true,
			        'param_name'  => 'title',
			        'value'       => 'Newsletter',
			        'description' => sprintf( wp_kses( __( 'You can edit your sign-up form in the <a href="%s" target="_blank">MailChimp for WordPress form settings</a>.', 'tm-robin' ),
			                                           array(
				                                           'a' => array(
					                                           'href'   => array(),
					                                           'target' => array(),
				                                           ),
			                                           ) ), admin_url( 'admin.php?page=mailchimp-for-wp-forms' )
			        ),
		        ),
		        array(
			        'heading'     => esc_html__( 'Form Id', 'tm-robin' ),
			        'description' => esc_html__( 'Input the id of form. Leave blank to show default form.', 'tm-robin' ),
			        'type'        => 'textfield',
			        'param_name'  => 'form_id',
		        ),
				array(
			        'type'       => 'checkbox',
			        'param_name' => 'align_center',
			        'value'      => array( esc_html__( 'Align Center', 'tm-robin' ) => 'yes' ),
		        ),
				array(
			        'group'      => esc_html__( 'Color', 'tm-robin' ),
			        'type'       => 'colorpicker',
			        'heading'    => esc_html__( 'Title color', 'tm-robin' ),
			        'param_name' => 'title_color',
			        'value'      => '#202020',
		        ),
				array(
			        'group'      => esc_html__( 'Color', 'tm-robin' ),
			        'type'       => 'colorpicker',
			        'heading'    => esc_html__( 'Text color', 'tm-robin' ),
			        'param_name' => 'text_color',
			        'value'      => '#202020',
		        ),
				array(
			        'group'      => esc_html__( 'Color', 'tm-robin' ),
			        'type'       => 'colorpicker',
			        'heading'    => esc_html__( 'Input border color', 'tm-robin' ),
			        'param_name' => 'input_border_color',
			        'value'      => '#ccc',
		        ),
				array(
			        'group'      => esc_html__( 'Color', 'tm-robin' ),
			        'type'       => 'colorpicker',
			        'heading'    => esc_html__( 'Input border color on focus', 'tm-robin' ),
			        'param_name' => 'input_border_color_focus',
			        'value'      => tm_robin_get_option( 'secondary_color' ),
		        ),
				array(
			        'group'      => esc_html__( 'Color', 'tm-robin' ),
			        'type'       => 'colorpicker',
			        'heading'    => esc_html__( 'Input background color', 'tm-robin' ),
			        'param_name' => 'input_bg_color',
			        'value'      => '#fff',
		        ),
				array(
			        'group'      => esc_html__( 'Color', 'tm-robin' ),
			        'type'       => 'colorpicker',
			        'heading'    => esc_html__( 'Button text color', 'tm-robin' ),
			        'param_name' => 'button_text_color',
			        'value'      => '#fff',
		        ),
				array(
			        'group'      => esc_html__( 'Color', 'tm-robin' ),
			        'type'       => 'colorpicker',
			        'heading'    => esc_html__( 'Button text color (on hover)', 'tm-robin' ),
			        'param_name' => 'button_text_color_hover',
			        'value'      => '#fff',
		        ),
				array(
			        'group'      => esc_html__( 'Color', 'tm-robin' ),
			        'type'       => 'colorpicker',
			        'heading'    => esc_html__( 'Button border color', 'tm-robin' ),
			        'param_name' => 'button_border_color',
			        'value'      => tm_robin_get_option( 'primary_color' ),
		        ),
				array(
			        'group'      => esc_html__( 'Color', 'tm-robin' ),
			        'type'       => 'colorpicker',
			        'heading'    => esc_html__( 'Button border color (on hover)', 'tm-robin' ),
			        'param_name' => 'button_border_color_hover',
			        'value'      => tm_robin_get_option( 'secondary_color' ),
		        ),
				array(
			        'group'      => esc_html__( 'Color', 'tm-robin' ),
			        'type'       => 'colorpicker',
			        'heading'    => esc_html__( 'Button background color', 'tm-robin' ),
			        'param_name' => 'button_bg_color',
			        'value'      => tm_robin_get_option( 'primary_color' ),
		        ),
				array(
			        'group'      => esc_html__( 'Color', 'tm-robin' ),
			        'type'       => 'colorpicker',
			        'heading'    => esc_html__( 'Button background color (on hover)', 'tm-robin' ),
			        'param_name' => 'button_bg_color_hover',
			        'value'      => tm_robin_get_option( 'secondary_color' ),
		        ),
				TM_Robin_VC::get_param( 'el_class' ),
				TM_Robin_VC::get_param( 'css' ),
	        ),
        ) );
