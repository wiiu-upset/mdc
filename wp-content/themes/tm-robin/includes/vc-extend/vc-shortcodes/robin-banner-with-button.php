<?php

/**
 * ThemeMove Banner With Button Shortcode
 *
 * @version 1.0
 * @package Robin
 */
class WPBakeryShortCode_Robin_Banner_With_Button extends WPBakeryShortCode {

	public function shortcode_css( $css_id ) {

		$atts  = vc_map_get_attributes( $this->getShortcode(), $this->getAtts() );
		$cssID = '#' . $css_id;

		$color_first_line  = $atts['color_first_line'] ? $atts['color_first_line'] : PRIMARY_COLOR;
		$color_second_line = $atts['color_second_line'] ? $atts['color_second_line'] : SECONDARY_COLOR;
		$color_description = $atts['color_description'] ? $atts['color_description'] : SECONDARY_COLOR;

		$button_style        = $atts['button_style'] ? $atts['button_style'] : '';
		$button_color        = $atts['button_color'] ? $atts['button_color'] : 'transparent';
		$button_bg_color     = $atts['button_bg_color'] ? $atts['button_bg_color'] : 'transparent';
		$button_border_color = $atts['button_border_color'] ? $atts['button_border_color'] : 'transparent';

		$button_color_hover        = $atts['button_color_hover'] ? $atts['button_color_hover'] : 'transparent';
		$button_bg_color_hover     = $atts['button_bg_color_hover'] ? $atts['button_bg_color_hover'] : 'transparent';
		$button_border_color_hover = $atts['button_border_color_hover'] ? $atts['button_border_color_hover'] : 'transparent';


		$css = '';

		$css .= $cssID . ' .first-line{color:' . $color_first_line . '}';
		$css .= $cssID . ' .second-line{color:' . $color_second_line . '}';
		$css .= $cssID . ' .description{color:' . $color_description . '}';

		if ( $button_style == 'custom' ) {
			$css .= $cssID . ' .banner-button{color:' . $button_color . ';background-color:' . $button_bg_color . ';border-color:' . $button_border_color . ';}';
			$css .= $cssID . ' .banner-button:hover{color:' . $button_color_hover . ';background-color:' . $button_bg_color_hover . ';border-color:' . $button_border_color_hover . ';}';
		}

		global $tm_robin_shortcode_css;
		$tm_robin_shortcode_css .= $css;
	}

	/**
	 * Defines fields names for google_fonts, font_container and etc
	 *
	 * @since 4.4
	 * @var array
	 */
	protected $fields = array(
		'google_fonts_1' => 'google_fonts_1',
		'google_fonts_2' => 'google_fonts_2',
		'el_class'       => 'el_class',
		'css'            => 'css',
		'text'           => 'text',
	);

	/**
	 * Parses shortcode attributes and set defaults based on vc_map function relative to shortcode and fields names
	 *
	 * @param $atts
	 *
	 * @since 4.3
	 * @return array
	 */
	public function getAttributes( $atts, $line = 'first' ) {
		/**
		 * Shortcode attributes
		 *
		 * @var $google_fonts_1
		 * @var $google_fonts_2
		 * @var $el_class
		 * @var $css
		 */
		$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
		extract( $atts );

		/**
		 * Get default values from VC_MAP.
		 **/
		if ( $line == 'first' ) {
			$google_fonts_field = $this->getParamData( 'google_fonts_1' );
		} else {
			$google_fonts_field = $this->getParamData( 'google_fonts_2' );
		}

		$el_class                    = $this->getExtraClass( $el_class );
		$google_fonts_obj            = new Vc_Google_Fonts();
		$google_fonts_field_settings = isset( $google_fonts_field['settings'], $google_fonts_field['settings']['fields'] ) ? $google_fonts_field['settings']['fields'] : array();

		$results = array(
			'google_fonts' => $google_fonts_1,
			'el_class'     => $el_class,
			'css'          => $css,
		);

		if ( $line == 'first' ) {
			$google_fonts_data            = strlen( $google_fonts_1 ) > 0 ? $google_fonts_obj->_vc_google_fonts_parse_attributes( $google_fonts_field_settings, $google_fonts_1 ) : '';
			$results['google_fonts_data'] = $google_fonts_data;
		} else {
			$google_fonts_data              = strlen( $google_fonts_2 ) > 0 ? $google_fonts_obj->_vc_google_fonts_parse_attributes( $google_fonts_field_settings, $google_fonts_2 ) : '';
			$results['google_fonts_data_2'] = $google_fonts_data;
		}

		return $results;
	}

	/**
	 * Get param value by providing key
	 *
	 * @param $key
	 *
	 * @since 4.4
	 * @return array|bool
	 */
	protected function getParamData( $key ) {
		return WPBMap::getParam( $this->shortcode, $this->getField( $key ) );
	}

	/**
	 * Used to get field name in vc_map function for google_fonts, font_container and etc..
	 *
	 * @param $key
	 *
	 * @since 4.4
	 * @return bool
	 */
	protected function getField( $key ) {
		return isset( $this->fields[ $key ] ) ? $this->fields[ $key ] : false;
	}

	/**
	 * Parses google_fonts_data to get needed css styles to markup
	 *
	 * @param $el_class
	 * @param $css
	 * @param $google_fonts_data
	 * @param $atts
	 *
	 * @since 4.3
	 * @return array
	 */
	public function getStyles( $el_class, $css, $google_fonts_data, $atts, $line = 'first' ) {
		$styles = array();

		if ( ( ! isset( $atts['use_theme_fonts'] ) || 'yes' !== $atts['use_theme_fonts'] )
		     && ! empty( $google_fonts_data )
		     && isset( $google_fonts_data['values'], $google_fonts_data['values']['font_family'], $google_fonts_data['values']['font_style'] )
		) {
			$google_fonts_family = explode( ':', $google_fonts_data['values']['font_family'] );
			$styles[]            = 'font-family:' . $google_fonts_family[0];
			$google_fonts_styles = explode( ':', $google_fonts_data['values']['font_style'] );
			$styles[]            = 'font-weight:' . $google_fonts_styles[1];
			$styles[]            = 'font-style:' . $google_fonts_styles[2];
		}

		if ( isset( $atts['font_size_1'] ) && $line == 'first' ) {
			$styles[] = 'font-size:' . $atts['font_size_1'] . 'px;';
		} elseif ( isset( $atts['font_size_2'] ) && $line == 'second' ) {
			$styles[] = 'font-size:' . $atts['font_size_2'] . 'px;';
		}

		if ( isset( $atts['line_height_1'] ) && $line == 'first' ) {
			$styles[] = 'line-height:' . $atts['line_height_1'];
		} elseif ( isset( $atts['line_height_2'] ) && $line == 'second' ) {
			$styles[] = 'line-height:' . $atts['line_height_2'];
		}

		$results = array(
			'styles' => $styles
		);

		if ( $line == 'first' ) {
			$css_class            = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'tm-shortcode tm-robin-banner-with-button' . $this->getCSSAnimation( $atts['animation'] ) . ' hover-' . $atts['hover_style'] . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
			$results['css_class'] = trim( preg_replace( '/\s+/', ' ', $css_class ) );
		}

		return $results;
	}

	/**
	 *
	 * Custom title in backend, show image instead of icon
	 *
	 * @param $param
	 * @param $value
	 *
	 * @return string
	 */
	public function singleParamHtmlHolder( $param, $value ) {

		$output = '';

		$param_name = isset( $param['param_name'] ) ? $param['param_name'] : '';
		$type       = isset( $param['type'] ) ? $param['type'] : '';
		$class      = isset( $param['class'] ) ? $param['class'] : '';

		if ( 'attach_image' === $param['type'] && 'image' === $param_name ) {
			$output       .= '<input type="hidden" class="wpb_vc_param_value ' . $param_name . ' ' . $type . ' ' . $class . '" name="' . $param_name . '" value="' . $value . '" />';
			$element_icon = $this->settings( 'icon' );
			$img          = wpb_getImageBySize( array(
				'attach_id'  => (int) preg_replace( '/[^\d]/', '', $value ),
				'thumb_size' => 'thumbnail',
				'class'      => 'attachment-thumbnail vc_general vc_element-icon tm-element-icon-none',
			) );
			$this->setSettings( 'logo', ( $img ? $img['thumbnail'] : '<img width="150" height="150" src="' . vc_asset_url( 'vc/blank.gif' ) . '" class="attachment-thumbnail vc_general vc_element-icon tm-element-icon-banner1"  data-name="' . $param_name . '" alt="" title="" style="display: none;" />' ) . '<span class="no_image_image vc_element-icon' . ( ! empty( $element_icon ) ? ' ' . $element_icon : '' ) . ( $img && ! empty( $img['p_img_large'][0] ) ? ' image-exists' : '' ) . '" /><a href="#" class="column_edit_trigger' . ( $img && ! empty( $img['p_img_large'][0] ) ? ' image-exists' : '' ) . '">' . __( 'Add image', 'tm-robin' ) . '</a>' );
			$output .= $this->outputCustomTitle( $this->settings['name'] );
		} elseif ( ! empty( $param['holder'] ) ) {
			if ( 'input' === $param['holder'] ) {
				$output .= '<' . $param['holder'] . ' readonly="true" class="wpb_vc_param_value ' . $param_name . ' ' . $type . ' ' . $class . '" name="' . $param_name . '" value="' . $value . '">';
			} elseif ( in_array( $param['holder'], array(
				'img',
				'iframe',
			) ) ) {
				$output .= '<' . $param['holder'] . ' class="wpb_vc_param_value ' . $param_name . ' ' . $type . ' ' . $class . '" name="' . $param_name . '" src="' . $value . '">';
			} elseif ( 'hidden' !== $param['holder'] ) {
				$output .= '<' . $param['holder'] . ' class="wpb_vc_param_value ' . $param_name . ' ' . $type . ' ' . $class . '" name="' . $param_name . '">' . $value . '</' . $param['holder'] . '>';
			}
		}

		if ( ! empty( $param['admin_label'] ) && true === $param['admin_label'] ) {
			$output .= '<span class="vc_admin_label admin_label_' . $param['param_name'] . ( empty( $value ) ? ' hidden-label' : '' ) . '"><label>' . $param['heading'] . '</label>: ' . $value . '</span>';
		}

		return $output;
	}

	protected function outputTitle( $title ) {
		return '';
	}

	protected function outputCustomTitle( $title ) {
		return '<h4 class="wpb_element_title">' . $title . ' ' . $this->settings( 'logo' ) . '</h4>';
	}
}

vc_map( array(
	'name'     => esc_html__( 'Banner with Button', 'tm-robin' ),
	'base'     => 'robin_banner_with_button',
	'icon'     => 'robin-icon-banner-with-button',
	'category' => sprintf( esc_html__( 'by %s', 'tm-robin' ), TM_ROBIN_THEME_NAME ),
	'params'   => array(

		// General
		array(
			'type'        => 'dropdown',
			'heading'     => __( 'Image source', 'tm-robin' ),
			'param_name'  => 'source',
			'value'       => array(
				esc_html__( 'Media library', 'tm-robin' ) => 'media_library',
				esc_html__( 'External link', 'tm-robin' ) => 'external_link',
			),
			'std'         => 'media_library',
			'description' => esc_html__( 'Select image source.', 'tm-robin' ),
		),
		array(
			'type'        => 'attach_image',
			'heading'     => esc_html__( 'Banner image', 'tm-robin' ),
			'param_name'  => 'image',
			'value'       => '',
			'description' => esc_html__( 'Select an image from media library.', 'tm-robin' ),
			'dependency'  => array(
				'element' => 'source',
				'value'   => 'media_library',
			),
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'External link', 'tm-robin' ),
			'param_name'  => 'custom_src',
			'description' => esc_html__( 'Select external link.', 'tm-robin' ),
			'dependency'  => array(
				'element' => 'source',
				'value'   => 'external_link',
			),
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Image size (Optional)', 'tm-robin' ),
			'param_name'  => 'img_size',
			'value'       => 'full',
			'description' => esc_html__( 'Enter image size (Example: "thumbnail", "medium", "large", "full" or other sizes defined by theme). Alternatively enter size in pixels (Example: 200x100 (Width x Height)).', 'tm-robin' ),
			'dependency'  => array(
				'element' => 'source',
				'value'   => array( 'media_library' ),
			),
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Image size (Optional)', 'tm-robin' ),
			'param_name'  => 'external_img_size',
			'value'       => '',
			'description' => esc_html__( 'Enter image size in pixels. Example: 200x100 (Width x Height).', 'tm-robin' ),
			'dependency'  => array(
				'element' => 'source',
				'value'   => 'external_link',
			),
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'First line', 'tm-robin' ),
			'param_name'  => 'first_line',
			'description' => esc_html__( 'Enter text for the first line', 'tm-robin' ),
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Second line', 'tm-robin' ),
			'param_name'  => 'second_line',
			'description' => esc_html__( 'Enter text for the second line', 'tm-robin' ),
		),
		array(
			'type'        => 'textarea',
			'heading'     => esc_html__( 'Description', 'tm-robin' ),
			'param_name'  => 'description',
			'description' => esc_html__( 'Enter text for the description', 'tm-robin' ),
		),
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Text alignment', 'tm-robin' ),
			'description' => esc_html__( 'Select the alignment for text of this banner.', 'tm-robin' ),
			'param_name'  => 'alignment',
			'value'       => array(
				esc_html__( 'Left', 'tm-robin' )   => 'left',
				esc_html__( 'Center', 'tm-robin' ) => 'center',
				esc_html__( 'Right', 'tm-robin' )  => 'right',
			),
			'std'         => 'center',
		),
		TM_Robin_VC::get_param( 'el_class' ),

		// Font
		array(
			'group'      => esc_html__( 'Font', 'tm-robin' ),
			'type'       => 'checkbox',
			'heading'    => esc_html__( 'Use theme default font family for the first and second line?', 'tm-robin' ),
			'param_name' => 'use_theme_fonts',
			'value'      => array( esc_html__( 'Yes', 'tm-robin' ) => 'yes' ),
			'std'        => 'yes',
		),
		array(
			'group'      => esc_html__( 'Font', 'tm-robin' ),
			'type'       => 'number',
			'heading'    => esc_html__( 'Font Size of the FIRST line', 'tm-robin' ),
			'param_name' => 'font_size_1',
			'value'      => 24,
			'min'        => 16,
			'max'        => 50,
			'step'       => 1,
			'suffix'     => 'px',
		),
		array(
			'group'      => esc_html__( 'Font', 'tm-robin' ),
			'type'       => 'number',
			'heading'    => esc_html__( 'Line height of the FIRST line', 'tm-robin' ),
			'param_name' => 'line_height_1',
			'value'      => 1.5,
			'min'        => 1,
			'max'        => 2,
			'step'       => .1,
			'suffix'     => '',
		),
		array(
			'group'      => esc_html__( 'Font', 'tm-robin' ),
			'heading'    => esc_html__( 'CUSTOM FONT FOR THE FIRST LINE', 'tm-robin' ),
			'type'       => 'google_fonts',
			'param_name' => 'google_fonts_1',
			'value'      => 'font_family:' . rawurlencode( 'Playfair Display:regular,italic,700,700italic,900,900italic' ) . '|font_style:' . rawurlencode( '700 bold italic:700:italic' ),
			'settings'   => array(
				'fields' => array(
					'font_family_description' => esc_html__( 'Select font family.', 'tm-robin' ),
					'font_style_description'  => esc_html__( 'Select font styling.', 'tm-robin' ),
				),
			),
			'dependency' => array(
				'element'            => 'use_theme_fonts',
				'value_not_equal_to' => 'yes',
			),
		),
		array(
			'group'      => esc_html__( 'Font', 'tm-robin' ),
			'type'       => 'number',
			'heading'    => esc_html__( 'Font Size of the SECOND line', 'tm-robin' ),
			'param_name' => 'font_size_2',
			'value'      => 36,
			'min'        => 16,
			'max'        => 60,
			'step'       => 1,
			'suffix'     => 'px',
		),
		array(
			'group'      => esc_html__( 'Font', 'tm-robin' ),
			'type'       => 'number',
			'heading'    => esc_html__( 'Line height of the SECOND line', 'tm-robin' ),
			'param_name' => 'line_height_2',
			'value'      => 1,
			'min'        => 1,
			'max'        => 2,
			'step'       => .1,
			'suffix'     => '',
		),
		array(
			'group'      => esc_html__( 'Font', 'tm-robin' ),
			'heading'    => esc_html__( 'CUSTOM FONT FOR THE SECOND LINE', 'tm-robin' ),
			'type'       => 'google_fonts',
			'param_name' => 'google_fonts_2',
			'value'      => 'font_family:' . rawurlencode( 'Playfair Display:regular,italic,700,700italic,900,900italic' ) . '|font_style:' . rawurlencode( '700 bold italic:700:italic' ),
			'settings'   => array(
				'fields' => array(
					'font_family_description' => esc_html__( 'Select font family.', 'tm-robin' ),
					'font_style_description'  => esc_html__( 'Select font styling.', 'tm-robin' ),
				),
			),
			'dependency' => array(
				'element'            => 'use_theme_fonts',
				'value_not_equal_to' => 'yes',
			),
		),

		// Button
		array(
			'group'      => esc_html__( 'Button', 'tm-robin' ),
			'type'       => 'textfield',
			'heading'    => esc_html__( 'Button Text', 'tm-robin' ),
			'param_name' => 'button_text',
		),
		array(
			'group'      => esc_html__( 'Button', 'tm-robin' ),
			'type'       => 'vc_link',
			'heading'    => esc_html__( 'Button URL (Link)', 'tm-robin' ),
			'param_name' => 'button_link',
		),
		array(
			'group'       => esc_html__( 'Button', 'tm-robin' ),
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Button Style', 'tm-robin' ),
			'param_name'  => 'button_style',
			'value'       => array(
				esc_html__( 'Default', 'tm-robin' )     => '',
				esc_html__( 'Alternative', 'tm-robin' ) => 'alt',
				esc_html__( 'Custom', 'tm-robin' )      => 'custom',
			),
			'description' => esc_html__( 'Select button style.', 'tm-robin' ),
		),
		array(
			'group'      => esc_html__( 'Button', 'tm-robin' ),
			'type'       => 'colorpicker',
			'heading'    => esc_html__( 'Background color', 'tm-robin' ),
			'param_name' => 'button_bg_color',
			'value'      => tm_robin_get_option( 'secondary_color' ),
			'dependency' => array(
				'element' => 'button_style',
				'value'   => 'custom',
			),
		),
		array(
			'group'      => esc_html__( 'Button', 'tm-robin' ),
			'type'       => 'colorpicker',
			'heading'    => esc_html__( 'Background color (on hover)', 'tm-robin' ),
			'param_name' => 'button_bg_color_hover',
			'value'      => tm_robin_get_option( 'primary_color' ),
			'dependency' => array(
				'element' => 'button_style',
				'value'   => 'custom',
			),
		),
		array(
			'group'      => esc_html__( 'Button', 'tm-robin' ),
			'type'       => 'colorpicker',
			'heading'    => esc_html__( 'Text color', 'tm-robin' ),
			'param_name' => 'button_color',
			'value'      => '#fff',
			'dependency' => array(
				'element' => 'button_style',
				'value'   => 'custom',
			),
		),
		array(
			'group'      => esc_html__( 'Button', 'tm-robin' ),
			'type'       => 'colorpicker',
			'heading'    => esc_html__( 'Text color (on hover)', 'tm-robin' ),
			'param_name' => 'button_color_hover',
			'value'      => '#fff',
			'dependency' => array(
				'element' => 'button_style',
				'value'   => 'custom',
			),
		),
		array(
			'group'      => esc_html__( 'Button', 'tm-robin' ),
			'type'       => 'colorpicker',
			'heading'    => esc_html__( 'Border color', 'tm-robin' ),
			'param_name' => 'button_border_color',
			'value'      => tm_robin_get_option( 'secondary_color' ),
			'dependency' => array(
				'element' => 'button_style',
				'value'   => 'custom',
			),
		),
		array(
			'group'      => esc_html__( 'Button', 'tm-robin' ),
			'type'       => 'colorpicker',
			'heading'    => esc_html__( 'Border color (on hover)', 'tm-robin' ),
			'param_name' => 'button_border_color_hover',
			'value'      => tm_robin_get_option( 'primary_color' ),
			'dependency' => array(
				'element' => 'button_style',
				'value'   => 'custom',
			),
		),

		// Color
		array(
			'group'       => esc_html__( 'Color', 'tm-robin' ),
			'type'        => 'colorpicker',
			'heading'     => esc_html__( 'First Line', 'tm-robin' ),
			'description' => esc_html__( 'Select color for the first line of this banner', 'tm-robin' ),
			'param_name'  => 'color_first_line',
			'value'       => tm_robin_get_option( 'primary_color' ),
		),
		array(
			'group'       => esc_html__( 'Color', 'tm-robin' ),
			'type'        => 'colorpicker',
			'heading'     => esc_html__( 'Second Line', 'tm-robin' ),
			'description' => esc_html__( 'Select color for the second line of this banner', 'tm-robin' ),
			'param_name'  => 'color_second_line',
			'value'       => tm_robin_get_option( 'secondary_color' ),
		),
		array(
			'group'       => esc_html__( 'Color', 'tm-robin' ),
			'type'        => 'colorpicker',
			'heading'     => esc_html__( 'Description', 'tm-robin' ),
			'description' => esc_html__( 'Select color for the description text of this banner', 'tm-robin' ),
			'param_name'  => 'color_description',
			'value'       => tm_robin_get_option( 'secondary_color' ),
		),

		// Animation
		array(
			'group'       => esc_html__( 'Animation', 'tm-robin' ),
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Banner hover effect', 'tm-robin' ),
			'admin_label' => true,
			'param_name'  => 'hover_style',
			'value'       => array(
				esc_html__( 'none', 'tm-robin' )                    => '',
				esc_html__( 'Style 1 - Zoom in', 'tm-robin' )       => 'zoom-in',
				esc_html__( 'Style 2 - Blur', 'tm-robin' )          => 'blur',
				esc_html__( 'Style 3 - Gray scale', 'tm-robin' )    => 'grayscale',
				esc_html__( 'Style 4 - White Overlay', 'tm-robin' ) => 'white-overlay',
				esc_html__( 'Style 5 - Black Overlay', 'tm-robin' ) => 'black-overlay',
			),
			'std'         => 'zoom-in',
			'description' => esc_html__( 'Select animation style for banner when mouse over. Note: Some styles only work in modern browsers', 'tm-robin' ),
		),
		array(
			'group'      => esc_html__( 'Animation', 'tm-robin' ),
			'type'       => 'animation_style',
			'heading'    => esc_html__( 'Banner Animation', 'tm-robin' ),
			'param_name' => 'animation',
			'settings'   => array(
				'type' => array( 'in', 'other' )
			),
		),
		array(
			'group'      => esc_html__( 'Animation', 'tm-robin' ),
			'type'       => 'animation_style',
			'heading'    => esc_html__( 'Text animation', 'tm-robin' ),
			'param_name' => 'text_animation',
			'settings'   => array(
				'type' => array( 'in', 'other' )
			),
		),

		TM_Robin_VC::get_param( 'css' ),
	),
) );
