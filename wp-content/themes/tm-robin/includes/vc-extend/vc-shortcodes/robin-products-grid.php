<?php

/**
 * Robin Product Grid Shortcode
 *
 * @version 1.0
 * @package Robin
 */
class WPBakeryShortCode_Robin_Products_Grid extends WPBakeryShortCode {

	public function __construct( $settings ) {
		parent::__construct( $settings );
		add_filter( 'tm_robin_shop_products_columns', array( $this, 'product_columns' ) );
		add_filter( 'tm_robin_single_product_archive_thumbnail_size', array( $this, 'image_size' ) );
	}

	public function product_columns() {

		$atts = $this->getAtts();

		return array(
			'xs' => 2,
			'sm' => 2,
			'md' => 3,
			'lg' => 3,
			'xl' => $atts['columns'],
		);
	}

	public function image_size() {

		$atts = $this->getAtts();

		if ( empty( $atts['img_size'] ) ) {
			$atts['img_size'] = 'shop_catalog';
		}

		return isset( $atts['img_size'] ) ? TM_Robin_Helper::convert_image_size( $atts['img_size'] ) : 'shop_catalog';
	}
}

// Mapping shortcode.
vc_map( array(
	'name'        => esc_html__( 'Product Grid', 'tm-robin' ),
	'base'        => 'robin_products_grid',
	'icon'        => 'robin-icon-products-grid',
	'category'    => sprintf( esc_html__( 'by %s', 'tm-robin' ), TM_ROBIN_THEME_NAME ),
	'description' => esc_html__( 'Add products in a grid', 'tm-robin' ),
	'params'      => array(
		array(
			'type'        => 'dropdown',
			'param_name'  => 'data_source',
			'admin_label' => true,
			'heading'     => esc_html__( 'Data source', 'tm-robin' ),
			'value'       => array(
				esc_html__( 'Recent Products', 'tm-robin' )       => 'recent_products',
				esc_html__( 'Featured Products', 'tm-robin' )     => 'featured_products',
				esc_html__( 'On Sale Products', 'tm-robin' )      => 'sale_products',
				esc_html__( 'Best-Selling Products', 'tm-robin' ) => 'best_selling_products',
				esc_html__( 'Related Products', 'tm-robin' )      => 'related_products',
				esc_html__( 'Top Rated Products', 'tm-robin' )    => 'top_rated_products',
				esc_html__( 'Product Attribute', 'tm-robin' )     => 'product_attribute',
				esc_html__( 'List of Products', 'tm-robin' )      => 'products',
				esc_html__( 'Categories', 'tm-robin' )            => 'categories',
			),
			'description' => esc_html__( 'Select data source for your product grid', 'tm-robin' ),
		),
		TM_Robin_VC::get_param( 'product_cat_autocomplete', '', array(
			'element' => 'data_source',
			'value'   => array( 'categories' ),
		) ),
		TM_Robin_VC::get_param( 'product_autocomplete', '', array(
			'element' => 'data_source',
			'value'   => array( 'products' ),
		) ),
		TM_Robin_VC::get_param( 'product_attribute', '', array(
			'element' => 'data_source',
			'value'   => array( 'product_attribute' ),
		) ),
		TM_Robin_VC::get_param( 'product_term', '', array(
			'element' => 'data_source',
			'value'   => array( 'product_attribute' ),
		) ),
		array(
			'type'        => 'number',
			'param_name'  => 'number',
			'heading'     => esc_html__( 'Number', 'tm-robin' ),
			'description' => esc_html__( 'Number of products in the grid (-1 is all, limited to 1000)', 'tm-robin' ),
			'value'       => 12,
			'max'         => 1000,
			'min'         => - 1,
			'dependency'  => array( 'element' => 'data_source', 'value_not_equal_to' => array( 'products' ) ),
		),
		TM_Robin_VC::get_param( 'columns' ),
		array(
			'type'       => 'autocomplete',
			'heading'    => esc_html__( 'Exclude products', 'tm-robin' ),
			'param_name' => 'exclude',
			'settings'   => array(
				'multiple' => true,
				'sortable' => true,
			),
		),
		array(
			'type'        => 'checkbox',
			'param_name'  => 'disable_filter',
			'description' => esc_html__( 'Auto disable if it\'s inside the tabs, tours, accordion and FAQ elements.', 'tm-robin' ),
			'value'       => array( esc_html__( 'Disable Filter', 'tm-robin' ) => 'yes' ),
			'dependency'  => array( 'element' => 'data_source', 'value' => array( 'categories' ) ),
		),
		array(
			'type'        => 'checkbox',
			'param_name'  => 'include_children',
			'description' => esc_html__( 'Whether or not to include children categories', 'tm-robin' ),
			'value'       => array( esc_html__( 'Include children', 'tm-robin' ) => 'yes' ),
			'std'         => 'yes',
			'dependency'  => array( 'element' => 'data_source', 'value' => array( 'categories' ) ),
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Image size', 'tm-robin' ),
			'param_name'  => 'img_size',
			'value'       => 'shop_catalog',
			'description' => esc_html__( 'Enter image size . Example: "thumbnail", "medium", "large", "full" or other sizes defined by current theme . Alternatively enter image size in pixels: 200x100( Width x Height). Leave empty to use "shop_catalog" size . ', 'tm-robin' ),
		),
		array(
			'type'       => 'checkbox',
			'param_name' => 'show_load_more',
			'value'      => array( esc_html__( 'Show \'Load more\' button', 'tm-robin' ) => 'yes' ),
			'dependency' => array( 'element' => 'data_source', 'value_not_equal_to' => array( 'products' ) ),
		),
		TM_Robin_VC::get_param( 'el_class' ),
		// Data settings.
		TM_Robin_VC::get_param( 'order_product', esc_html__( 'Data Settings', 'tm-robin' ), array(
			'element'            => 'data_source',
			'value_not_equal_to' => array(
				'recent_products',
				'best_selling_products',
				'top_rated_products',
			),
		) ),
		TM_Robin_VC::get_param( 'order_way', esc_html__( 'Data Settings', 'tm-robin' ), array(
			'element'            => 'data_source',
			'value_not_equal_to' => array(
				'recent_products',
				'best_selling_products',
				'top_rated_products',
			),
		) ),
		TM_Robin_VC::get_param( 'css' )
	),
) );


//Filters For autocomplete param:
//For suggestion: vc_autocomplete_[shortcode_name]_[param_name]_callback
add_filter( 'vc_autocomplete_robin_products_grid_product_ids_callback', array(
	'TM_Robin_VC',
	'product_id_callback',
), 10, 1 );

add_filter( 'vc_autocomplete_robin_products_grid_product_ids_render', array(
	'TM_Robin_VC',
	'product_id_render',
), 10, 1 );

add_filter( 'vc_autocomplete_robin_products_grid_exclude_callback', array(
	'TM_Robin_VC',
	'product_id_callback',
), 10, 1 );

add_filter( 'vc_autocomplete_robin_products_grid_exclude_render', array(
	'TM_Robin_VC',
	'product_id_render',
), 10, 1 );

//For param: "filter" param value
//vc_form_fields_render_field_{shortcode_name}_{param_name}_param
add_filter( 'vc_form_fields_render_field_robin_products_grid_filter_param', array(
	'TM_Robin_VC',
	'product_attribute_filter_param_value',
), 10, 4 ); // Defines default value for param if not provided. Takes from other param value.
