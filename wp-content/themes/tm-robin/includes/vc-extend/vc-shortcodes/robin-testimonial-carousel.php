<?php

/**
 * Testimonial Carousel Shortcode
 *
 * @version 1.0
 * @package Robin
 */
class WPBakeryShortCode_Robin_Testimonial_Carousel extends WPBakeryShortCode {

}

// Mapping shortcode.
vc_map( array(
			'name'        => esc_html__( 'Testimonials Carousel', 'tm-robin' ),
			'base'        => 'robin_testimonial_carousel',
			'icon'        => 'robin-icon-testimonial-carousel',
			'category'    => sprintf( esc_html__( 'by %s', 'tm-robin' ), TM_ROBIN_THEME_NAME ),
			'description' => esc_html__( 'Show testimonials in a carousel', 'tm-robin' ),
			'params'      => array(
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Text Size', 'tm-robin' ),
					'param_name'  => 'text_size',
					'admin_label' => true,
					'value'       => array(
						esc_html__( 'Large', 'tm-robin' )    => 'large',
						esc_html__( 'Standard', 'tm-robin' ) => 'standard',
					),
				),
				array(
					'type'        => 'dropdown',
					'heading'     => __( 'Text Color', 'tm-robin' ),
					'param_name'  => 'text_color',
					'value'       => array(
						__( 'Dark', 'tm-robin' )  => 'dark',
						__( 'Light', 'tm-robin' ) => 'light',
					),
					'description' => __( 'Choose the text color of the testimonials.', 'tm-robin' ),
				),
				array(
					'type'        => 'number',
					'heading'     => esc_html__( 'Number of items', 'tm-robin' ),
					'param_name'  => 'item_count',
					'description' => esc_html__( 'The number of testimonials to show. Enter -1 to show ALL testimonials (limited to 1000)', 'tm-robin' ),
					'value'       => - 1,
					'max'         => 1000,
					'min'         => - 1,
					'step'        => 1,
				),
				array(
					'type'        => 'dropdown',
					'heading'     => __( 'Testimonials Order', 'tm-robin' ),
					'param_name'  => 'order',
					'value'       => array(
						__( 'Random', 'tm-robin' ) => 'rand',
						__( 'Latest', 'tm-robin' ) => 'date',
					),
					'description' => __( 'Choose the order of the testimonials.', 'tm-robin' ),
				),
				array(
					'type'        => 'dropdown',
					'heading'     => __( 'Testimonials category', 'tm-robin' ),
					'param_name'  => 'category',
					'value'       => TM_Robin_Helper::get_category_list( 'testimonials-category' ),
					'description' => __( 'Choose the category for the testimonials.', 'tm-robin' ),
				),

				array(
					'type'       => 'checkbox',
					'param_name' => 'loop',
					'value'      => array( esc_html__( 'Enable carousel loop mode', 'tm-robin' ) => 'yes' ),
					'std'        => 'yes',
				),
				array(
					'type'       => 'checkbox',
					'param_name' => 'auto_play',
					'value'      => array( esc_html__( 'Enable carousel autolay', 'tm-robin' ) => 'yes' ),
				),
				array(
					'type'       => 'number',
					'param_name' => 'auto_play_speed',
					'heading'    => esc_html__( 'Auto play speed', 'tm-robin' ),
					'value'      => 5,
					'max'        => 10,
					'min'        => 3,
					'step'       => 0.5,
					'suffix'     => 'seconds',
					'dependency' => array(
						'element' => 'auto_play',
						'value'   => 'yes',
					),
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'nav_type',
					'heading'    => esc_html__( 'Navigation type', 'tm-robin' ),
					'value'      => array(
						esc_html__( 'Arrows', 'tm-robin' ) => 'arrows',
						esc_html__( 'Dots', 'tm-robin' )   => 'dots',
						__( 'Arrows & Dots', 'tm-robin' )  => 'both',
						esc_html__( 'None', 'tm-robin' )   => '',
					),
				),

				TM_Robin_VC::get_param( 'el_class' ),
				TM_Robin_VC::get_param( 'css' ),
			),
		) );
