<?php

/**
 * Robin Blog Shortcode
 *
 * @version 1.0
 * @package Robin
 */
class WPBakeryShortCode_Robin_Blog extends WPBakeryShortCode {

	public $query = '';
	public $num_pages = 0;
	public $paged;
	public $offset = 0;


	public function get_query( $atts ) {

		$total_posts    = intval( $atts['total_posts'] ) > 0 ? intval( $atts['total_posts'] ) : 1000;
		$posts_per_page = intval( $atts['posts_per_page'] ) > 0 ? intval( $atts['posts_per_page'] ) : 5;

		if ( $total_posts > 0 && $posts_per_page > 0 && $posts_per_page > $total_posts ) {
			$posts_per_page = $total_posts;
		}

		if ( get_query_var( 'paged' ) ) {
			$this->paged = get_query_var( 'paged' );
		} else if ( get_query_var( 'page' ) ) {
			$this->paged = get_query_var( 'page' );
		} else {
			$this->paged = 1;
		}

		$orderby = $atts['orderby'];
		$order   = $atts['order'];

		$cat_slugs = $atts['cat_slugs'];
		$tag_slugs = $atts['tag_slugs'];

		$offset = $posts_per_page * ( $this->paged - 1 );
		$args   = array(
			'post_type'      => 'post',
			'offset'         => $offset,
			'post_count'     => $total_posts,
			'posts_per_page' => $posts_per_page,
			'orderby'        => $orderby,
			'order'          => $order,
		);

		if ( $cat_slugs ) {
			$args['category_name'] = $cat_slugs;
		}

		if ( $tag_slugs ) {
			$args['tag'] = $tag_slugs;
		}

		$this->query = new WP_Query( $args );

		$this->num_pages = intval( $atts['total_posts'] ) != - 1 ? ceil( $total_posts / $posts_per_page ) : $this->query->max_num_pages;
	}

	/***
	 * Display navigation to next/previous set of posts when applicable.
	 *
	 * @return array|string
	 */
	public function get_paging_nav() {
		global $wp_rewrite;

		// Don't print empty markup if there's only one page.
		if ( $this->num_pages < 2 ) {
			return '';
		}

		$pagenum_link = wp_kses_post( get_pagenum_link() );
		$query_args   = array();
		$url_parts    = explode( '?', $pagenum_link );

		if ( isset( $url_parts[1] ) ) {
			wp_parse_str( $url_parts[1], $query_args );
		}

		$pagenum_link = esc_url( remove_query_arg( array_keys( $query_args ), $pagenum_link ) );
		$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

		$format = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
		$format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';

		// Set up paginated links.
		$links = paginate_links( array(
			'base'      => $pagenum_link,
			'format'    => $format,
			'total'     => $this->num_pages,
			'current'   => $this->paged,
			'add_args'  => array_map( 'urlencode', $query_args ),
			'prev_text' => '<i class="fa fa-angle-left"></i>',
			'next_text' => '<i class="fa fa-angle-right"></i>',
			'type'      => 'list',
			'end_size'  => 3,
			'mid_size'  => 3,
		) );

		if ( ! $links ) {
			return '';
		}

		return $links;
	}
}

// Mapping shortcode.
vc_map( array(
	'name'        => esc_html__( 'Blog', 'tm-robin' ),
	'base'        => 'robin_blog',
	'icon'        => 'robin-icon-blog',
	'category'    => sprintf( esc_html__( 'by %s', 'tm-robin' ), TM_ROBIN_THEME_NAME ),
	'description' => esc_html__( 'Show posts in a grid or carousel', 'tm-robin' ),
	'params'      => array(
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'View mode', 'tm-robin' ),
			'description' => esc_html__( 'Select a template to display post', 'tm-robin' ),
			'param_name'  => 'view',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Grid', 'tm-robin' )     => 'grid',
				esc_html__( 'Carousel', 'tm-robin' ) => 'carousel',
				esc_html__( 'Masonry', 'tm-robin' )  => 'masonry',
			),
		),
		array(
			'type'       => 'dropdown',
			'heading'    => esc_html__( 'Alignment', 'tm-robin' ),
			'param_name' => 'grid_align',
			'value'      => array(
				esc_html__( 'Left', 'tm-robin' )   => 'text-xs-left',
				esc_html__( 'Center', 'tm-robin' ) => 'text-xs-center',
				esc_html__( 'Right', 'tm-robin' )  => 'text-xs-right',
			),
		),
		array(
			'type'       => 'checkbox',
			'param_name' => 'loop',
			'value'      => array( esc_html__( 'Enable loop mode', 'tm-robin' ) => 'yes' ),
			'std'        => 'yes',
			'dependency' => array( 'element' => 'view', 'value' => array( 'carousel' ) ),
		),
		array(
			'type'       => 'checkbox',
			'param_name' => 'auto_play',
			'value'      => array( esc_html__( 'Enable carousel autoplay', 'tm-robin' ) => 'yes' ),
			'dependency' => array(
				'element' => 'view',
				'value'   => array( 'carousel' ),
			),
		),
		array(
			'type'       => 'number',
			'param_name' => 'auto_play_speed',
			'heading'    => esc_html__( 'Auto play speed', 'tm-robin' ),
			'value'      => 5,
			'max'        => 10,
			'min'        => 3,
			'step'       => 0.5,
			'suffix'     => 'seconds',
			'dependency' => array(
				'element' => 'auto_play',
				'value'   => 'yes',
			),
		),
		array(
			'type'       => 'dropdown',
			'param_name' => 'nav_type',
			'heading'    => esc_html__( 'Navigation type', 'tm-robin' ),
			'value'      => array(
				esc_html__( 'Arrows', 'tm-robin' ) => 'arrows',
				esc_html__( 'Dots', 'tm-robin' )   => 'dots',
				__( 'Arrows & Dots', 'tm-robin' )  => 'both',
				esc_html__( 'None', 'tm-robin' )   => '',
			),
			'dependency' => array(
				'element' => 'view',
				'value'   => array( 'carousel' ),
			),
		),
		array(
			'type'        => 'number',
			'heading'     => esc_html__( 'Total Posts', 'tm-robin' ),
			'param_name'  => 'total_posts',
			'value'       => - 1,
			'max'         => 1000,
			'min'         => - 1,
			'step'        => 1,
			'description' => esc_html__( 'Set max limit for items in grid or enter -1 to show all (limited to 1000)', 'tm-robin' ),
		),
		array(
			'type'        => 'number',
			'heading'     => esc_html__( 'Posts per page', 'tm-robin' ),
			'param_name'  => 'posts_per_page',
			'value'       => 10,
			'min'         => 1,
			'step'        => 1,
			'description' => esc_html__( 'Number of items to show per page', 'tm-robin' ),
			'dependency'  => array(
				'element'            => 'view',
				'value_not_equal_to' => array( 'carousel' ),
			),
		),
		array(
			'type'       => 'checkbox',
			'param_name' => 'show_load_more',
			'value'      => array( esc_html__( 'Show \'Load more\' button instead of pagination', 'tm-robin' ) => 'yes' ),
			'dependency' => array( 'element' => 'view', 'value_not_equal_to' => array( 'carousel' ) ),
		),
		TM_Robin_VC::get_param( 'columns' ),
		TM_Robin_VC::get_param( 'el_class' ),
		TM_Robin_VC::get_param( 'order', esc_html__( 'Data Settings', 'tm-robin' ) ),
		TM_Robin_VC::get_param( 'order_way', esc_html__( 'Data Settings', 'tm-robin' ) ),
		array(
			'group'       => esc_html__( 'Data Settings', 'tm-robin' ),
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Filter by', 'tm-robin' ),
			'param_name'  => 'filter',
			'admin_label' => true,
			'description' => esc_html__( 'Select filter source.', 'tm-robin' ),
			'value'       => array(
				esc_html__( 'Categories', 'tm-robin' ) => 'category',
				esc_html__( 'Tags', 'tm-robin' )       => 'tag',
			),
		),
		array(
			'group'      => esc_html__( 'Data Settings', 'tm-robin' ),
			'type'       => 'chosen',
			'heading'    => esc_html__( 'Select Categories', 'tm-robin' ),
			'param_name' => 'cat_slugs',
			'options'    => array(
				'type'  => 'taxonomy',
				'get'   => 'category',
				'field' => 'slug',
			),
			'dependency' => array( 'element' => 'filter', 'value' => 'category' ),
		),
		array(
			'group'      => esc_html__( 'Data Settings', 'tm-robin' ),
			'type'       => 'chosen',
			'heading'    => esc_html__( 'Select Tags', 'tm-robin' ),
			'param_name' => 'tag_slugs',
			'options'    => array(
				'type'  => 'taxonomy',
				'get'   => 'post_tag',
				'field' => 'slug',
			),
			'dependency' => array( 'element' => 'filter', 'value' => 'tag' ),
		),
		TM_Robin_VC::get_param( 'css' ),
	),
) );
