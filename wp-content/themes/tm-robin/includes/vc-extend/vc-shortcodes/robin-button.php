<?php

/**
 * Robin Button Shortcode.
 *
 * @version 1.0
 * @package Robin
 */
class WPBakeryShortCode_Robin_Button extends WPBakeryShortCode {

	public function shortcode_css( $css_id ) {

		$atts  = vc_map_get_attributes( $this->getShortcode(), $this->getAtts() );
		$cssID = '#' . $css_id;

		$style = $atts['style'] ? $atts['style'] : '';

		$font_color          = $atts['font_color'] ? $atts['font_color'] : 'transparent';
		$button_bg_color     = $atts['button_bg_color'] ? $atts['button_bg_color'] : 'transparent';
		$button_border_color = $atts['button_border_color'] ? $atts['button_border_color'] : 'transparent';

		$font_color_hover          = $atts['font_color_hover'] ? $atts['font_color_hover'] : 'transparent';
		$button_bg_color_hover     = $atts['button_bg_color_hover'] ? $atts['button_bg_color_hover'] : 'transparent';
		$button_border_color_hover = $atts['button_border_color_hover'] ? $atts['button_border_color_hover'] : 'transparent';

		$font_size = $atts['font_size'] . 'px';

		$css = '';

		if ( $style == 'custom' ) {
			$css .= $cssID . '{color:' . $font_color . ';background-color:' . $button_bg_color . ';border-color:' . $button_border_color . ';font-size:' . $font_size . ';}';
			$css .= $cssID . ':hover{color:' . $font_color_hover . ';background-color:' . $button_bg_color_hover . ';border-color:' . $button_border_color_hover . ';}';
		}

		global $tm_robin_shortcode_css;
		$tm_robin_shortcode_css .= $css;
	}
}

$params = array_merge(
// General
	array(
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Button Style', 'tm-robin' ),
			'admin_label' => true,
			'param_name'  => 'style',
			'value'       => array(
				esc_html__( 'Default', 'tm-robin' )     => '',
				esc_html__( 'Alternative', 'tm-robin' ) => 'alt',
				esc_html__( 'Custom', 'tm-robin' )      => 'custom',
			),
			'description' => esc_html__( 'Select button style.', 'tm-robin' ),
		),
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Size', 'tm-robin' ),
			'admin_label' => true,
			'param_name'  => 'size',
			'value'       => array(
				esc_html__( 'Small', 'tm-robin' )       => 'small',
				esc_html__( 'Medium', 'tm-robin' )      => 'medium',
				esc_html__( 'Large', 'tm-robin' )       => 'large',
				esc_html__( 'Extra large', 'tm-robin' ) => 'xlarge',
			),
			'std'         => 'medium',
			'description' => esc_html__( 'Select button size.', 'tm-robin' ),
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Text', 'tm-robin' ),
			'admin_label' => true,
			'param_name'  => 'text',
			'description' => esc_html__( 'Enter text on the button.', 'tm-robin' ),
		),
		array(
			'type'        => 'vc_link',
			'heading'     => esc_html__( 'URL (Link)', 'tm-robin' ),
			'param_name'  => 'link',
			'description' => esc_html__( 'Enter button link', 'tm-robin' ),
		),
		array(
			'type'       => 'number',
			'heading'    => esc_html__( 'Font size', 'tm-robin' ),
			'param_name' => 'font_size',
			'value'      => 14,
			'min'        => 10,
			'suffix'     => 'px',
			'dependency' => array(
				'element' => 'style',
				'value'   => 'custom',
			),
		),
		array(
			'type'       => 'checkbox',
			'param_name' => 'add_icon',
			'value'      => array( esc_html__( 'Add icon?', 'tm-robin' ) => 'yes' ),
		),
	),
	// Extra class
	array(
		TM_Robin_VC::get_param( 'el_class' ),
	),
	// Icon.
	TM_Robin_VC::icon_libraries( array( 'element' => 'add_icon', 'not_empty' => true ) ),
	// Icon position.
	array(
		array(
			'group'       => esc_html__( 'Icon', 'tm-robin' ),
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Icon position', 'tm-robin' ),
			'value'       => array(
				esc_html__( 'Left', 'tm-robin' )  => 'left',
				esc_html__( 'Right', 'tm-robin' ) => 'right',
			),
			'param_name'  => 'icon_pos',
			'description' => esc_html__( 'Select icon library.', 'tm-robin' ),
			'dependency'  => array( 'element' => 'add_icon', 'not_empty' => true ),
		),
	),
	// Color.
	array(
		array(
			'group'      => esc_html__( 'Color', 'tm-robin' ),
			'type'       => 'colorpicker',
			'heading'    => esc_html__( 'Background color', 'tm-robin' ),
			'param_name' => 'button_bg_color',
			'value'      => tm_robin_get_option( 'secondary_color' ),
			'dependency' => array(
				'element' => 'style',
				'value'   => 'custom',
			),
		),
		array(
			'group'      => esc_html__( 'Color', 'tm-robin' ),
			'type'       => 'colorpicker',
			'heading'    => esc_html__( 'Background color (on hover)', 'tm-robin' ),
			'param_name' => 'button_bg_color_hover',
			'value'      => tm_robin_get_option( 'primary_color' ),
			'dependency' => array(
				'element' => 'style',
				'value'   => 'custom',
			),
		),
		array(
			'group'      => esc_html__( 'Color', 'tm-robin' ),
			'type'       => 'colorpicker',
			'heading'    => esc_html__( 'Text color', 'tm-robin' ),
			'param_name' => 'font_color',
			'value'      => '#fff',
			'dependency' => array(
				'element' => 'style',
				'value'   => 'custom',
			),
		),
		array(
			'group'      => esc_html__( 'Color', 'tm-robin' ),
			'type'       => 'colorpicker',
			'heading'    => esc_html__( 'Text color (on hover)', 'tm-robin' ),
			'param_name' => 'font_color_hover',
			'value'      => '#fff',
			'dependency' => array(
				'element' => 'style',
				'value'   => 'custom',
			),
		),
		array(
			'group'      => esc_html__( 'Color', 'tm-robin' ),
			'type'       => 'colorpicker',
			'heading'    => esc_html__( 'Border color', 'tm-robin' ),
			'param_name' => 'button_border_color',
			'value'      => tm_robin_get_option( 'secondary_color' ),
			'dependency' => array(
				'element' => 'style',
				'value'   => 'custom',
			),
		),
		array(
			'group'      => esc_html__( 'Color', 'tm-robin' ),
			'type'       => 'colorpicker',
			'heading'    => esc_html__( 'Border color (on hover)', 'tm-robin' ),
			'param_name' => 'button_border_color_hover',
			'value'      => tm_robin_get_option( 'primary_color' ),
			'dependency' => array(
				'element' => 'style',
				'value'   => 'custom',
			),
		),
	),
	array(
		// Css box,
		TM_Robin_VC::get_param( 'css' ),
	)
);

// Mapping shortcode.
vc_map( array(
	'name'        => esc_html__( 'Button', 'tm-robin' ),
	'base'        => 'robin_button',
	'icon'        => 'robin-icon-button',
	'category'    => sprintf( esc_html__( 'by %s', 'tm-robin' ), TM_ROBIN_THEME_NAME ),
	'description' => esc_html__( 'Eye catching button', 'tm-robin' ),
	'js_view'     => 'VcIconElementView_Backend',
	'params'      => $params,
) );
