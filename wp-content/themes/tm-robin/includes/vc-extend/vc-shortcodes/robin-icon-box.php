<?php

/**
 * Robin Icon Box Shortcode
 *
 * @version 1.0
 * @package Robin
 */
class WPBakeryShortCode_Robin_Icon_Box extends WPBakeryShortCode {

	public function shortcode_css( $css_id ) {

		$atts   = vc_map_get_attributes( $this->getShortcode(), $this->getAtts() );
		$css_id = '#' . $css_id;

		$icon_font_size = $atts['icon_font_size'];
		$icon_color     = $atts['icon_color'] ? $atts['icon_color'] : 'transparent';
		$icon_bgcolor   = $atts['icon_bgcolor'] ? $atts['icon_bgcolor'] : 'transparent';

		$title_font_size  = $atts['title_font_size'];
		$title_font_color = $atts['title_font_color'] ? $atts['title_font_color'] : 'transparent';

		$content_font_size  = $atts['content_font_size'];
		$content_font_color = $atts['content_font_color'] ? $atts['content_font_color'] : 'transparent';

		$link_color = $atts['link_color'] ? $atts['link_color'] : 'transparent';

		$with_bg  = $atts['with_bg'];
		$bg_shape = $atts['bg_shape'];

		$css = '';

		$icon = $css_id . ' i,' . $css_id . ' span';
		$css .= $icon . '{color: ' . $icon_color . ';';

		if ( 'yes' == $with_bg ) {
			if ( $bg_shape == 'square' || $bg_shape == 'circle' || $bg_shape == 'rounded' ) {
				$css .= 'background-color:' . $icon_bgcolor . ';';
			} else {
				$css .= 'border-color:' . $icon_bgcolor . ';';
			}
		}

		if ( is_numeric( $icon_font_size ) ) {
			$css .= 'font-size:' . $icon_font_size . 'px;}';
		}

		$title = $css_id . ' .title, ' . $css_id . ' .title > a';
		$css .= $title . '{color:' . $title_font_color . ';';

		if ( is_numeric( $title_font_size ) ) {
			$css .= 'font-size:' . $title_font_size . 'px;}';
		}

		$description = $css_id . ' .description,' . $css_id . ' .description em';
		$css .= $description . '{color:' . $content_font_color . ';';

		if ( is_numeric( $content_font_size ) ) {
			$css .= 'font-size:' . $content_font_size . 'px;}';
		}

		$css .= $css_id . ' a{color:' . $link_color . ';}';

		global $tm_robin_shortcode_css;
		$tm_robin_shortcode_css .= $css;
	}
}

$params = array_merge(
// General
	array(
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Style', 'tm-robin' ),
			'param_name'  => 'style',
			'value'       => array(
				esc_html__( 'Left', 'tm-robin' )   => 'left',
				esc_html__( 'Center', 'tm-robin' ) => 'center',
				esc_html__( 'Right', 'tm-robin' )  => 'right',
			),
			'description' => esc_html__( 'Select icon box style', 'tm-robin' ),
		),
		array(
			'type'       => 'dropdown',
			'heading'    => esc_html__( 'Vertical Alignment', 'tm-robin' ),
			'param_name' => 'v_align',
			'value'      => array(
				esc_html__( 'Top', 'tm-robin' )    => 'top',
				esc_html__( 'Middle', 'tm-robin' ) => 'middle',
				esc_html__( 'Bottom', 'tm-robin' ) => 'bottom',
			),
		),
		array(
			'type'        => 'textarea',
			'heading'     => esc_html__( 'Title', 'tm-robin' ),
			'param_name'  => 'title',
			'admin_label' => true,
			'value'       => esc_html__( 'This is icon box title', 'tm-robin' ),
		),
		array(
			'type'       => 'number',
			'heading'    => esc_html__( 'Title font size', 'tm-robin' ),
			'param_name' => 'title_font_size',
			'value'      => 20,
			'min'        => 10,
			'suffix'     => 'px',
		),
		array(
			'type'       => 'textarea_html',
			'heading'    => esc_html__( 'Description', 'tm-robin' ),
			'param_name' => 'content',
			'value'      => wp_kses_post( __( '<p>This is the description of icon box element</p>', 'tm-robin' ) ),
		),
		array(
			'type'       => 'number',
			'heading'    => esc_html__( 'Description font size', 'tm-robin' ),
			'param_name' => 'content_font_size',
			'value'      => 15,
			'min'        => 10,
			'suffix'     => 'px',
		),
		array(
			'type'        => 'vc_link',
			'heading'     => esc_html__( 'URL (Link)', 'tm-robin' ),
			'param_name'  => 'link',
			'description' => esc_html__( 'Add link to icon box', 'tm-robin' ),
		),
		array(
			'type'       => 'checkbox',
			'param_name' => 'use_link_title',
			'value'      => array( esc_html__( 'Use link in title', 'tm-robin' ) => 'yes' ),
		),
		array(
			'type'       => 'checkbox',
			'param_name' => 'use_text',
			'value'      => array( esc_html__( 'Use text instead of icon', 'tm-robin' ) => 'yes' ),
		),
		array(
			'type'       => 'textfield',
			'heading'    => esc_html__( 'Text', 'tm-robin' ),
			'param_name' => 'text',
			'dependency' => array(
				'element' => 'use_text',
				'value'   => 'yes',
			),
		),
	),
	// Extra class.
	array(
		TM_Robin_VC::get_param( 'el_class' ),
	),
	// Icon.
	TM_Robin_VC::icon_libraries( array( 'element' => 'use_text', 'value_not_equal_to' => 'yes' ) ),
	// Icon font-size.
	array(
		array(
			'group'      => esc_html__( 'Icon', 'tm-robin' ),
			'type'       => 'number',
			'heading'    => esc_html__( 'Font size', 'tm-robin' ),
			'param_name' => 'icon_font_size',
			'value'      => 40,
			'min'        => 10,
			'suffix'     => 'px',
		),
	),
	array(
		array(
			'group'      => esc_html__( 'Icon', 'tm-robin' ),
			'type'       => 'checkbox',
			'param_name' => 'with_bg',
			'value'      => array( esc_html__( 'Icon with background', 'tm-robin' ) => 'yes' ),
		),
		array(
			'group'      => esc_html__( 'Icon', 'tm-robin' ),
			'type'       => 'dropdown',
			'heading'    => esc_html__( 'Background shape', 'tm-robin' ),
			'param_name' => 'bg_shape',
			'value'      => array(
				esc_html__( 'Square', 'tm-robin' )          => 'square',
				esc_html__( 'Circle', 'tm-robin' )          => 'circle',
				esc_html__( 'Rounded', 'tm-robin' )         => 'rounded',
				esc_html__( 'Outline Square', 'tm-robin' )  => 'outline-square',
				esc_html__( 'Outline Circle', 'tm-robin' )  => 'outline-circle',
				esc_html__( 'Outline Rounded', 'tm-robin' ) => 'outline-rounded',
			),
			'dependency' => array(
				'element' => 'with_bg',
				'value'   => 'yes',
			),
		),
	),
	// Color.
	array(
		array(
			'group'       => esc_html__( 'Color', 'tm-robin' ),
			'type'        => 'colorpicker',
			'heading'     => esc_html__( 'Title font color', 'tm-robin' ),
			'param_name'  => 'title_font_color',
			'value'       => '#222222',
			'description' => esc_html__( 'Select title font color', 'tm-robin' ),
		),
		array(
			'group'       => esc_html__( 'Color', 'tm-robin' ),
			'type'        => 'colorpicker',
			'heading'     => esc_html__( 'Description font color', 'tm-robin' ),
			'param_name'  => 'content_font_color',
			'value'       => '#878787',
			'description' => esc_html__( 'Select description font color', 'tm-robin' ),
		),
		array(
			'group'       => esc_html__( 'Color', 'tm-robin' ),
			'type'        => 'colorpicker',
			'heading'     => esc_html__( 'Link color', 'tm-robin' ),
			'param_name'  => 'link_color',
			'value'       => '#878787',
			'description' => esc_html__( 'Select link color', 'tm-robin' ),
		),
		array(
			'group'       => esc_html__( 'Color', 'tm-robin' ),
			'type'        => 'colorpicker',
			'heading'     => esc_html__( 'Icon color', 'tm-robin' ),
			'admin_label' => true,
			'param_name'  => 'icon_color',
			'value'       => tm_robin_get_option( 'primary_color' ),
			'description' => esc_html__( 'Select icon color', 'tm-robin' ),
		),
		array(
			'group'       => esc_html__( 'Color', 'tm-robin' ),
			'type'        => 'colorpicker',
			'heading'     => esc_html__( 'Icon background color', 'tm-robin' ),
			'param_name'  => 'icon_bgcolor',
			'value'       => tm_robin_get_option( 'primary_color' ),
			'description' => esc_html__( 'Select icon background color', 'tm-robin' ),
			'dependency'  => array(
				'element' => 'with_bg',
				'value'   => 'yes',
			),
		),
	),
	// Css box,
	array(
		TM_Robin_VC::get_param( 'css' ),
	)
);

vc_map( array(
	        'name'        => esc_html__( 'Icon Box', 'tm-robin' ),
	        'base'        => 'robin_icon_box',
	        'icon'        => 'robin-icon-box',
	        'category'    => sprintf( esc_html__( 'by %s', 'tm-robin' ), TM_ROBIN_THEME_NAME ),
	        'description' => esc_html__( 'Eye catching icons from libraries', 'tm-robin' ),
	        'js_view'     => 'VcIconElementView_Backend',
	        'params'      => $params,
        ) );
