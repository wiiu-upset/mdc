<?php

/**
 * Robin Instagram shortcode
 *
 * @version 1.0
 * @package Robin
 */
class WPBakeryShortCode_Robin_Instagram extends WPBakeryShortCode {

}

// Mapping shortcode
vc_map( array(
			'name'        => esc_html__( 'Instagram', 'tm-robin' ),
			'base'        => 'robin_instagram',
			'icon'        => 'robin-icon-instagram',
			'category'    => sprintf( esc_html__( 'by % s', 'tm-robin' ), TM_ROBIN_THEME_NAME ),
			'description' => esc_html__( 'Displays latest Instagram photos', 'tm-robin' ),
			'params'      => array(
				array(
					'type'        => 'textfield',
					'heading'     => __( 'Instagram Username', 'tm-robin' ),
					'admin_label' => true,
					'param_name'  => 'username',
					'value'       => '',
					'description' => esc_html__( 'Enter Instagram username (not include @). Example: <b>thememove</b>', 'tm-robin' ),
				),
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Number of items', 'tm-robin' ),
					'admin_label' => true,
					'param_name'  => 'number_items',
					'value'       => '6',
				),
				array(
					'type'        => 'dropdown',
					'heading'     => __( 'Item Width', 'tm-robin' ),
					'description' => __( 'Enter item width in a row (has 12 columns)', 'tm-robin' ),
					'admin_label' => true,
					'param_name'  => 'item_width_md',
					'value'       => array(
						esc_html__( 'Default (2 columns - 1/6 - 6 items in a row)', 'tm-robin' ) => 2,
						esc_html__( '1 columns - 1/1 - 12 items in a row', 'tm-robin' )          => 1,
						esc_html__( '2 columns - 1/6 - 6 items in a row', 'tm-robin' )           => 2,
						esc_html__( '3 columns - 1/4 - 4 items in a row', 'tm-robin' )           => 3,
						esc_html__( '4 columns - 1/3 - 3 items in a row', 'tm-robin' )           => 4,
						esc_html__( '5 columns - 5 items in a row', 'tm-robin' )                 => 'is-5',
						esc_html__( '6 columns - 1/2 - 2 items in a row', 'tm-robin' )           => 6,
						esc_html__( '12 columns - 1/1 - 1 item in a row', 'tm-robin' )           => 12,
					),
				),
				array(
					'type'       => 'textfield',
					'heading'    => esc_html__( 'Item spacing', 'tm-robin' ),
					'param_name' => 'spacing',
					'value'      => '0',
				),
				array(
					'type'       => 'checkbox',
					'param_name' => 'show_likes_comments',
					'value'      => array(
						esc_html__( 'Show likes and comments', 'tm-robin' ) => true,
					),
				),
				array(
					'type'       => 'checkbox',
					'param_name' => 'show_follow_text',
					'value'      => array(
						esc_html__( 'Show follow text', 'tm-robin' ) => true,
					),
				),
				array(
					'type'       => 'textarea_html',
					'heading'    => __( 'Text', 'tm-robin' ),
					'param_name' => 'content',
					'value'      => __( 'Follow us on Instagram', 'tm-robin' ),
					'dependency' => array(
						'element'   => 'show_follow_text',
						'not_empty' => true,
					),
				),
				array(
					'type'       => 'checkbox',
					'param_name' => 'link_new_page',
					'value'      => array(
						esc_html__( 'Open links in new page', 'tm-robin' ) => true,
					),
				),
				array(
					'type'       => 'checkbox',
					'param_name' => 'square_media',
					'value'      => array(
						esc_html__( 'Show square media', 'tm-robin' ) => true,
					),
					'std'        => true,
				),
				TM_Robin_VC::get_param( 'el_class' ),
				TM_Robin_VC::get_param( 'css' ),
			),

		) );
