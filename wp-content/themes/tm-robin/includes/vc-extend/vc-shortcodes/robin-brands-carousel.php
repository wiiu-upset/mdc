<?php

/**
 * Robin Brands Carousel
 *
 * @version 1.0
 * @package Robin
 */
class WPBakeryShortCode_Robin_Brands_Carousel extends WPBakeryShortCode {
}

// Mapping shortcode.
vc_map( array(
	'name'        => __( 'Brand\'s Image Carousel', 'tm-robin' ),
	'base'        => 'robin_brands_carousel',
	'icon'        => 'robin-icon-brands-carousel',
	'category'    => sprintf( esc_html__( 'by %s', 'tm-robin' ), TM_ROBIN_THEME_NAME ),
	'description' => esc_html__( 'Show brands in a carousel.', 'tm-robin' ),
	'params'      => array(
		array(
			'type'       => 'chosen',
			'heading'    => esc_html__( 'Select Brand', 'tm-robin' ),
			'param_name' => 'brand_slugs',
			'options'    => array(
				'type'  => 'taxonomy',
				'get'   => 'product_brand',
				'field' => 'slug',
			)
		),
		array(
			'type'       => 'checkbox',
			'param_name' => 'hide_empty',
			'std'        => 'yes',
			'value'      => array( esc_html__( 'Hide empty brands', 'tm-robin' ) => 'yes' ),
		),
		array(
			'type'       => 'checkbox',
			'param_name' => 'display_featured',
			'value'      => array( esc_html__( 'Display only feature brands', 'tm-robin' ) => 'yes' ),
		),
		array(
			'type'       => 'dropdown',
			'heading'    => esc_html__( 'Number of brands to show', 'tm-robin' ),
			'param_name' => 'number',
			'std'        => 6,
			'value'      => array(
				1,
				2,
				3,
				4,
				5,
				6,
			)
		),
		array(
			'type'       => 'checkbox',
			'param_name' => 'show_title',
			'value'      => array( esc_html__( 'Show brand\'s title', 'tm-robin' ) => 'yes' ),
		),
		array(
			'type'       => 'checkbox',
			'param_name' => 'new_tab',
			'value'      => array( esc_html__( 'Open link in a new tab', 'tm-robin' ) => 'yes' ),
		),
		array(
			'type'       => 'checkbox',
			'param_name' => 'loop',
			'value'      => array( esc_html__( 'Enable carousel loop mode', 'tm-robin' ) => 'yes' ),
			'std'        => 'yes',
		),
		array(
			'type'       => 'checkbox',
			'param_name' => 'auto_play',
			'value'      => array( esc_html__( 'Enable carousel autolay', 'tm-robin' ) => 'yes' ),
		),
		array(
			'type'       => 'number',
			'param_name' => 'auto_play_speed',
			'heading'    => esc_html__( 'Auto play speed', 'tm-robin' ),
			'value'      => 5,
			'max'        => 10,
			'min'        => 3,
			'step'       => 0.5,
			'suffix'     => 'seconds',
			'dependency' => array(
				'element' => 'auto_play',
				'value'   => 'yes',
			),
		),
		array(
			'type'       => 'dropdown',
			'param_name' => 'nav_type',
			'heading'    => esc_html__( 'Navigation type', 'tm-robin' ),
			'value'      => array(
				esc_html__( 'Arrows', 'tm-robin' ) => 'arrows',
				esc_html__( 'Dots', 'tm-robin' )   => 'dots',
				__( 'Arrows & Dots', 'tm-robin' )  => 'both',
				esc_html__( 'None', 'tm-robin' )   => '',
			),
		),
		TM_Robin_VC::get_param( 'el_class' ),
		TM_Robin_VC::get_param( 'css' ),
	)
) );
