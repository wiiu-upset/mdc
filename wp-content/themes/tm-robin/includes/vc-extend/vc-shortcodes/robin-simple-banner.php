<?php

/**
 * ThemeMove Simple Banner Shortcode
 *
 * @version 1.0
 * @package Robin
 */
class WPBakeryShortCode_Robin_Simple_Banner extends WPBakeryShortCode {

	public function shortcode_css( $css_id ) {

		$atts  = vc_map_get_attributes( $this->getShortcode(), $this->getAtts() );
		$cssID = '#' . $css_id;

		$button_style        = $atts['button_style'] ? $atts['button_style'] : '';
		$button_color        = $atts['button_color'] ? $atts['button_color'] : 'transparent';
		$button_bg_color     = $atts['button_bg_color'] ? $atts['button_bg_color'] : 'transparent';
		$button_border_color = $atts['button_border_color'] ? $atts['button_border_color'] : 'transparent';

		$button_color_hover        = $atts['button_color_hover'] ? $atts['button_color_hover'] : 'transparent';
		$button_bg_color_hover     = $atts['button_bg_color_hover'] ? $atts['button_bg_color_hover'] : 'transparent';
		$button_border_color_hover = $atts['button_border_color_hover'] ? $atts['button_border_color_hover'] : 'transparent';

		$css = '';

		if ( $button_style == 'custom' ) {
			$css .= $cssID . ' .banner-button{color:' . $button_color . ';background-color:' . $button_bg_color . ';border-color:' . $button_border_color . ';}';
			$css .= $cssID . ' .banner-button:hover{color:' . $button_color_hover . ';background-color:' . $button_bg_color_hover . ';border-color:' . $button_border_color_hover . ';}';
		}

		global $tm_robin_shortcode_css;
		$tm_robin_shortcode_css .= $css;
	}

	/**
	 *
	 * Custom title in backend, show image instead of icon
	 *
	 * @param $param
	 * @param $value
	 *
	 * @return string
	 */
	public function singleParamHtmlHolder( $param, $value ) {

		$output = '';

		$param_name = isset( $param['param_name'] ) ? $param['param_name'] : '';
		$type       = isset( $param['type'] ) ? $param['type'] : '';
		$class      = isset( $param['class'] ) ? $param['class'] : '';

		if ( 'attach_image' === $param['type'] && 'image' === $param_name ) {
			$output       .= '<input type="hidden" class="wpb_vc_param_value ' . $param_name . ' ' . $type . ' ' . $class . '" name="' . $param_name . '" value="' . $value . '" />';
			$element_icon = $this->settings( 'icon' );
			$img          = wpb_getImageBySize( array(
				'attach_id'  => (int) preg_replace( '/[^\d]/', '', $value ),
				'thumb_size' => 'thumbnail',
				'class'      => 'attachment-thumbnail vc_general vc_element-icon tm-element-icon-none',
			) );
			$this->setSettings( 'logo', ( $img ? $img['thumbnail'] : '<img width="150" height="150" src="' . vc_asset_url( 'vc/blank.gif' ) . '" class="attachment-thumbnail vc_general vc_element-icon tm-element-icon-banner1"  data-name="' . $param_name . '" alt="" title="" style="display: none;" />' ) . '<span class="no_image_image vc_element-icon' . ( ! empty( $element_icon ) ? ' ' . $element_icon : '' ) . ( $img && ! empty( $img['p_img_large'][0] ) ? ' image-exists' : '' ) . '" /><a href="#" class="column_edit_trigger' . ( $img && ! empty( $img['p_img_large'][0] ) ? ' image-exists' : '' ) . '">' . __( 'Add image', 'tm-robin' ) . '</a>' );
			$output .= $this->outputCustomTitle( $this->settings['name'] );
		} elseif ( ! empty( $param['holder'] ) ) {
			if ( 'input' === $param['holder'] ) {
				$output .= '<' . $param['holder'] . ' readonly="true" class="wpb_vc_param_value ' . $param_name . ' ' . $type . ' ' . $class . '" name="' . $param_name . '" value="' . $value . '">';
			} elseif ( in_array( $param['holder'], array(
				'img',
				'iframe',
			) ) ) {
				$output .= '<' . $param['holder'] . ' class="wpb_vc_param_value ' . $param_name . ' ' . $type . ' ' . $class . '" name="' . $param_name . '" src="' . $value . '">';
			} elseif ( 'hidden' !== $param['holder'] ) {
				$output .= '<' . $param['holder'] . ' class="wpb_vc_param_value ' . $param_name . ' ' . $type . ' ' . $class . '" name="' . $param_name . '">' . $value . '</' . $param['holder'] . '>';
			}
		}

		if ( ! empty( $param['admin_label'] ) && true === $param['admin_label'] ) {
			$output .= '<span class="vc_admin_label admin_label_' . $param['param_name'] . ( empty( $value ) ? ' hidden-label' : '' ) . '"><label>' . $param['heading'] . '</label>: ' . $value . '</span>';
		}

		return $output;
	}

	protected function outputTitle( $title ) {
		return '';
	}

	protected function outputCustomTitle( $title ) {
		return '<h4 class="wpb_element_title">' . $title . ' ' . $this->settings( 'logo' ) . '</h4>';
	}
}

vc_map( array(
	'name'        => esc_html__( 'Simple Banner', 'tm-robin' ),
	'base'        => 'robin_simple_banner',
	'icon'        => 'robin-icon-simple-banner',
	'description' => esc_html__( 'Banner with simple image and text', 'tm-robin' ),
	'category'    => sprintf( esc_html__( 'by %s', 'tm-robin' ), TM_ROBIN_THEME_NAME ),
	'params'      => array(

		array(
			'type'        => 'dropdown',
			'heading'     => __( 'Image source', 'tm-robin' ),
			'param_name'  => 'source',
			'value'       => array(
				esc_html__( 'Media library', 'tm-robin' ) => 'media_library',
				esc_html__( 'External link', 'tm-robin' ) => 'external_link',
			),
			'std'         => 'media_library',
			'description' => esc_html__( 'Select image source.', 'tm-robin' ),
		),

		array(
			'type'        => 'attach_image',
			'heading'     => esc_html__( 'Banner image', 'tm-robin' ),
			'param_name'  => 'image',
			'value'       => '',
			'description' => esc_html__( 'Select an image from media library.', 'tm-robin' ),
			'dependency'  => array(
				'element' => 'source',
				'value'   => 'media_library',
			),
		),

		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'External link', 'tm-robin' ),
			'param_name'  => 'custom_src',
			'description' => esc_html__( 'Select external link.', 'tm-robin' ),
			'dependency'  => array(
				'element' => 'source',
				'value'   => 'external_link',
			),
		),

		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Image size (Optional)', 'tm-robin' ),
			'param_name'  => 'img_size',
			'value'       => 'full',
			'description' => esc_html__( 'Enter image size (Example: "thumbnail", "medium", "large", "full" or other sizes defined by theme). Alternatively enter size in pixels (Example: 200x100 (Width x Height)).', 'tm-robin' ),
			'dependency'  => array(
				'element' => 'source',
				'value'   => array( 'media_library' ),
			),
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Image size (Optional)', 'tm-robin' ),
			'param_name'  => 'external_img_size',
			'value'       => '',
			'description' => esc_html__( 'Enter image size in pixels. Example: 200x100 (Width x Height).', 'tm-robin' ),
			'dependency'  => array(
				'element' => 'source',
				'value'   => 'external_link',
			),
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'First line', 'tm-robin' ),
			'param_name'  => 'first_line',
			'description' => esc_html__( 'Enter text for the first line', 'tm-robin' ),
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Second line', 'tm-robin' ),
			'param_name'  => 'second_line',
			'description' => esc_html__( 'Enter text for the second line', 'tm-robin' ),
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Third line', 'tm-robin' ),
			'param_name'  => 'third_line',
			'description' => esc_html__( 'Enter text for the third line', 'tm-robin' ),
		),
		array(
			'type'        => 'vc_link',
			'heading'     => esc_html__( 'Banner URL (Link)', 'tm-robin' ),
			'param_name'  => 'banner_link',
			'description' => esc_html__( 'Enter banner link', 'tm-robin' ),
		),
		array(
			'type'        => 'animation_style',
			'heading'     => esc_html__( 'Animation', 'tm-robin' ),
			'param_name'  => 'animation',
			'settings'    => array(
				'type' => array( 'in', 'other' )
			),
			'admin_label' => true
		),
		TM_Robin_VC::get_param( 'el_class' ),

		// Button
		array(
			'group'      => esc_html__( 'Button', 'tm-robin' ),
			'type'       => 'textfield',
			'heading'    => esc_html__( 'Button Text', 'tm-robin' ),
			'param_name' => 'button_text',
		),
		array(
			'group'       => esc_html__( 'Button', 'tm-robin' ),
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Button Style', 'tm-robin' ),
			'param_name'  => 'button_style',
			'value'       => array(
				esc_html__( 'Default', 'tm-robin' )     => '',
				esc_html__( 'Alternative', 'tm-robin' ) => 'alt',
				esc_html__( 'Custom', 'tm-robin' )      => 'custom',
			),
			'description' => esc_html__( 'Select button style.', 'tm-robin' ),
		),
		array(
			'group'      => esc_html__( 'Button', 'tm-robin' ),
			'type'       => 'colorpicker',
			'heading'    => esc_html__( 'Background color', 'tm-robin' ),
			'param_name' => 'button_bg_color',
			'value'      => tm_robin_get_option( 'secondary_color' ),
			'dependency' => array(
				'element' => 'button_style',
				'value'   => 'custom',
			),
		),
		array(
			'group'      => esc_html__( 'Button', 'tm-robin' ),
			'type'       => 'colorpicker',
			'heading'    => esc_html__( 'Background color (on hover)', 'tm-robin' ),
			'param_name' => 'button_bg_color_hover',
			'value'      => tm_robin_get_option( 'primary_color' ),
			'dependency' => array(
				'element' => 'button_style',
				'value'   => 'custom',
			),
		),
		array(
			'group'      => esc_html__( 'Button', 'tm-robin' ),
			'type'       => 'colorpicker',
			'heading'    => esc_html__( 'Text color', 'tm-robin' ),
			'param_name' => 'button_color',
			'value'      => '#fff',
			'dependency' => array(
				'element' => 'button_style',
				'value'   => 'custom',
			),
		),
		array(
			'group'      => esc_html__( 'Button', 'tm-robin' ),
			'type'       => 'colorpicker',
			'heading'    => esc_html__( 'Text color (on hover)', 'tm-robin' ),
			'param_name' => 'button_color_hover',
			'value'      => '#fff',
			'dependency' => array(
				'element' => 'button_style',
				'value'   => 'custom',
			),
		),
		array(
			'group'      => esc_html__( 'Button', 'tm-robin' ),
			'type'       => 'colorpicker',
			'heading'    => esc_html__( 'Border color', 'tm-robin' ),
			'param_name' => 'button_border_color',
			'value'      => tm_robin_get_option( 'secondary_color' ),
			'dependency' => array(
				'element' => 'button_style',
				'value'   => 'custom',
			),
		),
		array(
			'group'      => esc_html__( 'Button', 'tm-robin' ),
			'type'       => 'colorpicker',
			'heading'    => esc_html__( 'Border color (on hover)', 'tm-robin' ),
			'param_name' => 'button_border_color_hover',
			'value'      => tm_robin_get_option( 'primary_color' ),
			'dependency' => array(
				'element' => 'button_style',
				'value'   => 'custom',
			),
		),

		TM_Robin_VC::get_param( 'css' ),
	),
) );
