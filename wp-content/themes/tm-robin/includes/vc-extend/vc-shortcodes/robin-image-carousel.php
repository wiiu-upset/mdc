<?php

/**
 * Robin Image Carousel
 *
 * @version 1.0
 * @package Robin
 */
class WPBakeryShortCode_Robin_Image_Carousel extends WPBakeryShortCode {

	public function shortcode_css( $css_id ) {

		$atts   = vc_map_get_attributes( $this->getShortcode(), $this->getAtts() );
		$css_id = '#' . $css_id;
		$css    = '';

		$effect  = $atts['effect'];
		$opacity = $atts['opacity'];
		$scale   = $atts['scale'];
		$overlay = $atts['overlay'];

		switch ( $effect ) {
			case 'opacity':
				$css .= $css_id . ' .tm-carousel-item img{opacity:' . $opacity . '}';
				$css .= $css_id . ' .tm-carousel-item:hover img{opacity:1}';
				break;
			case 'zoom':
				$css .= $css_id . ' .tm-carousel-item img{ -ms-transform: scale(1);-webkit-transform: scale(1);transform: scale(1);}';
				$css .= $css_id . ' .tm-carousel-item:hover img{ -ms-transform: scale(' . $scale . ');-webkit-transform: scale(' . $scale . ');transform: scale(' . $scale . ');}';
				break;
			case 'overlay':
				$css .= $css_id . '.link-no .tm-carousel-item:before,' . $css_id . ' .tm-carousel-item a:before{background-color:' . $overlay . '}';
				break;
			default:
				break;
		}

		global $tm_robin_shortcode_css;
		$tm_robin_shortcode_css .= $css;
	}
}

vc_map( array(
	        'name'        => esc_html__( 'Image Carousel', 'tm-robin' ),
	        'base'        => 'robin_image_carousel',
	        'icon'        => 'robin-icon-image-carousel',
	        'description' => esc_html__( 'Animated carousel with images', 'tm-robin' ),
	        'category'    => sprintf( esc_html__( 'by % s', 'tm-robin' ), TM_ROBIN_THEME_NAME ),
	        'params'      => array(
				array(
			        'type'        => 'attach_images',
			        'heading'     => esc_html__( 'Images', 'tm-robin' ),
			        'param_name'  => 'images',
			        'value'       => '',
			        'description' => esc_html__( 'Select images from media library . ', 'tm-robin' ),
			        'save_always' => true,
		        ),
				array(
			        'type'        => 'textfield',
			        'heading'     => esc_html__( 'Carousel image size', 'tm-robin' ),
			        'param_name'  => 'img_size',
			        'value'       => 'full',
			        'description' => esc_html__( 'Enter image size . Example: "thumbnail", "medium", "large", "full" or other sizes defined by current theme . Alternatively enter image size in pixels: 200x100( Width x Height). Leave empty to use "thumbnail" size . ', 'tm-robin' ),
		        ),
				array(
			        'type'        => 'dropdown',
			        'heading'     => esc_html__( 'On click action', 'tm-robin' ),
			        'param_name'  => 'onclick',
			        'value'       => array(
				        esc_html__( 'None', 'tm-robin' )              => 'link_no',
				        esc_html__( 'Open lightbox', 'tm-robin' )     => 'link_image',
				        esc_html__( 'Open custom links', 'tm-robin' ) => 'custom_link',
			        ),
			        'description' => esc_html__( 'Select action for click event. ', 'tm-robin' ),
		        ),
				array(
			        'type'        => 'dropdown',
			        'heading'     => esc_html__( 'Custom link target', 'tm-robin' ),
			        'param_name'  => 'custom_links_target',
			        'description' => esc_html__( 'Select how to open custom links . ', 'tm-robin' ),
			        'dependency'  => array(
				        'element' => 'onclick',
				        'value'   => array( 'custom_link' ),
			        ),
			        'value'       => vc_target_param_list(),
		        ),

		        array(
			        'type'        => 'exploded_textarea_safe',
			        'heading'     => esc_html__( 'Custom links', 'tm-robin' ),
			        'param_name'  => 'custom_links',
			        'description' => esc_html__( 'Enter links for each slide( Note: divide links with linebreaks( Enter ).', 'tm-robin' ),
			        'dependency'  => array(
				        'element' => 'onclick',
				        'value'   => array( 'custom_link' ),
			        ),
		        ),
		        array(
			        'type'       => 'dropdown',
			        'heading'    => esc_html__( 'Number of images to show', 'tm-robin' ),
			        'param_name' => 'number_of_images_to_show',
			        'value'      => array(
				        1,
				        2,
				        3,
				        4,
				        5,
				        6,
			        ),
		        ),
				array(
			        'type'       => 'checkbox',
			        'param_name' => 'loop',
			        'value'      => array( esc_html__( 'Enable carousel loop mode', 'tm-robin' ) => 'yes' ),
			        'std'        => 'yes',
		        ),
				array(
			        'type'       => 'checkbox',
			        'param_name' => 'auto_play',
			        'value'      => array( esc_html__( 'Enable carousel autolay', 'tm-robin' ) => 'yes' ),
		        ),
				array(
			        'type'       => 'number',
			        'param_name' => 'auto_play_speed',
			        'heading'    => esc_html__( 'Auto play speed', 'tm-robin' ),
			        'value'      => 5,
			        'max'        => 10,
			        'min'        => 3,
			        'step'       => 0.5,
			        'suffix'     => 'seconds',
			        'dependency' => array(
				        'element' => 'auto_play',
				        'value'   => 'yes',
			        ),
		        ),
				array(
			        'type'       => 'dropdown',
			        'param_name' => 'nav_type',
			        'heading'    => esc_html__( 'Navigation type', 'tm-robin' ),
			        'value'      => array(
				        esc_html__( 'Arrows', 'tm-robin' ) => 'arrows',
				        esc_html__( 'Dots', 'tm-robin' )   => 'dots',
				        __( 'Arrows & Dots', 'tm-robin' )  => 'both',
				        esc_html__( 'None', 'tm-robin' )   => '',
			        ),
		        ),
				array(
			        'type'       => 'checkbox',
			        'param_name' => 'show_title',
			        'value'      => array( esc_html__( 'Show title of images', 'tm-robin' ) => 'yes' ),
		        ),
				TM_Robin_VC::get_param( 'el_class' ),
		        array(
			        'group'       => esc_html__( 'Hover effect', 'tm-robin' ),
			        'type'        => 'dropdown',
			        'param_name'  => 'effect',
			        'heading'     => esc_html__( 'Image hover effect', 'tm-robin' ),
			        'description' => esc_html__( 'Select an effect when mouse over the images . ', 'tm-robin' ),
			        'value'       => array(
				        esc_html__( 'Opacity', 'tm-robin' ) => 'opacity',
				        esc_html__( 'Zoom', 'tm-robin' )    => 'zoom',
				        esc_html__( 'Overlay', 'tm-robin' ) => 'overlay',
			        ),
		        ),
		        array(
			        'group'      => esc_html__( 'Hover effect', 'tm-robin' ),
			        'type'       => 'number',
			        'heading'    => esc_html__( 'Opacity value from', 'tm-robin' ),
			        'param_name' => 'opacity',
			        'value'      => 0.6,
			        'min'        => 0,
			        'max'        => 1,
			        'step'       => 0.1,
			        'suffix'     => 'to 1',
			        'dependency' => array(
				        'element' => 'effect',
				        'value'   => array( 'opacity' ),
			        ),
		        ),
		        array(
			        'group'      => esc_html__( 'Hover effect', 'tm-robin' ),
			        'type'       => 'number',
			        'heading'    => esc_html__( 'Zoom scale', 'tm-robin' ),
			        'param_name' => 'scale',
			        'max'        => 5,
			        'min'        => 0.1,
			        'value'      => 1.1,
			        'step'       => 0.1,
			        'dependency' => array(
				        'element' => 'effect',
				        'value'   => array( 'zoom' ),
			        ),
		        ),
		        array(
			        'group'      => esc_html__( 'Hover effect', 'tm-robin' ),
			        'type'       => 'colorpicker',
			        'heading'    => esc_html__( 'Overlay color', 'tm-robin' ),
			        'param_name' => 'overlay',
			        'value'      => 'rgba( 255, 255, 255, 0.6 )',
			        'dependency' => array(
				        'element' => 'effect',
				        'value'   => array( 'overlay' ),
			        ),
		        ),
				TM_Robin_VC::get_param( 'css' ),
	        ),
        ) );
