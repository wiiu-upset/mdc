<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'  => esc_html__( 'Breadcrumbs', 'tm-robin' ),
	'id'     => 'panel_breadcrumbs',
	'icon'   => 'fa fa-angle-double-right',
	'fields' => array(

		array(
			'id'       => 'breadcrumbs',
			'type'     => 'switch',
			'title'    => esc_html__( 'Breadcrumbs', 'tm-robin' ),
			'subtitle' => esc_html__( 'Displays a full chain of links to the current page.', 'tm-robin' ),
			'default'  => true,
		),

		array(
			'id'       => 'post_nav',
			'type'     => 'switch',
			'title'    => esc_html__( 'Post Navigation', 'tm-robin' ),
			'subtitle' => esc_html__( 'Enabling this option will display the post navigation on single post or single product page.', 'tm-robin' ),
			'default'  => true,
			'required'    => array(
				'breadcrumbs',
				'=',
				true,
			),
		),

		array(
			'id'      => 'breadcrumbs_position',
			'type'    => 'button_set',
			'title'   => esc_html__( 'Breadcrumbs Position', 'tm-robin' ),
			'options' => array(
				'inside' => esc_html__( 'Inside Page Title', 'tm-robin' ),
				'below'  => esc_html__( 'Below Page Title', 'tm-robin' ),
			),
			'default' => 'inside',
		),

		array(
			'id'          => 'breadcrumbs_text_color',
			'type'        => 'color',
			'transparent' => false,
			'title'       => esc_html__( 'Breadcrumbs Text Color', 'tm-robin' ),
			'output'      => $tm_robin_selectors['breadcrumbs_text_color'],
			'default'     => '#444444',
		),

		array(
			'id'          => 'breadcrumbs_seperator_color',
			'type'        => 'color',
			'transparent' => false,
			'title'       => esc_html__( 'Breadcrumbs Separator Color', 'tm-robin' ),
			'output'      => $tm_robin_selectors['breadcrumbs_seperator_color'],
			'default'     => '#444444',
		),

		array(
			'id'          => 'breadcrumbs_link_color',
			'type'        => 'color',
			'transparent' => false,
			'title'       => esc_html__( 'Breadcrumbs Link Color', 'tm-robin' ),
			'output'      => $tm_robin_selectors['breadcrumbs_link_color'],
			'default'     => '#444444',
		),

		array(
			'id'          => 'breadcrumbs_link_color_hover',
			'type'        => 'color',
			'transparent' => false,
			'title'       => esc_html__( 'Breadcrumbs Link Color: Hover', 'tm-robin' ),
			'output'      => $tm_robin_selectors['breadcrumbs_link_color_hover'],
			'default'     => PRIMARY_COLOR,
		),
	),
) );
