<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'  => esc_html__( 'Footer', 'tm-robin' ),
	'id'     => 'panel_footer',
	'icon'   => 'fa fa-angle-double-down ',
	'fields' => array(

		array(
			'id'       => 'footer_layout',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Footer columns', 'tm-robin' ),
			'subtitle' => esc_html__( 'Choose number of columns to display in footer area', 'tm-robin' ),
			'desc'     => wp_kses( sprintf( __( 'Note: Each column represents one Footer Sidebar in <a href="%s">Appearance -> Widgets</a> settings.', 'tm-robin' ), admin_url( 'widgets.php' ) ), wp_kses_allowed_html( 'post' ) ),
			'options'  => array(
				'1_12' => array(
					'title' => esc_html__( '1 Column', 'tm-robin' ),
					'img'   => get_template_directory_uri() . '/assets/admin/images/footer_col_1.png',
				),
				'2_6'  => array(
					'title' => esc_html__( '2 Columns', 'tm-robin' ),
					'img'   => get_template_directory_uri() . '/assets/admin/images/footer_col_2.png',
				),
				'3_4'  => array(
					'title' => esc_html__( '3 Columns', 'tm-robin' ),
					'img'   => get_template_directory_uri() . '/assets/admin/images/footer_col_3.png',
				),
				'4_3'  => array(
					'title' => esc_html__( '4 Columns', 'tm-robin' ),
					'img'   => get_template_directory_uri() . '/assets/admin/images/footer_col_4.png',
				),
			),
			'default'  => '3_4',
		),

		array(
			'id'      => 'footer_width',
			'type'    => 'button_set',
			'title'   => esc_html__( 'Footer Width', 'tm-robin' ),
			'options' => array(
				'standard' => esc_html__( 'Standard', 'tm-robin' ),
				'wide'     => esc_html__( 'Wide', 'tm-robin' ),

			),
			'default' => 'wide',
		),

		array(
			'id'      => 'footer_color_scheme',
			'type'    => 'button_set',
			'title'   => esc_html__( 'Footer Color Scheme', 'tm-robin' ),
			'options' => array(
				'custom'            => esc_html__( 'Custom', 'tm-robin' ),
				'light' => esc_html__( 'Light', 'tm-robin' ),
				'dark'  => esc_html__( 'Dark', 'tm-robin' ),

			),
			'default' => 'custom',
		),

		array(
			'id'          => 'footer_bgcolor',
			'type'        => 'color',
			'title'       => esc_html__( 'Background color', 'tm-robin' ),
			'subtitle'    => esc_html__( 'Specify footer background color', 'tm-robin' ),
			'transparent' => false,
			'output'      => $tm_robin_selectors['footer_bgcolor'],
			'default'     => SECONDARY_COLOR,
			'required'    => array(
				'footer_color_scheme',
				'=',
				'custom',
			),
		),

		array(
			'id'          => 'footer_color',
			'type'        => 'color',
			'title'       => esc_html__( 'Text color', 'tm-robin' ),
			'subtitle'    => esc_html__( 'This is the standard text color for footer', 'tm-robin' ),
			'transparent' => false,
			'output'      => $tm_robin_selectors['footer_color'],
			'default'     => '#999',
			'required'    => array(
				'footer_color_scheme',
				'=',
				'custom',
			),
		),

		array(
			'id'          => 'footer_accent_color',
			'type'        => 'color',
			'title'       => esc_html__( 'Accent color', 'tm-robin' ),
			'subtitle'    => esc_html__( 'This color will apply to buttons, links, etc...', 'tm-robin' ),
			'transparent' => false,
			'output'      => $tm_robin_selectors['footer_accent_color'],
			'default'     => '#ffffff',
			'required'    => array(
				'footer_color_scheme',
				'=',
				'custom',
			),
		),
	),
) );

require_once TM_ROBIN_OPTIONS_DIR . DS . 'footer' . DS . 'footer_copyright.php';
