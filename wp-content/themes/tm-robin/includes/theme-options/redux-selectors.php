<?php

define( 'PRIMARY_COLOR', '#fab200' );
define( 'SECONDARY_COLOR', '#111111' );
define( 'TERTIARY_COLOR', '#111111' );

/**
 * Get CSS selectors for Redux ouput
 */
if ( ! function_exists( ' tm_robin_get_selectors ' ) ) {
	function tm_robin_get_selectors() {
		return apply_filters( 'tm_robin_get_selectors', array(
			'primary_color' => array(
				'color' => TM_Robin_Helper::text2line( '.text-hightlight, 
					.select2-container--default .select2-results__option--highlighted[aria-selected],
					#woo-quick-view .quickview-loading span:before,
					.isw-swatches .isw-swatch--isw_text .isw-term:not(.isw-disabled):hover,
					.product-style-hover--info .add-to-cart-btn,
					.area-404__heading,
					.isw-swatches .isw-swatch--isw_text .isw-term.isw-selected,
					.footer-vc strong,
					.social-robin ul.social-links i:hover' ),

				'background-color' => TM_Robin_Helper::text2line( '.scroll-to-top,
					.post-tags .tagcloud a:hover, 
					.widget_tag_cloud .tagcloud a:hover, 
					.widget_product_tag_cloud .tagcloud a:hover,
					.widget_shopping_cart .button:hover, .widget_shopping_cart .button:focus,
					button.alt, input[type="submit"].alt, input[type="button"].alt, a.button.alt, .tm-button.button.alt,
					button.alt-button, input[type="submit"].alt-button, input[type="button"].alt-button, a.button.alt-button, .tm-button.button.alt,
					button:hover, input[type="submit"]:hover, input[type="button"]:hover, a.button:hover, .tm-button.button:hover,
					button:focus, input[type="submit"]:focus, input[type="button"]:focus, a.button:focus, .tm-button.button:focus,
					.woocommerce-checkout form.checkout_coupon .button,
					#customer_login .woocommerce-Button[name="register"],
					.woocommerce .order-again .button
					.tm-products-filter ul li > a:after,
					.page-header__prev-item:hover > a, .page-header__next-item:hover > a,
					.product-categories-menu .cat-item a:after,
					.woocommerce .order-again .button:hover,
					.woocommerce.single-product .product .product-tabs-wrapper .wc-tabs li:after,
					.woocommerce .order-again .button,
					.ui-slider .ui-slider-handle, .ui-slider .ui-slider-range,
					.tm-robin-mailchimp .title:after,
					.tm-robin-products-widget .product-buttons > div a:hover,
					.tm-robin-products-grid-hot.enable-carousel .slick-arrow:hover:before,
					.categories-icon-list > [class*=\' col-\']:hover,
					.tm-products-filter ul li > a:after,
					.wb-allview-formcnt .wb-allview-lettercnt .wb-wb-allview-letters:hover,
					.wb-allview-formcnt .wb-allview-lettercnt .wb-wb-allview-letters:focus,
					.wb-bx-controls.wb-bx-has-controls-direction .wb-bx-prev:hover,
					.wb-bx-controls.wb-bx-has-controls-direction .wb-bx-next:hover,
					.wpb-js-composer .vc_tta-tabs.vc_tta-style-robin.vc_tta-style-robin .vc_tta-tab > a:after,
					.wpb-js-composer .vc_tta-tabs.vc_tta-style-robin.vc_tta-style-robin .vc_tta-panel .vc_tta-panel-title > a:after,
					.tm-robin-simple-banner .third-line:after,
					.wb-bx-wrapper.wb-carousel-robin .wb-bx-pager.wb-bx-default-pager .wb-bx-pager-item a:hover,
					.wb-bx-wrapper.wb-carousel-robin .wb-bx-pager.wb-bx-default-pager .wb-bx-pager-item a.active,
					.site-footer .mc4wp-form button[type="submit"]:hover,
					.woocommerce .widget_layered_nav.yith-woocommerce-ajax-product-filter ul.yith-wcan-label li a:hover' ),

				'border-color' => TM_Robin_Helper::text2line( 'button.alt, input[type="submit"].alt, input[type="button"].alt, a.button.alt, .tm-button.button.alt,
					button.alt-button, input[type="submit"].alt-button, input[type="button"].alt-button, a.button.alt-button, .tm-button.button.alt,
					button:hover, input[type="submit"]:hover, input[type="button"]:hover, a.button:hover, .tm-button.button:hover,
					button:focus, input[type="submit"]:focus, input[type="button"]:focus, a.button:focus, .tm-button.button:focus,
					.post-tags .tagcloud a:hover,
					.widget_tag_cloud .tagcloud a:hover, 
					.widget_product_tag_cloud .tagcloud a:hover,
					.page-header__prev-item:hover > a, .page-header__next-item:hover > a,
					.widget_shopping_cart .button:hover, .widget_shopping_cart .button:focus,
					#customer_login .woocommerce-Button[name="register"],
					.woocommerce .order-again .button:hover,
					a.show-categories-menu,
					.wb-allview-formcnt .wb-allview-lettercnt .wb-wb-allview-letters:hover,
					.wb-allview-formcnt .wb-allview-lettercnt .wb-wb-allview-letters:focus,
				    .tm-products-widget .slick-arrow.small:focus,
				    .tm-robin-products-widget .slick-arrow.small:focus,
				    .site-footer .mc4wp-form button[type="submit"]:hover,
				    .ajax-results-wrapper .suggestion-title ins,
				    .ajax-results-wrapper .suggestion-sku ins' ),

			),

			'secondary_color' => array(
				'color' => TM_Robin_Helper::text2line( '.ajax-results-wrapper .suggestion-sku, 
					.ajax-results-wrapper .suggestion-price,
					.wpb-js-composer .vc_tta-tabs.vc_tta-style-robin.vc_tta-style-robin .vc_tta-tab.vc_active > a' ),

				'background-color' => TM_Robin_Helper::text2line( '.widget_shopping_cart .button.checkout:hover,
					.slick-arrow:hover, .slick-arrow:focus,
					.slick-dots li.slick-active button,
					.slick-dots button:hover,
					.widget_shopping_cart .button.checkout:focus,
					button.alt:hover, input[type="submit"].alt:hover, input[type="button"].alt:hover, a.button.alt:hover, .tm-button.button.alt:hover,
					button.alt:focus, input[type="submit"].alt:focus, input[type="button"].alt:focus, a.button.alt:focus, .tm-button.button.alt:focus,
					button.alt-button:hover, input[type="submit"].alt-button:hover, input[type="button"].alt-button:hover, a.button.alt-button:hover,
					button.alt-button:focus, input[type="submit"].alt-button:focus, input[type="button"].alt-button:focus, a.button.alt-button:focus,
					button, input[type="submit"], input[type="button"], a.button, .tm-button.button,
					.wb-bx-wrapper.wb-carousel-robin .wb-bx-pager.wb-bx-default-pager .wb-bx-pager-item a,
					.tm-loadmore-wrap.loading button.tm-loadmore-btn' ),


				'border-color' => TM_Robin_Helper::text2line( '.widget_shopping_cart .button.checkout:hover,
					.widget_shopping_cart .button.checkout:focus,
					button.alt:hover, input[type="submit"].alt:hover, input[type="button"].alt:hover, a.button.alt:hover, .tm-button.button.alt:hover,
					button.alt:focus, input[type="submit"].alt:focus, input[type="button"].alt:focus, a.button.alt:focus, .tm-button.button.alt:focus,
					button.alt-button:hover, input[type="submit"].alt-button:hover, input[type="button"].alt-button:hover, a.button.alt-button:hover,
					button.alt-button:focus, input[type="submit"].alt-button:focus, input[type="button"].alt-button:focus, a.button.alt-button:focus,
					button, input[type="submit"], input[type="button"], a.button, .tm-button.button,
					.tm-loadmore-wrap.loading button.tm-loadmore-btn' ),
			),

			'tertiary_color' => array(
				'background-color' => '',
			),

			'primary_font'   => array( 'body' ),
			'secondary_font' => array( 'font-family' => '.secondary-font, .tm-icon-box.secondary-font .title,.tm-icon-box.secondary-font a' ),
			'tertiary_font'  => array( 'font-family' => '.tertiary-font, .tm-icon-box.tertiary-font .title,.tm-icon-box.tertiary-font a' ),

			'heading_font' => array(
				TM_Robin_Helper::text2line( 'h1,h2,h3,h4,h5,h6,h1 a,h2 a,h3 a,h4 a,h5 a, h6 a,
					.woocommerce.single-product .product #review_form_wrapper #reply-title' ),
			),

			'link_color'       => array(
				'color'            => TM_Robin_Helper::text2line( 'a,
					.site-header .header-search.search-open > a.toggle,
					.cookie-buttons a.cookie-btn,
					.header-wishlist .add-to-cart-btn .button' ),
				'background-color' => TM_Robin_Helper::text2line( '' ),
			),
			'link_hover_color' => array(
				'color'            => TM_Robin_Helper::text2line( 'a:hover, a:focus,
					.site-mobile-menu .menu-show-all > a,
					.topbar .switcher .nice-select:hover .current,
					.topbar .switcher .nice-select:hover:after,
					.topbar .switcher .nice-select .option:hover,
					.cookie-wrapper a,
					.entry-meta .meta-author a,
					.shop-loop-head .switcher.active,
					.shop-loop-head .switcher:hover,
					.product-loop .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse a,
					.product-loop .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse a,
					.woocommerce.single-product .product .summary .product-meta-buttons > div a:hover,
					.woocommerce.single-product .product .summary table.product_meta td.value a:hover,
					.woocommerce.single-product .product .summary table.product-share td.value a:hover,
					.wc-tabs li.active > a,
					.category-grid-item:hover .product-category-content .category-link a,
					.category-grid-item:hover .product-category-content .category-link:after,
					.col-switcher a.active' ),
				'background-color' => TM_Robin_Helper::text2line( '.cookie-buttons a:hover' ),
				'border-color'     => TM_Robin_Helper::text2line( '.widget_calendar tbody td a:hover' ),
			),

			// Top bar
			'topbar_color'     => array( 'color' => '.topbar, .topbar a' ),
			'topbar_bgcolor'   => array( 'background-color' => '.topbar' ),
			'topbar_bdcolor'   => array( 'border-color' => '.topbar' ),

			'header_bgcolor'               => array( 'background-color' => '.site-header' ),
			'header_bdcolor'               => array( 'border-color' => '.site-header' ),

			// Page title
			'page_title_text_color'        => array(
				'color' => TM_Robin_Helper::text2line( '.page-title h1,
					.page-title a,
					.page-title .site-breadcrumbs,
					.page-title .site-breadcrumbs ul li:after,
					.page-title .site-breadcrumbs .insight_core_breadcrumb a,
					.page-title .site-breadcrumbs .insight_core_breadcrumb a:hover' ),
			),
			'page_subtitle_color'          => array( 'color' => '.page-title .page-subtitle' ),
			'page_title_bg_color'          => array( 'background-color' => '.page-title' ),
			'page_title_overlay_color'     => array( 'background-color' => '.page-title:before' ),
			'page_title_bg_image'          => array( 'background-image' => '.page-title' ),

			// Breadcrumbs
			'breadcrumbs_text_color'       => array( 'color' => '.site-breadcrumbs' ),
			'breadcrumbs_seperator_color'  => array( 'color' => '.site-breadcrumbs ul li:after' ),
			'breadcrumbs_link_color'       => array( 'color' => '.site-breadcrumbs .insight_core_breadcrumb a' ),
			'breadcrumbs_link_color_hover' => array( 'color' => '.site-breadcrumbs .insight_core_breadcrumb a:hover' ),

			// Mini cart
			'minicart_icon_color'          => array( 'color' => '.header-minicart > a.toggle > i, .site-header.sticky-header .header-minicart > a.toggle > i' ),
			'minicart_text_color'          => array( 'color' => '.header-minicart > a.toggle > span, .site-header.sticky-header .header-minicart > a.toggle > span' ),
			'minicart_count_bg_color'      => array( 'background-color' => '.header-minicart > a.toggle > span, .site-header.sticky-header .header-minicart > a.toggle > span' ),

			// search
			'search_color'                 => array( 'color' => '.header-search > a.toggle, .site-header.sticky-header .header-search > a.toggle' ),

			// wishlist
			'wishlist_icon_color'          => array( 'color' => '.header-wishlist > a.toggle > i, .site-header.sticky-header .header-wishlist > a.toggle > i' ),
			'wishlist_text_color'          => array( 'color' => '.header-wishlist > a.toggle > span, .site-header.sticky-header .header-wishlist > a.toggle > span' ),
			'wishlist_count_bg_color'      => array( 'background-color' => '.header-wishlist > a.toggle > span, .site-header.sticky-header .header-wishlist > a.toggle > span' ),

			//'' => array(),

			'site_menu_font'           => array( '.site-menu .menu > ul > li > a, .site-menu .menu > li > a' ),
			'site_menu_items_color'    => array( 'color' => '.site-menu .menu > ul > li > a, .site-menu .menu > li > a, .site-header.sticky-header .site-menu .menu > ul > li > a, .site-header.sticky-header .site-menu .menu > li > a' ),
			'site_menu_subitems_color' => array( 'color' => '.site-menu .menu > ul > li .children a, .site-menu .menu > li .sub-menu a, .site-header.sticky-header .site-menu .menu > ul > li .children a, .site-header.sticky-header .site-menu .menu > li .sub-menu a' ),
			'site_menu_bgcolor'        => array( 'background-color' => '.site-header .site-menu-wrap' ),

			'footer_bgcolor'      => array( 'background-color' => '.site-footer' ),
			'footer_color'        => array( 'color' => '.site-footer, .site-footer a' ),
			'footer_accent_color' => array( 'color' => '.site-footer .widget-title, .site-footer a:hover, .site-footer .entry-title a, .widget-title:after' ),

			'footer_copyright_bgcolor'          => array( 'background-color' => '.site-footer .site-copyright' ),
			'footer_copyright_color'            => array( 'color' => '.site-footer .site-copyright' ),
			'footer_copyright_link_color'       => array( 'color' => '.site-footer .site-copyright a' ),

			// Product buttons color
			'product_overlay_color'             => array( 'background-color' => TM_Robin_Helper::text2line( '.product-loop.product-style-hover--info .product-thumb > a:before' ) ),
			'product_accent_color'              => array(
				'color' => TM_Robin_Helper::text2line(
					'.product-loop.product-style-hover--info,
					.product-loop.product-style-hover--info a,
					.product-loop.product-style-hover--info .product-info .product-title > a,
					.product-loop.product-style-hover--info .price,
					.product-loop.product-style-hover--info .yith-wcwl-add-to-wishlist' )
			),
			'product_buttons_bg_color'          => array( 'background-color' => TM_Robin_Helper::text2line( '.product-loop .product-buttons.product-buttons--custom > div' ) ),
			'product_buttons_hover_bg_color'    => array( 'background-color' => TM_Robin_Helper::text2line( '.product-loop .product-buttons.product-buttons--custom > div:hover' ) ),
			'product_buttons_bd_color'          => array(
				'border-color' => TM_Robin_Helper::text2line(
					'.product-loop .product-buttons.product-buttons--custom .quick-view-btn, 
					.product-loop .product-buttons.product-buttons--custom .compare-btn' ) ),
			'product_buttons_hover_bd_color'    => array(
				'border-color' => TM_Robin_Helper::text2line(
					'.product-loop .product-buttons.product-buttons--custom .quick-view-btn:hover, 
					.product-loop .product-buttons.product-buttons--custom .compare-btn:hover' ) ),
			'product_buttons_color'             => array( 'color' => TM_Robin_Helper::text2line( '.product-loop .product-buttons.product-buttons--custom a' ) ),
			'product_buttons_hover_color'       => array( 'color' => TM_Robin_Helper::text2line( '.product-loop .product-buttons.product-buttons--custom > div:hover a' ) ),
		) );
	}
}

$tm_robin_selectors = tm_robin_get_selectors();
