<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'      => esc_html__( 'General', 'tm-robin' ),
	'id'         => 'section_general',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'post_meta',
			'type'     => 'switch',
			'title'    => esc_html__( 'Post Meta', 'tm-robin' ),
			'subtitle' => esc_html__( 'Turn on to display post meta on blog posts', 'tm-robin' ),
			'default'  => true,
		),
	),
) );
