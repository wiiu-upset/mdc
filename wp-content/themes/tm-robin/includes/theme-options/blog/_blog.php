<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title' => esc_html__( 'Blog', 'tm-robin' ),
	'id'    => 'panel_blog',
	'icon'  => 'fa fa-pencil'
) );

require_once TM_ROBIN_OPTIONS_DIR . DS . 'blog' . DS . 'blog_general.php';
require_once TM_ROBIN_OPTIONS_DIR . DS . 'blog' . DS . 'blog_archive.php';
require_once TM_ROBIN_OPTIONS_DIR . DS . 'blog' . DS . 'blog_single.php';
