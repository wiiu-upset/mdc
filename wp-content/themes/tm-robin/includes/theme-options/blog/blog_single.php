<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'      => esc_html__( 'Blog Single Post', 'tm-robin' ),
	'id'         => 'section_single_post',
	'subsection' => true,
	'fields'     => array(

		array(
			'id'       => 'post_sidebar_config',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Post Sidebar', 'tm-robin' ),
			'subtitle' => esc_html__( 'Controls the position of sidebars for the single post pages.', 'tm-robin' ),
			'options'  => array(
				'left'  => array(
					'title' => esc_html__( 'Left', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . DS . '2cl.png',
				),
				'no'    => array(
					'title' => esc_html__( 'Disable', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . DS . '1c.png',
				),
				'right' => array(
					'title' => esc_html__( 'Right', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . DS . '2cr.png',
				),
			),
			'default'  => 'right',
		),

		array(
			'id'       => 'post_show_tags',
			'type'     => 'switch',
			'title'    => esc_html__( 'Tags', 'tm-robin' ),
			'subtitle' => esc_html__( 'Turn on to display the tags.', 'tm-robin' ),
			'default'  => true,
		),

		array(
			'id'       => 'post_show_author_info',
			'type'     => 'switch',
			'title'    => esc_html__( 'Author Info Box', 'tm-robin' ),
			'subtitle' => esc_html__( 'Turn on to display the author info box below posts.', 'tm-robin' ),
			'default'  => true,
		),

		array(
			'id'       => 'post_show_related',
			'type'     => 'switch',
			'title'    => esc_html__( 'Related Posts', 'tm-robin' ),
			'subtitle' => esc_html__( 'Turn on to display related posts.', 'tm-robin' ),
			'default'  => true,
		),

		array(
			'id'       => 'post_show_share',
			'type'     => 'switch',
			'title'    => esc_html__( 'Social Sharing Box', 'tm-robin' ),
			'subtitle' => esc_html__( 'Turn on to display the social sharing box.', 'tm-robin' ),
			'default'  => true,
		),

		array(
			'id'       => 'post_share_links',
			'type'     => 'checkbox',
			'title'    => esc_html__( 'Share the post on ', 'tm-robin' ),
			'options'  => array(
				'facebook'  => '<i class="fa fa-facebook"></i>&nbsp;&nbsp;' . esc_html__( 'Facebook', 'tm-robin' ),
				'twitter'   => '<i class="fa fa-twitter"></i>&nbsp;&nbsp;' . esc_html__( 'Twitter', 'tm-robin' ),
				'google'    => '<i class="fa fa-google-plus"></i>&nbsp;&nbsp;' . esc_html__( 'Google+', 'tm-robin' ),
				'pinterest' => '<i class="fa fa-pinterest"></i>&nbsp;&nbsp;' . esc_html__( 'Pinterest', 'tm-robin' ),
				'email'     => '<i class="fa fa-envelope-o"></i>&nbsp;&nbsp;' . esc_html__( 'Email', 'tm-robin' ),
			),
			'default'  => array(
				'facebook'  => '1',
				'twitter'   => '1',
				'google'    => '1',
				'pinterest' => '1',
				'email'     => '1',
			),
			'required' => array(
				array( 'post_show_share', '=', array( true ) ),
			),
		),

	),
) );
