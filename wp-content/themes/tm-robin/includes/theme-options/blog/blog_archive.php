<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'      => esc_html__( 'Blog Archive', 'tm-robin' ),
	'id'         => 'section_blog_archive',
	'subsection' => true,
	'fields'     => array(

		array(
			'id'       => 'archive_sidebar_config',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Archive Sidebar Position', 'tm-robin' ),
			'subtitle' => esc_html__( 'Controls the position of sidebars for the archive pages.', 'tm-robin' ),
			'options'  => array(
				'left'        => array(
					'title' => esc_html__( 'Left', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . DS . '2cl.png',
				),
				'no' => array(
					'title' => esc_html__( 'Disable', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . DS . '1c.png',
				),
				'right'       => array(
					'title' => esc_html__( 'Right', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . DS . '2cr.png',
				),
			),
			'default'  => 'right',
		),

		array(
			'id'       => 'archive_display_type',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Archive Display Type', 'tm-robin' ),
			'subtitle' => esc_html__( 'Select the display type.', 'tm-robin' ),
			'options'  => array(
				'standard' => esc_html__( 'Standard', 'tm-robin' ),
				'grid'     => esc_html__( 'Grid', 'tm-robin' ),
				'masonry'  => esc_html__( 'Masonry', 'tm-robin' ),
			),
			'default'  => 'standard',
		),

		array(
			'id'       => 'archive_sidebar',
			'type'     => 'select',
			'title'    => esc_html__( 'Archive Sidebar', 'tm-robin' ),
			'subtitle' => esc_html__( 'Choose the sidebar for archive pages.', 'tm-robin' ),
			'data'     => 'sidebars',
			'default'  => 'sidebar',
		),

		array(
			'id'       => 'archive_content_output',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Archive Content Output', 'tm-robin' ),
			'subtitle' => esc_html__( 'Select if you\'d like to output the content or excerpt on archive pages.', 'tm-robin' ),
			'options'  => array(
				'excerpt' => esc_html__( 'Excerpt', 'tm-robin' ),
				'content' => esc_html__( 'Content', 'tm-robin' ),
			),
			'default'  => 'excerpt',
		),

		array(
			'id'                => 'excerpt_length',
			'type'              => 'text',
			'title'             => esc_html__( 'Excerpt Length', 'tm-robin' ),
			'subtitle'          => sprintf( esc_html__( 'Controls the number of words of the post excerpt (from 0 to %s words)', 'tm-robin' ), apply_filters( 'tm_robin_max_excerpt_length', 500 ) ),
			'default'           => 30,
			'display_value'     => 'label',
			'validate_callback' => 'tm_robin_validate_excerpt_callback',
			'required'          => array(
				'archive_content_output',
				'=',
				'excerpt',
			),
		),

	),
) );

if ( ! function_exists( 'tm_robin_validate_excerpt_callback' ) ) {
	function tm_robin_validate_excerpt_callback( $field, $value, $existing_value ) {
		$error = false;

		if ( ! is_numeric( $value ) ) {

			$error = true;

			$value        = $existing_value;
			$field['msg'] = esc_html__( 'You must provide a numerical value for this option.', 'tm-robin' );

		} elseif ( $value < 0 || $value > apply_filters( 'tm_robin_max_excerpt_length', 500 ) ) {

			$error = true;

			$value        = $existing_value;
			$field['msg'] = sprintf( esc_html__( 'The excerpt length must be from 0 to %s words.', 'tm-robin' ), apply_filters( 'tm_robin_max_excerpt_length', 500 ) );
		}

		$return['value'] = $value;

		if ( $error ) {
			$return['error'] = $field;
		}

		return $return;
	}
}

