<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'  => esc_html__( 'Social', 'tm-robin' ),
	'id'     => 'panel_social',
	'icon'   => 'fa fa-share-alt',
	'fields' => array(
		array(
			'id'         => 'social_links',
			'type'       => 'repeater',
			'title'      => esc_html__( 'Social Items', 'tm-robin' ),
			'item_name'  => esc_html__( 'Items', 'tm-robin' ),
			'bind_title' => 'icon',
			'fields'     => array(
				array(
					'id'      => 'icon',
					'title'   => esc_html__( 'Icon', 'tm-robin' ),
					'type'    => 'select',
					'desc'    => esc_html__( 'Select a social network to automatically add its icon', 'tm-robin' ),
					'options' => TM_Robin_Helper::social_icons(),
					'default' => 'none',
				),
				array(
					'id'          => 'icon_class',
					'title'       => esc_html__( 'Custom Icon', 'tm-robin' ),
					'type'        => 'text',
					'placeholder' => esc_html__( 'Font Awesome Class', 'tm-robin' ),
					'desc'        => wp_kses( sprintf( __( 'Use your custom icon. You can find Font Awesome icon class <a target="_blank" href="%s">here</a>.', 'tm-robin' ), 'http://fontawesome.io/cheatsheet/' ), array(
						'a' => array(
							'href'   => array(),
							'target' => array(),
						),
					) ),
				),
				array(
					'id'          => 'url',
					'type'        => 'text',
					'title'       => esc_html__( 'Link (URL)', 'tm-robin' ),
					'placeholder' => esc_html__( 'http://', 'tm-robin' ),
					'desc'        => esc_html__( 'Add an URL to your social network', 'tm-robin' ),
				),
				array(
					'id'          => 'title',
					'title'       => esc_html__( 'Title', 'tm-robin' ),
					'type'        => 'text',
					'placeholder' => esc_html__( 'Title', 'tm-robin' ),
					'desc'        => esc_html__( 'Insert your custom title here', 'tm-robin' ),
				),
				array(
					'id'    => 'custom_class',
					'title' => esc_html__( 'Custom CSS class', 'tm-robin' ),
					'type'  => 'text',
					'desc'  => esc_html__( 'Insert your custom CSS class here', 'tm-robin' ),
				),
			),
		),
	),
) );
