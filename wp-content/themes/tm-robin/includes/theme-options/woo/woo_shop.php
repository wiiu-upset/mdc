<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'      => esc_html__( 'Shop Page', 'tm-robin' ),
	'id'         => 'section_shop',
	'subsection' => true,
	'fields'     => array(

		array(
			'id'       => 'shop_sidebar_config',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Shop Sidebar Position', 'tm-robin' ),
			'subtitle' => esc_html__( 'Controls the position of sidebars for the shop pages.', 'tm-robin' ),
			'options'  => array(
				'left'  => array(
					'title' => esc_html__( 'Left', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . DS . '2cl.png',
				),
				'no'    => array(
					'title' => esc_html__( 'Disable', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . DS . '1c.png',
				),
				'right' => array(
					'title' => esc_html__( 'Right', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . DS . '2cr.png',
				),
			),
			'default'  => 'left',
		),

		array(
			'id'       => 'shop_sidebar',
			'type'     => 'select',
			'title'    => esc_html__( 'Shop Sidebar', 'tm-robin' ),
			'subtitle' => esc_html__( 'Choose the sidebar for archive pages.', 'tm-robin' ),
			'data'     => 'sidebars',
			'default'  => 'sidebar-shop',
		),

		array(
			'id'      => 'full_width_shop',
			'type'    => 'switch',
			'title'   => esc_html__( 'Full width shop', 'tm-robin' ),
			'default' => false,
		),

		array(
			'id'      => 'shop_categories_menu',
			'type'    => 'switch',
			'title'   => esc_html__( 'Show categories menu in the page title', 'tm-robin' ),
			'default' => true,
		),

		array(
			'id'      => 'column_switcher',
			'type'    => 'switch',
			'title'   => esc_html__( 'Show column switcher', 'tm-robin' ),
			'default' => true,
		),

		array(
			'id'       => 'shop_view_mode',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Shop View Mode', 'tm-robin' ),
			'subtitle' => esc_html__( 'Select the view mode for the shop', 'tm-robin' ),
			'options'  => array(
				'grid' => esc_html__( 'Grid', 'tm-robin' ),
				'list' => esc_html__( 'List', 'tm-robin' ),
			),
			'default'  => 'grid',
		),

		array(
			'id'       => 'categories_layout',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Categories Layout', 'tm-robin' ),
			'subtitle' => esc_html__( 'Select layout for the categories block', 'tm-robin' ),
			'options'  => array(
				'grid'     => esc_html__( 'Grid', 'tm-robin' ),
				'carousel' => esc_html__( 'Carousel', 'tm-robin' ),
			),
			'default'  => 'carousel',
		),

		array(
			'id'       => 'categories_columns',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Categories columns', 'tm-robin' ),
			'subtitle' => esc_html__( 'How many categories you want to show per row on the shop page?', 'tm-robin' ),
			'options'  => array(
				2 => '2',
				3 => '3',
				4 => '4',
				5 => '5',
				6 => '6',
			),
			'default'  => 3,
		),

		array(
			'id'      => 'shop_ajax_on',
			'type'    => 'switch',
			'title'   => esc_html__( 'Enable AJAX functionality on shop', 'tm-robin' ),
			'default' => true,
		),
	),
) );
