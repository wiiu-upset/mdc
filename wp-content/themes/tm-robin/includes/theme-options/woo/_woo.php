<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title' => esc_html__( 'Shop', 'tm-robin' ),
	'id'    => 'panel_woo',
	'icon'  => 'fa fa-shopping-basket'
) );

require_once TM_ROBIN_OPTIONS_DIR . DS . 'woo' . DS . 'woo_general.php';
require_once TM_ROBIN_OPTIONS_DIR . DS . 'woo' . DS . 'woo_shop.php';
require_once TM_ROBIN_OPTIONS_DIR . DS . 'woo' . DS . 'woo_product.php';
