<?php
Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'      => esc_html__( 'Single Product Page', 'tm-robin' ),
	'id'         => 'section_product',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'product_sidebar_config',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Single Product Page Sidebar Position', 'tm-robin' ),
			'subtitle' => esc_html__( 'Controls the position of sidebars for the product pages.', 'tm-robin' ),
			'options'  => array(
				'left'        => array(
					'title' => esc_html__( 'Left', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . '/2cl.png',
				),
				'no' => array(
					'title' => esc_html__( 'Disable', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . '/1c.png',
				),
				'right'       => array(
					'title' => esc_html__( 'Right', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . '/2cr.png',
				),
			),
			'default'  => 'no',
		),

		array(
			'id'      => 'product_ajax_add_to_cart',
			'type'    => 'switch',
			'title'   => esc_html__( 'Enable AJAX add to cart on single product page', 'tm-robin' ),
			'default' => true,
		),

		array(
			'id'      => 'product_thumbnails_position',
			'type'    => 'button_set',
			'title'   => esc_html__( 'Thumbnails Position', 'tm-robin' ),
			'options' => array(
				'left'   => esc_html__( 'Left', 'tm-robin' ),
				'bottom' => esc_html__( 'Bottom', 'tm-robin' ),
			),
			'default' => 'left',
		),

		array(
			'id'      => 'product_page_layout',
			'type'    => 'button_set',
			'title'   => esc_html__( 'Product Page Layout', 'tm-robin' ),
			'options' => array(
				'basic'            => esc_html__( 'Basic', 'tm-robin' ),
				'fullwidth'        => esc_html__( 'Fullwidth', 'tm-robin' ),
				'sticky'           => esc_html__( 'Sticky Details', 'tm-robin' ),
				'sticky-fullwidth' => esc_html__( 'Sticky Details (Fullwidth)', 'tm-robin' ),
			),
			'default' => 'basic',
		),

		array(
			'id'       => 'product_zoom_on',
			'type'     => 'switch',
			'title'    => esc_html__( 'Enable zoom for product image', 'tm-robin' ),
			'subtitle' => esc_html__( 'If you use Product Page Layout as full width, you should use images of 1000px or more in width. Otherwise, this function will not work. Also, this function does not work with External / Affiliate product.', 'tm-robin' ),
			'default'  => true,
		),

		/* @since 1.2 */
		array(
			'id'       => 'product_wishlist_on',
			'type'     => 'switch',
			'title'    => esc_html__( 'Show "Wishlist" button', 'tm-robin' ),
			'subtitle' => wp_kses( sprintf( __( 'This feature requires <a href="%s" target="_blank">YITH WooCommerce Wishlist</a> plugin.', 'tm-robin' ), esc_url( 'https://wordpress.org/plugins/yith-woocommerce-wishlist/' ) ), array(
				'a' => array(
					'href'   => array(),
					'target' => array(),
				),
			) ),
			'default'  => true,
		),

		array(
			'id'       => 'product_compare_on',
			'type'     => 'switch',
			'title'    => esc_html__( 'Show "Compare" button', 'tm-robin' ),
			'subtitle' => wp_kses( sprintf( __( 'This feature requires <a href="%s" target="_blank">YITH WooCommerce Compare</a> plugin.', 'tm-robin' ), esc_url( 'https://wordpress.org/plugins/yith-woocommerce-compare/' ) ), array(
				'a' => array(
					'href'   => array(),
					'target' => array(),
				),
			) ),
			'default'  => true,
		),

		array(
			'id'       => 'product_show_share',
			'type'     => 'switch',
			'title'    => esc_html__( 'Social Sharing Box', 'tm-robin' ),
			'subtitle' => esc_html__( 'Turn on to display the social sharing box.', 'tm-robin' ),
			'default'  => true,
		),

		array(
			'id'       => 'product_share_links',
			'type'     => 'checkbox',
			'title'    => esc_html__( 'Share the product on ', 'tm-robin' ),
			'options'  => array(
				'facebook'  => '<i class="fa fa-facebook"></i>&nbsp;&nbsp;' . esc_html__( 'Facebook', 'tm-robin' ),
				'twitter'   => '<i class="fa fa-twitter"></i>&nbsp;&nbsp;' . esc_html__( 'Twitter', 'tm-robin' ),
				'google'    => '<i class="fa fa-google-plus"></i>&nbsp;&nbsp;' . esc_html__( 'Google+', 'tm-robin' ),
				'pinterest' => '<i class="fa fa-pinterest"></i>&nbsp;&nbsp;' . esc_html__( 'Pinterest', 'tm-robin' ),
				'email'     => '<i class="fa fa-envelope-o"></i>&nbsp;&nbsp;' . esc_html__( 'Email', 'tm-robin' ),
			),
			'default'  => array(
				'facebook'  => '1',
				'twitter'   => '1',
				'google'    => '1',
				'pinterest' => '1',
				'email'     => '1',
			),
			'required' => array(
				array( 'product_show_share', '=', array( true ) ),
			),
		),

		array(
			'id'            => 'product_related',
			'type'          => 'slider',
			'title'         => esc_html__( 'Related Products', 'tm-robin' ),
			'subtitle'      => esc_html__( 'Related products is a section on some templates that pulls other products from your store that share the same tags or categories as the current product. Set 0 to hide this section.', 'tm-robin' ),
			'min'           => 0,
			'max'           => 24,
			'default'       => 8,
			'display_value' => 'label',
		),
	),
) );
