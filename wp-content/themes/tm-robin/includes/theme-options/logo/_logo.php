<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'  => __( 'Logo & Favicon', 'tm-robin' ),
	'id'     => 'panel_logo',
	'icon'   => 'fa fa-picture-o',
	'fields' => array(

		array(
			'id'      => 'logo',
			'type'    => 'media',
			'desc'    => esc_html__( 'Upload image: png, jpg or gif file', 'tm-robin' ),
			'title'   => esc_html__( 'Logo Image', 'tm-robin' ),
			'default' => array(
				'url' => TM_ROBIN_IMAGES . '/logo.png'
			),
		),
		array(
			'id'       => 'logo_alt',
			'type'     => 'media',
			'subtitle' => esc_html__( 'for the header above the content', 'tm-robin' ),
			'desc'     => esc_html__( 'Upload image: png, jpg or gif file', 'tm-robin' ),
			'title'    => esc_html__( 'Alternative Logo Image ', 'tm-robin' ),
			'default' => array(
				'url' => TM_ROBIN_IMAGES . '/logo-alt.png'
			),
		),
		array(
			'id'    => 'logo_mobile',
			'type'  => 'media',
			'title' => esc_html__( 'Logo in mobile devices', 'tm-robin' ),
			'desc'  => esc_html__( 'Upload image: png, jpg or gif file', 'tm-robin' ),
			'default' => array(
				'url' => TM_ROBIN_IMAGES . '/logo.png'
			),
		),
		array(
			'id'            => 'logo_width',
			'type'          => 'slider',
			'title'         => esc_html__( 'Logo width', 'tm-robin' ),
			'subtitle'      => esc_html__( 'Set width for logo area in the header (in %)', 'tm-robin' ),
			'default'       => 10,
			'min'           => 1,
			'max'           => 50,
			'step'          => 1,
			'display_value' => 'label',
			'required'      => array(
				array( 'header', '=', array( 'base', 'wide' ) ),
			),
		),

		array(
			'id'    => 'favicon',
			'type'  => 'media',
			'title' => esc_html__( 'Favicon', 'tm-robin' ),
			'desc'  => esc_html__( 'Upload image: png, jpg or gif file. Optimal dimension: 32px x 32px', 'tm-robin' ),
		),

		array(
			'id'       => 'apple_touch',
			'type'     => 'media',
			'title'    => esc_html__( 'Apple touch icon', 'tm-robin' ),
			'subtitle' => esc_html__( 'The Apple Touch Icon is a file used for a web page icon on the Apple iPhone, iPod Touch, and iPad. When someone bookmarks your web page or adds your web page to their home screen this icon is used.', 'tm-robin' ),
			'desc'     => esc_html__( 'File must be .png format. Optimal dimension: 152px x 152px', 'tm-robin' ),
		),
	),
) );
