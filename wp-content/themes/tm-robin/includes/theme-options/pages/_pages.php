<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'  => __( 'Pages', 'tm-robin' ),
	'id'     => 'panel_pages',
	'icon'   => 'fa fa-credit-card',
	'fields' => array(

		array(
			'id'       => 'page_sidebar_config',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Page Sidebar Position', 'tm-robin' ),
			'subtitle' => esc_html__( 'Controls the position of sidebars for page.', 'tm-robin' ),
			'options'  => array(
				'left'  => array(
					'title' => esc_html__( 'Left', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . DS . '2cl.png',
				),
				'no'    => array(
					'title' => esc_html__( 'Disable', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . DS . '1c.png',
				),
				'right' => array(
					'title' => esc_html__( 'Right', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . DS . '2cr.png',
				),
			),
			'default'  => 'no',
		),


		array(
			'id'       => 'search_sidebar_config',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Search Sidebar Position', 'tm-robin' ),
			'subtitle' => esc_html__( 'Controls the position of sidebars for search result page.', 'tm-robin' ),
			'options'  => array(
				'left'  => array(
					'title' => esc_html__( 'Left', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . DS . '2cl.png',
				),
				'no'    => array(
					'title' => esc_html__( 'Disable', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . DS . '1c.png',
				),
				'right' => array(
					'title' => esc_html__( 'Right', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . DS . '2cr.png',
				),
			),
			'default'  => 'right',
		),

		array(
			'id'       => 'search_sidebar',
			'type'     => 'select',
			'title'    => esc_html__( 'Search Sidebar', 'tm-robin' ),
			'subtitle' => esc_html__( 'Choose the sidebar for search result page.', 'tm-robin' ),
			'data'     => 'sidebars',
			'default'  => 'sidebar',
		),

		array(
			'id'       => '404_bg',
			'type'     => 'background',
			'title'    => esc_html__( '404 Background', 'tm-robin' ),
			'subtitle' => esc_html__( 'Set background image or color for 404 page.', 'tm-robin' ),
			'output'   => array( '.area-404' ),
			'default'  => array(
				'background-image' => TM_ROBIN_IMAGES . DS . '404-bg.jpg',
			),
		),

	),
) );
