<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'            => esc_html__( 'Style', 'tm-robin' ),
	'id'               => 'panel_style',
	'customizer_width' => '400px',
	'icon'             => 'fa fa-paint-brush',
) );

require_once TM_ROBIN_OPTIONS_DIR . DS . 'style' . DS . 'style_typo.php';
require_once TM_ROBIN_OPTIONS_DIR . DS . 'style' . DS . 'style_colors.php';
