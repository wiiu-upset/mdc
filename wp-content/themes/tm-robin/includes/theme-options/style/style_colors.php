<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'            => __( 'Colors', 'tm-robin' ),
	'id'               => 'section_colors',
	'subsection'       => true,
	'customizer_width' => '450px',
	'fields'           => array(
		array(
			'id'    => 'info_color',
			'type'  => 'info',
			'style' => 'warning',
			'title' => esc_html__( 'IMPORTANT NOTE', 'tm-robin' ),
			'icon'  => 'el-icon-info-sign',
			'desc'  => esc_html__( 'This tab contains general color options. Additional color options for specific areas, can be found within other tabs. Example: For menu color options go to the menu tab.', 'tm-robin' ),
		),

		array(
			'id'          => 'primary_color',
			'type'        => 'color',
			'transparent' => false,
			'title'       => esc_html__( 'Primary Color', 'tm-robin' ),
			'default'     => PRIMARY_COLOR,
			'validate'    => 'color',
			'output'      => $tm_robin_selectors['primary_color'],
		),

		array(
			'id'          => 'secondary_color',
			'type'        => 'color',
			'transparent' => false,
			'title'       => esc_html__( 'Secondary Color', 'tm-robin' ),
			'default'     => SECONDARY_COLOR,
			'validate'    => 'color',
			'output'      => $tm_robin_selectors['secondary_color'],
		),

		array(
			'id'          => 'tertiary_color',
			'type'        => 'color',
			'transparent' => false,
			'title'       => esc_html__( 'Tertiary Color', 'tm-robin' ),
			'default'     => TERTIARY_COLOR,
			'validate'    => 'color',
			'output'      => $tm_robin_selectors['tertiary_color'],
		),

		array(
			'id'          => 'link_color',
			'type'        => 'color',
			'transparent' => false,
			'title'       => esc_html__( 'Links Color', 'tm-robin' ),
			'subtitle'    => esc_html__( 'Controls the color of all text links.', 'tm-robin' ),
			'default'     => '#666666',
			'validate'    => 'color',
			'output'      => $tm_robin_selectors['link_color'],
		),
		array(
			'id'          => 'link_hover_color',
			'type'        => 'color',
			'transparent' => false,
			'title'       => esc_html__( 'Links Hover Color', 'tm-robin' ),
			'subtitle'    => esc_html__( 'Controls the color of all text links when hover.', 'tm-robin' ),
			'default'     => PRIMARY_COLOR,
			'validate'    => 'color',
			'output'      => $tm_robin_selectors['link_hover_color'],
		),
	),
) );
