<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'  => esc_html__( 'Cookie Notice', 'tm-robin' ),
	'id'     => 'panel_cookie',
	'icon'   => 'fa fa-sticky-note-o',
	'fields' => array(
		array(
			'id'       => 'cookie_on',
			'type'     => 'switch',
			'title'    => esc_html__( 'Enable Cookie Notice', 'tm-robin' ),
			'subtitle' => esc_html__( 'Cookie Notice allows you to elegantly inform users that your site uses cookies and to comply with the EU cookie law regulations. Turn on this option and user will see info box at the bottom of the page that your web-site is using cookies.', 'tm-robin' ),
			'default'  => true,
		),
		array(
			'id'       => 'cookie_expires',
			'type'     => 'select',
			'title'    => esc_html__( 'Cookie expiry', 'tm-robin' ),
			'subtitle' => esc_html__( 'The ammount of time that cookie should be stored for', 'tm-robin' ),
			'options'  => array(
				1       => esc_html__( '1 day', 'tm-robin' ),
				7       => esc_html__( '1 week', 'tm-robin' ),
				30      => esc_html__( '1 month', 'tm-robin' ),
				90      => esc_html__( '3 months', 'tm-robin' ),
				180     => esc_html__( '6 months', 'tm-robin' ),
				365     => esc_html__( '1 year', 'tm-robin' ),
				3650000 => esc_html__( 'Infinity', 'tm-robin' ),
			),
			'default' => 30
		),
		array(
			'id'       => 'cookie_message',
			'type'     => 'editor',
			'title'    => esc_html__( 'Message', 'tm-robin' ),
			'subtitle' => esc_html__( 'Place here some information about cookies usage that will be shown in the notice', 'tm-robin' ),
			'default'  => esc_html__( 'We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it.', 'tm-robin' ),
		),
		array(
			'id'       => 'cookie_policy_page',
			'type'     => 'select',
			'title'    => __( 'Page with details', 'tm-robin' ),
			'subtitle' => __( 'Choose page that will contain detailed information about your Privacy Policy', 'tm-robin' ),
			'data'     => 'pages',
		),
	),
) );
