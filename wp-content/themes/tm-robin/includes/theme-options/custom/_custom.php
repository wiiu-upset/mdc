<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title' => esc_html__( 'Custom Code', 'tm-robin' ),
	'id'    => 'panel_custom',
	'icon'  => 'fa fa-code'
) );

require_once TM_ROBIN_OPTIONS_DIR . DS . 'custom' . DS . 'custom_css.php';
require_once TM_ROBIN_OPTIONS_DIR . DS . 'custom' . DS . 'custom_js.php';

