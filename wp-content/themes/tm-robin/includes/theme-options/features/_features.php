<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'  => esc_html__( 'Theme Features', 'tm-robin' ),
	'id'     => 'panel_features',
	'icon'   => 'fa fa-puzzle-piece',
	'fields' => array(
		array(
			'id'       => 'box_mode_on',
			'type'     => 'switch',
			'title'    => esc_html__( 'Box Mode', 'tm-robin' ),
			'subtitle' => esc_html__( 'Enabling this option then your site will switch to box mode', 'tm-robin' ),
			'default'  => false,
		),

		array(
			'id'       => 'site_bg',
			'type'     => 'background',
			'title'    => esc_html__( 'Site Background', 'tm-robin' ),
			'subtitle' => esc_html__( 'Set background image or color for body. Only for boxed layout', 'tm-robin' ),
			'output'   => 'body',
			'default'  => array(
				'background-color' => '#eee',
			),
			'required' => array(
				array( 'box_mode_on', '=', true ),
			),
		),

		array(
			'id'       => 'go_to_top_on',
			'type'     => 'switch',
			'title'    => esc_html__( 'Scroll to top', 'tm-robin' ),
			'subtitle' => esc_html__( 'Activating the scroll top anchor will output a link that appears in the bottom corner of your site for users to click on that will return them to the top of your website.', 'tm-robin' ),
			'default'  => true,
		),
	),
) );
