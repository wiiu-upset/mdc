<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'      => esc_html__( 'Main Menu', 'tm-robin' ),
	'id'         => 'section_site_menu',
	'subsection' => true,
	'fields'     => array(

		array(
			'id'          => 'site_menu_font',
			'type'        => 'typography',
			'title'       => esc_html__( 'Text Font', 'tm-robin' ),
			'all_styles'  => true,
			'google'      => true,
			'font-backup' => true,
			'line-height' => false,
			'color'       => false,
			'output'      => $tm_robin_selectors['site_menu_font'],
			'units'       => 'px',
			'subtitle'    => esc_html__( 'These settings control the typography for the mobile menu text.', 'tm-robin' ),
			'text-align'  => false,
			'default'     => array(
				'font-family' => 'Source Sans Pro',
				'google'      => true,
				'font-backup' => 'Arial, Helvetica, sans-serif',
				'font-weight' => '700',
				'font-size'   => '14px',
			),
		),
		array(
			'id'       => 'site_menu_align',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Main menu align', 'tm-robin' ),
			'subtitle' => esc_html__( 'Set the text align for the main menu of your site', 'tm-robin' ),
			'options'  => array(
				'left'   => esc_html__( 'Left', 'tm-robin' ),
				'center' => esc_html__( 'Center', 'tm-robin' ),
				'right'  => esc_html__( 'Right', 'tm-robin' ),
			),
			'default'  => 'center',
		),
		array(
			'id'       => 'site_menu_items_color',
			'type'     => 'link_color',
			'title'    => esc_html__( 'Menu Item Color', 'tm-robin' ),
			'subtitle' => esc_html__( 'Pick color for the menu items', 'tm-robin' ),
			'output'   => $tm_robin_selectors['site_menu_items_color'],
			'active'   => false,
			'visited'  => false,
			'default'  => array(
				'regular' => SECONDARY_COLOR,
				'hover'   => PRIMARY_COLOR,
			)
		),
		array(
			'id'       => 'site_menu_subitems_color',
			'type'     => 'link_color',
			'title'    => esc_html__( 'Menu Sub Item Color', 'tm-robin' ),
			'subtitle' => esc_html__( 'Pick color for the menu sub items', 'tm-robin' ),
			'output'   => $tm_robin_selectors['site_menu_subitems_color'],
			'active'   => false,
			'visited'  => false,
			'default'  => array(
				'regular' => SECONDARY_COLOR,
				'hover'   => PRIMARY_COLOR,
			)
		),
		array(
			'id'       => 'site_menu_bgcolor',
			'type'     => 'color',
			'title'    => esc_html__( 'Menu Background Color', 'tm-robin' ),
			'subtitle' => esc_html__( 'Only available for header which has menu in the bottom', 'tm-robin' ),
			'output'   => $tm_robin_selectors['site_menu_bgcolor'],
			'active'   => false,
			'visited'  => false,
			'default'  => '#ffffff',
			'required' => array(
				array( 'header', '=', array( 'menu-bottom', 'menu-bottom-wide' ) ),
			),
		),
	),
) );
