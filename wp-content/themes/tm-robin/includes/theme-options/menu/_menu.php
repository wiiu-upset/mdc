<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title' => esc_html__( 'Navigation', 'tm-robin' ),
	'id'    => 'panel_nav',
	'icon'  => 'fa fa-bars',
) );

require_once TM_ROBIN_OPTIONS_DIR . DS . 'menu' . DS . 'site_menu.php';
require_once TM_ROBIN_OPTIONS_DIR . DS . 'menu' . DS . 'mobile_menu.php';
