<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'      => esc_html__( 'Mobile Menu', 'tm-robin' ),
	'id'         => 'section_mobile_menu',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'mobile_menu_icon_animation',
			'title'    => esc_html__( 'Mobile Menu Icon Animation', 'tm-robin' ),
			'subtitle' => esc_html__( 'Select the animation of the mobile menu icon', 'tm-robin' ),
			'type'     => 'select',
			'options'  => array(
				'special'  => esc_html__( 'Special', 'tm-robin' ),
				'slider'   => esc_html__( 'Slider', 'tm-robin' ),
				'squeeze'  => esc_html__( 'Squeeze', 'tm-robin' ),
				'spin'     => esc_html__( 'Spin', 'tm-robin' ),
				'elastic'  => esc_html__( 'Elastic', 'tm-robin' ),
				'emphatic' => esc_html__( 'Emphatic', 'tm-robin' ),
				'collapse' => esc_html__( 'Collapse', 'tm-robin' ),
				'vortex'   => esc_html__( 'Vortex', 'tm-robin' ),
				'stand'    => esc_html__( 'Stand', 'tm-robin' ),
				'spring'   => esc_html__( 'Spring', 'tm-robin' ),
				'3dx'      => esc_html__( '3DX', 'tm-robin' ),
				'3dy'      => esc_html__( '3DY', 'tm-robin' ),
			),
			'default'  => 'special',
		),
		array(
			'id'       => 'mobile_menu_social',
			'type'     => 'switch',
			'title'    => esc_html__( 'Social in the mobile menu', 'tm-robin' ),
			'subtitle' => esc_html__( 'Enable / Disable social in the mobile menu', 'tm-robin' ),
			'default'  => true,
		),
	),
) );
