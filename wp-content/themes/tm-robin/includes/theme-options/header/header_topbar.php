<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'      => esc_html__( 'Top Bar', 'tm-robin' ),
	'id'         => 'section_topbar',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'topbar_on',
			'type'     => 'switch',
			'title'    => esc_html__( 'Top bar', 'tm-robin' ),
			'subtitle' => esc_html__( 'Enabling this option will display top area', 'tm-robin' ),
			'default'  => true,
		),
		array(
			'id'       => 'topbar_layout',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Top bar layout', 'tm-robin' ),
			'subtitle' => esc_html__( 'If you select 1 column, you\'ll only see the text in the top bar', 'tm-robin' ),
			'options'  => array(
				'1' => esc_html__( '1 column', 'tm-robin' ),
				'2' => esc_html__( '2 columns', 'tm-robin' ),

			),
			'default'  => '2',
		),
		array(
			'id'       => 'topbar_text',
			'type'     => 'textarea',
			'title'    => esc_html__( 'Text in the top bar', 'tm-robin' ),
			'subtitle' => esc_html__( 'Insert the text you want to see in the top bar here. You can use HTML or shortcodes', 'tm-robin' ),
			'args'     => array(
				'textarea_rows' => 3,
			),
			'default'  => '<ul><li><i class="pe-7s-phone"></i><a href="tel://12452456012">(1245) 2456 012</a></li><li><i class="pe-7s-mail-open"></i><a href="mailto:info@yourdomain.com">info@yourdomain.com</a></li></ul>',
		),
		array(
			'id'            => 'topbar_height',
			'type'          => 'slider',
			'title'         => esc_html__( 'Top bar height', 'tm-robin' ),
			'default'       => 38,
			'min'           => 30,
			'step'          => 1,
			'max'           => 60,
			'display_value' => 'label',
		),
		array(
			'id'      => 'topbar_width',
			'type'    => 'button_set',
			'title'   => esc_html__( 'Top bar width', 'tm-robin' ),
			'options' => array(
				'standard' => esc_html__( 'Standard', 'tm-robin' ),
				'wide'     => esc_html__( 'Wide', 'tm-robin' ),

			),
			'default' => 'wide',
		),
		array(
			'id'       => 'topbar_social',
			'type'     => 'switch',
			'title'    => esc_html__( 'Social links', 'tm-robin' ),
			'subtitle' => esc_html__( 'Enable / Disable the social links on the top bar', 'tm-robin' ),
			'default'  => true,
			'required' => array(
				array( 'topbar_layout', '=', array( '2' ) ),
			),
		),
		array(
			'id'       => 'topbar_menu',
			'type'     => 'switch',
			'title'    => esc_html__( 'Menu', 'tm-robin' ),
			'subtitle' => esc_html__( 'Enable / Disable the top bar menu', 'tm-robin' ),
			'default'  => true,
			'required' => array(
				array( 'topbar_layout', '=', array( '2' ) ),
			),
		),
		array(
			'id'       => 'topbar_logged_in_menu',
			'type'     => 'select',
			'title'    => esc_html__( 'Top bar menu (for logged-in users)', 'tm-robin' ),
			'subtitle' => esc_html__( 'Select a menu to display in the top bar for logged-in users', 'tm-robin' ),
			'options'  => TM_Robin_Helper::get_all_menus(),
			'default'  => 'none',
			'required' => array(
				array( 'topbar_menu', '=', array( true ) ),
				array( 'topbar_layout', '=', array( '2' ) ),
			),
		),
		array(
			'id'       => 'topbar_language_switcher_on',
			'type'     => 'switch',
			'title'    => esc_html__( 'Language Switcher', 'tm-robin' ),
			'subtitle' => wp_kses( sprintf( __( 'Enable / Disable the Language Switcher in the top bar instead of the Language Menu. This feature requires <a href="%s" target="_blank">WPML</a> or <a href="%s" target="_blank">Polylang</a> plugin.', 'tm-robin' ), esc_url( 'https://wpml.org/' ), esc_url( 'https://wordpress.org/plugins/polylang/' ) ), array(
				'a' => array(
					'href'   => array(),
					'target' => array(),
				),
			) ),
			'desc'     => esc_html__( 'The switchers was customized to compatible with our theme', 'tm-robin' ),
			'default'  => true,
			'required' => array(
				array( 'topbar_layout', '=', array( '2' ) ),
			),
		),
		array(
			'id'       => 'topbar_currency_switcher_on',
			'type'     => 'switch',
			'title'    => esc_html__( 'Currency Switcher', 'tm-robin' ),
			'subtitle' => wp_kses( sprintf( __( 'Enable / Disable the Currency Switcher in the top bar instead of the Currency Menu. This feature requires <a href="%s" target="_blank">WooCommerce Multilingual</a> or <a href="%s" target="_blank">WooCommerce Currency Switcher</a> plugin.', 'tm-robin' ), esc_url( 'https://wordpress.org/plugins/woocommerce-multilingual/' ), esc_url( 'https://wordpress.org/plugins/woocommerce-currency-switcher/' ) ), array(
				'a' => array(
					'href'   => array(),
					'target' => array(),
				),
			) ),
			'desc'     => esc_html__( 'The switchers was customized to compatible with our theme', 'tm-robin' ),
			'default'  => true,
			'required' => array(
				array( 'topbar_layout', '=', array( '2' ) ),
			),
		),
		array(
			'id'          => 'topbar_color',
			'type'        => 'color',
			'transparent' => false,
			'title'       => esc_html__( 'Text Color', 'tm-robin' ),
			'subtitle'    => esc_html__( 'Pick a text color for the top bar. Only avaiable if the text color scheme is Custom', 'tm-robin' ),
			'output'      => $tm_robin_selectors['topbar_color'],
			'validate'    => 'color',
			'default'     => '#666666',
		),
		array(
			'id'       => 'topbar_bgcolor',
			'type'     => 'color',
			'title'    => esc_html__( 'Background Color', 'tm-robin' ),
			'subtitle' => esc_html__( 'Pick a background color for the top bar', 'tm-robin' ),
			'output'   => $tm_robin_selectors['topbar_bgcolor'],
			'validate' => 'color',
			'default'  => 'transparent',
		),
		array(
			'id'          => 'topbar_bdcolor',
			'type'        => 'color',
			'transparent' => false,
			'title'       => esc_html__( 'Border Color', 'tm-robin' ),
			'subtitle'    => esc_html__( 'Pick a border color for the top bar. Only avaiable if the text color scheme is Custom', 'tm-robin' ),
			'output'      => $tm_robin_selectors['topbar_bdcolor'],
			'validate'    => 'color',
			'default'     => '#eee',
		),
	),
) );
