<?php

$search_post_type = array(
	'post' => esc_html__( 'Post', 'tm-robin' ),
);

if ( class_exists( 'WooCommerce' ) ) {
	$search_post_type['product'] = esc_html__( 'Product', 'tm-robin' );
}

Redux::setSection( TM_Robin_Redux::$opt_name,
	array(
		'title'      => esc_html__( 'Search', 'tm-robin' ),
		'id'         => 'section_header_search',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'search_on',
				'type'     => 'switch',
				'title'    => esc_html__( 'Enable Search', 'tm-robin' ),
				'subtitle' => esc_html__( 'Enable / Disable the search box', 'tm-robin' ),
				'default'  => true,
			),
			array(
				'id'       => 'search_post_type',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Search content type', 'tm-robin' ),
				'subtitle' => esc_html__( 'Select content type you want to use in the search box', 'tm-robin' ),
				'options'  => $search_post_type,
				'default'  => class_exists( 'WooCommerce' ) ? 'product' : 'post',
			),
			array(
				'id'       => 'search_by',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Search product by', 'tm-robin' ),
				'options'  => array(
					'title' => esc_html__( 'Title', 'tm-robin' ),
					'sku'   => esc_html__( 'SKU', 'tm-robin' ),
					'both'  => esc_html__( 'Both title & SKU', 'tm-robin' ),
				),
				'default'  => 'both',
				'required' => array(
					array( 'search_post_type', '=', array( 'product' ) ),
				),
			),
			array(
				'id'       => 'search_categories_on',
				'type'     => 'switch',
				'title'    => esc_html__( 'Categories select box', 'tm-robin' ),
				'subtitle' => esc_html__( 'Turn on this option if you want to show categories select box', 'tm-robin' ),
				'default'  => true,
			),
			array(
				'id'       => 'search_style',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Style', 'tm-robin' ),
				'subtitle' => esc_html__( 'Select the search form style', 'tm-robin' ),
				'options'  => array(
					'full-header' => esc_html__( 'Full Header', 'tm-robin' ),
					'full-screen' => esc_html__( 'Full Screen', 'tm-robin' ),
					'dropdown'    => esc_html__( 'Dropdown', 'tm-robin' ),
				),
				'default'  => 'full-header',
			),
			array(
				'id'       => 'search_results_layout',
				'type'     => 'radio',
				'title'    => esc_html__( 'Search Results Layout', 'tm-robin' ),
				'subtitle' => __( 'Select the search results container layout.<br/>Note: The widget area is only available if the search form style is Full Header or Full Screen.',
					'tm-robin' ),
				'options'  => array(
					'widget'  => esc_html__( 'Only widget area', 'tm-robin' ),
					'results' => esc_html__( 'Only search results (if Live Search is enabled)', 'tm-robin' ),
					'both'    => esc_html__( 'Both search results and widget area', 'tm-robin' ),
				),
				'default'  => 'both',
			),
			array(
				'id'      => 'search_ajax_on',
				'type'    => 'switch',
				'title'   => esc_html__( 'Live Search', 'tm-robin' ),
				'default' => true,
			),
			array(
				'id'            => 'search_min_chars',
				'type'          => 'slider',
				'title'         => esc_html__( 'Minimum number of characters', 'tm-robin' ),
				'subtitle'      => esc_html__( 'Minimum number of characters required to trigger autosuggest',
					'tm-robin' ),
				'min'           => 1,
				'max'           => 10,
				'step'          => 1,
				'default'       => 1,
				'display_value' => 'label',
				'required'      => array(
					array( 'search_ajax_on', '=', array( true ) ),
				),
			),
			array(
				'id'            => 'search_limit',
				'type'          => 'slider',
				'title'         => esc_html__( 'Maximum number of results', 'tm-robin' ),
				'subtitle'      => esc_html__( 'Maximum number of results showed within the autosuggest box',
					'tm-robin' ),
				'min'           => 1,
				'max'           => 20,
				'step'          => 1,
				'default'       => 6,
				'display_value' => 'label',
				'required'      => array(
					array( 'search_ajax_on', '=', array( true ) ),
				),
			),
			array(
				'id'       => 'search_excerpt_on',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show excerpt', 'tm-robin' ),
				'subtitle' => esc_html__( 'Show the excerpt of search result', 'tm-robin' ),
				'default'  => false,
				'required' => array(
					array( 'search_ajax_on', '=', array( true ) ),
				),
			),
			array(
				'id'          => 'search_color',
				'type'        => 'color',
				'transparent' => false,
				'title'       => esc_html__( 'Search Icon Color', 'tm-robin' ),
				'subtitle'    => esc_html__( 'Pick a color for the search icon', 'tm-robin' ),
				'output'      => $tm_robin_selectors['search_color'],
				'validate'    => 'color',
				'default'     => SECONDARY_COLOR,
			),
		),
	) );
