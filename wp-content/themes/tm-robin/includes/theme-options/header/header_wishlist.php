<?php

if ( ! class_exists( 'YITH_WCWL' ) ) {
	return;
}

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'      => esc_html__( 'Wishlist', 'tm-robin' ),
	'id'         => 'section_header_wishlist',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'wishlist_on',
			'type'     => 'switch',
			'title'    => esc_html__( 'Enable Wishlist', 'tm-robin' ),
			'subtitle' => esc_html__( 'Enable / Disable the wishlist icon in the header', 'tm-robin' ),
			'default'  => true,
		),
		array(
			'id'      => 'wishlist_icon',
			'type'    => 'button_set',
			'title'   => esc_html__( 'Wishlist Icon', 'tm-robin' ),
			'options' => array(
				'heart'   => '<i class="fa fa-heart"></i>&nbsp;&nbsp;' . esc_html__( 'Heart', 'tm-robin' ),
				'heart-o' => '<i class="fa fa-heart-o"></i>&nbsp;&nbsp;' . esc_html__( 'Outline Heart', 'tm-robin' ),
			),
			'default' => 'heart-o',
		),
		array(
			'id'      => 'wishlist_add_to_cart_on',
			'type'    => 'switch',
			'title'   => esc_html__( 'Show Add To Cart Button', 'tm-robin' ),
			'default' => true,
		),
		array(
			'id'      => 'wishlist_target',
			'type'    => 'switch',
			'title'   => esc_html__( 'Open Wishlist page in a new tab', 'tm-robin' ),
			'default' => false,
		),
		array(
			'id'          => 'wishlist_icon_color',
			'type'        => 'color',
			'transparent' => false,
			'title'       => esc_html__( 'Wishlist Icon Color', 'tm-robin' ),
			'subtitle'    => esc_html__( 'Pick a color for the wishlist icon', 'tm-robin' ),
			'output'      => $tm_robin_selectors['wishlist_icon_color'],
			'validate'    => 'color',
			'default'     => SECONDARY_COLOR,
		),
		array(
			'id'          => 'wishlist_text_color',
			'type'        => 'color',
			'transparent' => false,
			'title'       => esc_html__( 'Wishlist Text Color', 'tm-robin' ),
			'subtitle'    => esc_html__( 'Pick a color for the text of wishlist widget', 'tm-robin' ),
			'output'      => $tm_robin_selectors['wishlist_text_color'],
			'validate'    => 'color',
			'default'     => SECONDARY_COLOR,
		),
		array(
			'id'          => 'wishlist_count_bg_color',
			'type'        => 'color',
			'transparent' => false,
			'title'       => esc_html__( 'Wishlist Item Count Background Color', 'tm-robin' ),
			'subtitle'    => esc_html__( 'Pick a background color for the item count of wishlist widget', 'tm-robin' ),
			'output'      => $tm_robin_selectors['wishlist_count_bg_color'],
			'validate'    => 'color',
			'default'     => PRIMARY_COLOR,
		),
	),
) );
