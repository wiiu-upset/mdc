<?php

if ( ! class_exists( 'WooCommerce' ) ) {
	return;
}

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'      => esc_html__( 'Shopping Cart', 'tm-robin' ),
	'id'         => 'section_header_cart',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'minicart_on',
			'type'     => 'switch',
			'title'    => esc_html__( 'Enable Shopping Cart', 'tm-robin' ),
			'subtitle' => esc_html__( 'Enable / Disable the shopping cart', 'tm-robin' ),
			'default'  => true,
		),
		array(
			'id'       => 'minicart_message',
			'type'     => 'textarea',
			'title'    => esc_html__( 'Shopping Cart Message', 'tm-robin' ),
			'subtitle' => esc_html__( 'Insert the text you want to see in the shopping cart here.', 'tm-robin' ),
			'default'  => __( 'Free Shipping on All Orders Over $100!', 'tm-robin' ),
		),
		array(
			'id'       => 'minicart_message_pos',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Message Position', 'tm-robin' ),
			'subtitle' => esc_html__( 'Set the position for shopping cart message in the dropdown', 'tm-robin' ),
			'options'  => array(
				'top'    => esc_html__( 'Top', 'tm-robin' ),
				'bottom' => esc_html__( 'Bottom', 'tm-robin' ),
			),
			'default'  => 'bottom',
		),
		array(
			'id'      => 'minicart_icon',
			'type'    => 'button_set',
			'title'   => esc_html__( 'Shopping Cart Icon', 'tm-robin' ),
			'options' => array(
				'shopping-basket' => '<i class="fa fa-shopping-basket"></i>&nbsp;&nbsp;' . esc_html__( 'Basket', 'tm-robin' ),
				'shopping-cart'   => '<i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;' . esc_html__( 'Cart', 'tm-robin' ),
			),
			'default' => 'shopping-basket',
		),
		array(
			'id'          => 'minicart_icon_color',
			'type'        => 'color',
			'transparent' => false,
			'title'       => esc_html__( 'Cart Icon Color', 'tm-robin' ),
			'subtitle'    => esc_html__( 'Pick a color for the shopping cart icon', 'tm-robin' ),
			'output'      => $tm_robin_selectors['minicart_icon_color'],
			'default'     => SECONDARY_COLOR,
		),
		array(
			'id'          => 'minicart_text_color',
			'type'        => 'color',
			'transparent' => false,
			'title'       => esc_html__( 'Cart Text Color', 'tm-robin' ),
			'subtitle'    => esc_html__( 'Pick a color for the text of shopping cart', 'tm-robin' ),
			'output'      => $tm_robin_selectors['minicart_text_color'],
			'default'     => SECONDARY_COLOR,
		),
		array(
			'id'          => 'minicart_count_bg_color',
			'type'        => 'color',
			'transparent' => false,
			'title'       => esc_html__( 'Cart Item Count Background Color', 'tm-robin' ),
			'subtitle'    => esc_html__( 'Pick a background color for the item count of shopping cart', 'tm-robin' ),
			'output'      => $tm_robin_selectors['minicart_count_bg_color'],
			'validate'    => 'color',
			'default'     => PRIMARY_COLOR,
		),
	),
) );
