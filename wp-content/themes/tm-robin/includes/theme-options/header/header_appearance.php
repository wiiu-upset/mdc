<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'      => esc_html__( 'Header Appearance', 'tm-robin' ),
	'id'         => 'section_header_appearance',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'header_bgcolor',
			'type'     => 'color',
			'title'    => esc_html__( 'Background Color', 'tm-robin' ),
			'subtitle' => esc_html__( 'Pick a background color for the header', 'tm-robin' ),
			'output'   => $tm_robin_selectors['header_bgcolor'],
			'default'  => '#ffffff',
		),
		array(
			'id'       => 'header_bdcolor',
			'type'     => 'color',
			'title'    => esc_html__( 'Border Color', 'tm-robin' ),
			'subtitle' => esc_html__( 'Pick a border color for the header', 'tm-robin' ),
			'output'   => $tm_robin_selectors['header_bdcolor'],
			'validate' => 'color',
			'default'  => 'transparent',
		),
	),
) );
