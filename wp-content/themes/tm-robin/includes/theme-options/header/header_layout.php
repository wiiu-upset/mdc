<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title'      => esc_html__( 'Header Layout', 'tm-robin' ),
	'id'         => 'section_header_layout',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'sticky_header',
			'type'     => 'switch',
			'title'    => esc_html__( 'Sticky Header', 'tm-robin' ),
			'subtitle' => esc_html__( 'Enable / Disable sticky header option', 'tm-robin' ),
			'default'  => true,
		),
		array(
			'id'            => 'header_height',
			'type'          => 'slider',
			'title'         => esc_html__( 'Header Height', 'tm-robin' ),
			'default'       => 120,
			'min'           => 60,
			'step'          => 1,
			'max'           => 150,
			'display_value' => 'label',
		),
		array(
			'id'       => 'header_overlap',
			'type'     => 'switch',
			'title'    => esc_html__( 'Header above the content', 'tm-robin' ),
			'default'  => false,
			'required' => array(
				array( 'header', '=', array( 'base', 'wide' ) ),
			),
		),
		array(
			'id'            => 'right_column_width',
			'type'          => 'slider',
			'title'         => esc_html__( 'Header tools column width', 'tm-robin' ),
			'subtitle'      => esc_html__( 'Set width for icons area in the header (search, wishlist, shopping cart) (in %)', 'tm-robin' ),
			'default'       => 10,
			'min'           => 1,
			'max'           => 50,
			'step'          => 1,
			'display_value' => 'label',
			'required'      => array(
				array( 'header', '=', array( 'base', 'wide' ) ),
			),
		),
		array(
			'id'       => 'header_social',
			'type'     => 'switch',
			'title'    => esc_html__( 'Social in the header', 'tm-robin' ),
			'subtitle' => esc_html__( 'Only available for header which has menu in the bottom', 'tm-robin' ),
			'default'  => true,
			'required' => array(
				array( 'header', '=', array( 'menu-bottom', 'menu-bottom-wide' ) ),
			),
		),
		array(
			'id'       => 'header',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Header Type', 'tm-robin' ),
			'subtitle' => esc_html__( 'Select your header layout', 'tm-robin' ),
			'options'  => array(
				'base'             => array(
					'title' => esc_html__( 'Base Header', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . DS . 'header-base.jpg',
				),
				'wide'             => array(
					'title' => esc_html__( 'Wide Header', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . DS . 'header-wide.jpg',
				),
				'menu-bottom'      => array(
					'title' => esc_html__( 'Menu Bottom', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . DS . 'header-menu-bottom.jpg',
				),
				'menu-bottom-wide' => array(
					'title' => esc_html__( 'Menu Bottom Wide', 'tm-robin' ),
					'img'   => TM_ROBIN_ADMIN_IMAGES . DS . 'header-menu-bottom-wide.jpg',
				),
			),
			'default'  => 'wide',
		),

		array(
			'id'       => 'header_left_sidebar',
			'type'     => 'select',
			'title'    => esc_html__( 'Header Left Sidebar', 'tm-robin' ),
			'data'     => 'sidebars',
			'default'  => 'sidebar-header-left',
			'required' => array(
				array( 'header', '=', array( 'wide', 'menu-bottom', 'menu-bottom-wide' ) ),
			),
		),

		array(
			'id'       => 'header_right_sidebar',
			'type'     => 'select',
			'title'    => esc_html__( 'Header Right Sidebar', 'tm-robin' ),
			'data'     => 'sidebars',
			'default'  => 'sidebar-header-right',
			'required' => array(
				array( 'header', '=', array( 'wide', 'menu-bottom', 'menu-bottom-wide' ) ),
			),
		),
	),
) );
