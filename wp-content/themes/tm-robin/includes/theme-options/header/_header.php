<?php

Redux::setSection( TM_Robin_Redux::$opt_name, array(
	'title' => esc_html__( 'Header', 'tm-robin' ),
	'id'    => 'panel_header',
	'icon'  => 'el el-credit-card',
) );

require_once TM_ROBIN_OPTIONS_DIR . DS . 'header' . DS . 'header_topbar.php';
require_once TM_ROBIN_OPTIONS_DIR . DS . 'header' . DS . 'header_layout.php';
require_once TM_ROBIN_OPTIONS_DIR . DS . 'header' . DS . 'header_appearance.php';
require_once TM_ROBIN_OPTIONS_DIR . DS . 'header' . DS . 'header_search.php';
require_once TM_ROBIN_OPTIONS_DIR . DS . 'header' . DS . 'header_wishlist.php';
require_once TM_ROBIN_OPTIONS_DIR . DS . 'header' . DS . 'header_cart.php';
