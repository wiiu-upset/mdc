<?php

Redux::setSection( TM_Robin_Redux::$opt_name,
	array(
		'title'  => esc_html__( 'Page Title', 'tm-robin' ),
		'id'     => 'panel_page_title',
		'icon'   => 'fa fa-credit-card',
		'fields' => array(

			array(
				'id'       => 'page_title_on',
				'type'     => 'switch',
				'title'    => esc_html__( 'Page Title', 'tm-robin' ),
				'subtitle' => esc_html__( 'Enabling this option will display the page title area.', 'tm-robin' ),
				'default'  => true,
			),

			array(
				'id'       => 'remove_whitespace',
				'type'     => 'switch',
				'title'    => esc_html__( 'Remove whitespace', 'tm-robin' ),
				'subtitle' => esc_html__( 'Remove the whitespace below the header when the Page Title is turned off',
					'tm-robin' ),
				'default'  => true,
				'required' => array(
					'page_title_on',
					'=',
					false,
				),
			),

			array(
				'id'      => 'page_title_style',
				'type'    => 'button_set',
				'title'   => esc_html__( 'Style', 'tm-robin' ),
				'options' => array(
					'bg_color' => esc_html__( 'Background Color', 'tm-robin' ),
					'bg_image' => esc_html__( 'Background Image', 'tm-robin' ),
				),
				'default' => 'bg_color',
			),

			array(
				'id'      => 'page_title_height',
				'type'    => 'slider',
				'title'   => esc_html__( 'Page Title Height (in pixels)', 'tm-robin' ),
				'default' => 250,
				'min'     => 100,
				'max'     => 500,
				'step'    => 10,
			),

			array(
				'id'          => 'page_title_text_color',
				'type'        => 'color',
				'transparent' => false,
				'title'       => esc_html__( 'Text Color', 'tm-robin' ),
				'output'      => $tm_robin_selectors['page_title_text_color'],
				'default'     => '#111111',
			),

			array(
				'id'          => 'page_subtitle_color',
				'type'        => 'color',
				'transparent' => false,
				'title'       => esc_html__( 'Subtitle Color', 'tm-robin' ),
				'output'      => $tm_robin_selectors['page_subtitle_color'],
				'default'     => '#111111',
			),

			array(
				'id'          => 'page_title_bg_color',
				'type'        => 'color',
				'transparent' => false,
				'title'       => esc_html__( 'Background Color', 'tm-robin' ),
				'output'      => $tm_robin_selectors['page_title_bg_color'],
				'default'     => '#ffffff',
				'required'    => array(
					'page_title_style',
					'=',
					'bg_color',
				),
			),

			array(
				'id'       => 'page_title_overlay_color',
				'type'     => 'color_rgba',
				'title'    => esc_html__( 'Overlay Color', 'tm-robin' ),
				'output'   => $tm_robin_selectors['page_title_overlay_color'],
				'default'  => array(
					'color' => '#000000',
					'alpha' => 0,
				),
				'required' => array(
					'page_title_style',
					'=',
					'bg_image',
				),
			),

			array(
				'id'                    => 'page_title_bg_image',
				'type'                  => 'background',
				'title'                 => esc_html__( 'Background Image', 'tm-robin' ),
				'background-color'      => false,
				'background-repeat'     => false,
				'background-attachment' => false,
				'background-position'   => false,
				'background-size'       => false,
				'default'               => array(
					'background-image' => TM_ROBIN_IMAGES . '/page-title-bg.jpg',
				),
				'output'                => $tm_robin_selectors['page_title_bg_image'],
				'required'              => array(
					'page_title_style',
					'=',
					'bg_image',
				),
			),
		),
	) );
