<?php
/* Loads Robin Icon Font. */

// Enqueue CSS.
wp_enqueue_style( 'font-Robin', TM_ROBIN_THEME_URI . '/assets/libs/Robin/style.css' );

add_filter( 'vc_iconpicker-type-Robin', 'vc_iconpicker_type_Robin' );

/**
 * Robin icons from ThemeMove
 *
 * @param $icons - taken from filter - vc_map param field settings['source'] provided icons (default empty array).
 * If array categorized it will auto-enable category dropdown
 *
 * @since 4.4
 * @return array - of icons for iconpicker, can be categorized, or not.
 */
function vc_iconpicker_type_Robin( $icons ) {
	$Robin_icons = array(
		array( 'Robin-accessories' => 'accessories' ),
		array( 'Robin-bags' => 'bags' ),
		array( 'Robin-shoes' => 'shoes' ),
		array( 'Robin-man' => 'man' ),
		array( 'Robin-woman' => 'woman' ),
	);

	return array_merge( $icons, $Robin_icons );
}

?>
