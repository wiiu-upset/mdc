<form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div>
		<label class="screen-reader-text" for="s"><?php esc_html_e('Search for:', 'tm-robin');?></label>
		<input type="text" value="" name="s" id="s" placeholder="<?php esc_html_e('Search&hellip;', 'tm-robin'); ?>"/>
		<input type="hidden" name="post_type" value="<?php echo esc_attr( tm_robin_get_option( 'search_post_type' ) ); ?>"/>
		<button type="submit" id="searchsubmit"><?php esc_html_e('Search', 'tm-robin'); ?></button>
	</div>
</form>
