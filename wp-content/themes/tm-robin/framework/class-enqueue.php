<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue scripts and styles.
 *
 * @package   InsightFramework
 */

if ( ! class_exists( 'TM_Robin_Enqueue' ) ) {

	class TM_Robin_Enqueue {

		/**
		 * The constructor.
		 */
		public function __construct() {
			add_action( 'wp_enqueue_scripts',
				array(
					$this,
					'enqueue',
				) );
			add_action( 'wp_enqueue_scripts',
				array(
					$this,
					'custom_css',
				),
				999 );
			add_action( 'wp_enqueue_scripts',
				array(
					$this,
					'custom_js',
				),
				999 );

			add_action( 'admin_enqueue_scripts',
				array(
					$this,
					'admin_css',
				),
				999 );
			add_action( 'admin_enqueue_scripts',
				array(
					$this,
					'admin_js',
				) );
		}

		/**
		 * Enqueue scrips & styles.
		 *
		 * @access public
		 */
		public function enqueue() {
			/*
			 * Enqueue the theme's styles.css.
			 * This is recommended because we can add inline styles there
			 * and some plugins use it to do exactly that.
			 */
			if ( is_rtl() ) {
				//wp_enqueue_style( 'tm-robin-main-style', INSIGHT_THEME_URI . '/rtl.css' );
			} else {
				wp_enqueue_style( 'tm-robin-main-style', get_stylesheet_uri() );
			}

			/*
			 * The comment-reply script.
			 */
			if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
				wp_enqueue_script( 'comment-reply' );
			}

			/*
			 * Enqueue the default font (Source Sans Pro if Redux isn't installed)
			 */
			if ( ! class_exists( 'Redux' ) ) {
				$font_url = add_query_arg( 'family',
					'Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i',
					'http://fonts.googleapis.com/css' );
				wp_enqueue_style( 'google-fonts', $font_url, null, TM_ROBIN_THEME_VERSION );
			}

			/*
			 * Enqueue font awesome
			 */
			wp_enqueue_style( 'font-awesome',
				TM_ROBIN_THEME_URI . '/assets/libs/font-awesome/css/font-awesome.min.css' );

			/*
			 * Enqueue font awesome
			 */
			wp_enqueue_style( 'font-pe-icon-7-stroke',
				TM_ROBIN_THEME_URI . '/assets/libs/pixeden-stroke-7-icon/css/pe-icon-7-stroke.min.css' );

			/*
			 * Enqueue jQuery plugins
			 */
			wp_enqueue_script( 'tm-robin-js-plugins',
				TM_ROBIN_THEME_URI . '/assets/js/plugins.js',
				array( 'jquery' ),
				TM_ROBIN_THEME_VERSION,
				true );

			/*
			 * Enqueue main JS
			 */
			wp_enqueue_script( 'tm-robin-js-main',
				TM_ROBIN_THEME_URI . '/assets/js/main.js',
				array( 'jquery' ),
				TM_ROBIN_THEME_VERSION,
				true );

			$ajax_url     = admin_url( 'admin-ajax.php' );
			$current_lang = apply_filters( 'wpml_current_language', null );

			if ( $current_lang ) {
				$ajax_url = add_query_arg( 'lang', $current_lang, $ajax_url );
			}

			wp_localize_script( 'tm-robin-js-main',
				'tmRobinConfigs',
				array(
					'ajax_url'                         => esc_url( $ajax_url ),
					'ajax_url'                         => esc_url( admin_url( 'admin-ajax.php' ) ),
					'wc_cart_url'                      => ( function_exists( 'wc_get_cart_url' ) ? esc_url( wc_get_cart_url() ) : '' ),
					'quickview_image_width'            => apply_filters( 'tm_robin_quickview_image_width',
						intval( get_option( 'shop_single_image_size', array( 'width' => 423 ) )['width'] ) * .8 ),
					'quickview_image_height'           => apply_filters( 'tm_robin_quickview_image_height',
						intval( get_option( 'shop_single_image_size', array( 'height' => 576 ) )['height'] ) * .8 ),
					'logged_in'                        => is_user_logged_in(),
					'sticky_header'                    => tm_robin_get_option( 'sticky_header' ) == '1' ? true : false,
					'search_by'                        => tm_robin_get_option( 'search_by' ),
					'search_min_chars'                 => tm_robin_get_option( 'search_min_chars' ),
					'search_limit'                     => tm_robin_get_option( 'search_limit' ),
					'search_excerpt_on'                => tm_robin_get_option( 'search_excerpt_on' ) == '1' ? true : false,
					'adding_to_cart_text'              => apply_filters( 'tm_robin_adding_to_cart_text',
						esc_html__( 'Adding to cart...', 'tm-robin' ) ),
					'shop_view_mode'                   => tm_robin_get_option( 'shop_view_mode' ),
					'shop_add_to_cart_notification_on' => tm_robin_get_option( 'shop_add_to_cart_notification_on' ) == '1' ? true : false,
					'shop_add_to_cart_favico_on'       => tm_robin_get_option( 'shop_add_to_cart_favico_on' ) == '1' ? true : false,
					'shop_favico_badge_bg_color'       => apply_filters( 'tm_robin_shop_favico_badge_bg_color',
						'#ff0000' ),
					'shop_favico_badge_text_color'     => apply_filters( 'tm_robin_shop_favico_badge_text_color',
						'#ffffff' ),
					'added_to_cart_notification_text'  => apply_filters( 'tm_robin_added_to_cart_notification_text',
						esc_html__( 'has been added to cart!', 'tm-robin' ) ),
					'view_cart_notification_text'      => apply_filters( 'tm_robin_view_cart_notification_text',
						esc_html__( 'View Cart', 'tm-robin' ) ),
					'shop_wishlist_notification_on'    => tm_robin_get_option( 'shop_wishlist_notification_on' ) == '1' ? true : false,
					'added_to_wishlist_text'           => get_option( 'yith_wcwl_product_added_text',
						esc_html__( 'Product has been added to wishlist!', 'tm-robin' ) ),
					'browse_wishlist_text'             => get_option( 'yith_wcwl_browse_wishlist_text',
						esc_html__( 'Browse Wishlist', 'tm-robin' ) ),
					'wishlist_url'                     => ( function_exists( 'YITH_WCWL' ) ? esc_url( YITH_WCWL()->get_wishlist_url() ) : '' ),
					'shop_ajax_on'                     => tm_robin_get_option( 'shop_ajax_on' ) == '1' ? true : false,
					'categories_toggle'                => apply_filters( 'tm_robin_categories_toggle', true ),
					'product_page_layout'              => tm_robin_get_option( 'product_page_layout' ),
					'go_to_filter_text'                => apply_filters( 'tm_robin_go_to_filter_text',
						esc_html__( 'Back to top', 'tm-robin' ) ),
				) );
		}

		/**
		 * Add admin css
		 *
		 * @since 1.0
		 */
		public function admin_css() {
			/*
			 * Enqueue main JS
			 */
			wp_enqueue_style( 'css-admin', TM_ROBIN_THEME_URI . '/assets/admin/css/admin.css' );
		}

		/**
		 * Add admin js
		 *
		 * @since 1.0
		 */
		public function admin_js() {
			/*
			 * Enqueue main JS
			 */
			wp_enqueue_script( 'tm-robin-js-admin',
				TM_ROBIN_THEME_URI . '/assets/admin/js/admin.js',
				array(
					'jquery',
					'jquery-ui-tabs',
				),
				null,
				true );
		}

		/**
		 * Include the generated CSS and JS in the page header.
		 */
		public function custom_css() {

			$id = TM_Robin_Helper::get_the_ID();

			$primary_color = tm_robin_get_option( 'primary_color' );

			$topbar_on     = tm_robin_get_option( 'topbar_on' );
			$topbar_height = $topbar_on ? tm_robin_get_option( 'topbar_height' ) : 0;

			$logo_width         = tm_robin_get_option( 'logo_width' );
			$right_column_width = tm_robin_get_option( 'right_column_width' );

			$header                   = tm_robin_get_option( 'header' );
			$header_height            = tm_robin_get_option( 'header_height' );
			$header_sticky_height     = $header_height > 100 ? $header_height - 40 : $header_height - 30;
			$site_menu_items_color    = tm_robin_get_option( 'site_menu_items_color' );
			$site_menu_subitems_color = tm_robin_get_option( 'site_menu_subitems_color' );

			$page_title_height = tm_robin_get_option( 'page_title_height' );

			$custom_css = tm_robin_get_option( 'custom_css' );

			$output = '';

			if ( $topbar_on ) {
				$output .= '.topbar, .topbar .social-links li i{height:' . $topbar_height . 'px}';
				$output .= '.topbar .topbar-left, .topbar .topbar-right, 
				.topbar .social-links li i, .topbar .social-links li span.title,
				.topbar .switcher .nice-select {line-height:' . $topbar_height . 'px}';
			}

			$output .= '@media (min-width: 1200px) {';
			$output .= '.site-logo{line-height:' . $header_height . 'px}';
			$output .= '.site-logo img{max-height:' . $header_height . 'px}';
			$output .= '.is-sticky .site-logo, .is-sticky .header-tools a.toggle{line-height:' . $header_sticky_height . 'px}';
			$output .= '.is-sticky .site-logo img{max-height:' . $header_sticky_height . 'px}';
			$output .= '}';

			if ( $header != 'menu-bottom' && $header != 'menu-bottom-wide' ) {

				$output .= '.site-logo{width:' . $logo_width . '%}';
				$output .= '.header-tools{width:' . $right_column_width . '%}';
				$output .= '.site-menu .menu > ul > li, .site-menu .menu > li{height:' . $header_height . 'px;line-height:' . $header_height . 'px}';

			} elseif ( $header == 'menu-bottom' || $header == 'menu-bottom-wide' ) {

				$output .= '.site-header.sticky-header .site-menu .menu > ul > li, .hidden-sticky-header .site-menu .menu > li{height:' . $header_sticky_height . 'px;line-height:' . $header_sticky_height . 'px}';
				$output .= '@media (min-width: 1200px) {';
				$output .= '.site-header.sticky-header .site-logo{line-height:' . $header_sticky_height . 'px}';
				$output .= '.site-header.sticky-header .site-logo img{max-height:' . $header_sticky_height . 'px}';
				$output .= '.site-header.sticky-header .site-logo{width:' . $logo_width . '%}';
				$output .= '.site-header.sticky-header .header-tools{width:' . $right_column_width . '%}';
				$output .= '}';
			}
			$output .= '.site-header.sticky-header .site-menu .menu > ul > li, .is-sticky .site-menu .menu > li{height:' . $header_sticky_height . 'px;line-height:' . $header_sticky_height . 'px}';

			if ( is_array( $site_menu_items_color ) && isset( $site_menu_items_color['hover'] ) ) {
				$output .= '.site-menu .menu > ul > li:hover > a, .site-menu .menu > li:hover > a{color:' . $primary_color . '}';
				$output .= '.site-menu .menu > ul > li > a:after, .site-menu .menu > li > a:after { background-color:' . $site_menu_items_color['hover'] . '}';
			}

			if ( is_array( $site_menu_subitems_color ) && isset( $site_menu_subitems_color['hover'] ) ) {
				$output .= '.site-menu .menu > ul > li .children li.page_item:before, .site-menu .menu > li .sub-menu li.menu-item:before  { background-color:' . $site_menu_subitems_color['hover'] . '}';

				$output .= '.site-menu .menu > ul > li .children li.page_item:hover > a,
				.site-menu .menu > li .sub-menu li.menu-item:hover > a,
				.site-menu .menu > ul > li .children li.page_item:hover:after,
				.site-menu .menu > li .sub-menu li.menu-item:hover:after,
				.site-menu .menu > ul > li .children li.page_item.current_page_item > a,
				.site-menu .menu > li .sub-menu li.menu-item.current-menu-item > a {
					color: ' . $site_menu_subitems_color['hover'] . '}';
			}

			if ( $header != 'menu-bottom' && $header != 'menu-bottom-wide' ) {
				$output .= '@media (min-width: 1200px) {';
				$output .= '.header-overlap .site-header:not(.sticky-header){top:' . $topbar_height . 'px}';
				$output .= '}';
			}

			// Page title for post or page
			$output .= '@media (min-width: 1200px) {';
			$output .= '.page-title > .container > .row{height:' . $page_title_height . 'px}';
			$output .= '}';

			// Page title for post or page
			if ( $id || is_category() || is_tax( 'product_cat' ) || is_home() ) {

				// Page & Post
				$page_title_text_color    = get_post_meta( $id, 'tm_robin_page_title_text_color', true );
				$page_subtitle_color      = get_post_meta( $id, 'tm_robin_page_subtitle_color', true );
				$page_title_bg_color      = get_post_meta( $id, 'tm_robin_page_title_bg_color', true );
				$page_title_overlay_color = get_post_meta( $id, 'tm_robin_page_title_overlay_color', true );
				$page_title_bg_image      = get_post_meta( $id, 'tm_robin_page_title_bg_image', true );

				// Page title on category page
				if ( is_category() ) {
					$term_id                  = get_category( get_query_var( 'cat' ) )->term_id;
					$page_title_text_color    = get_term_meta( $term_id, 'tm_robin_page_title_text_color', true );
					$page_subtitle_color      = get_term_meta( $term_id, 'tm_robin_page_subtitle_color', true );
					$page_title_bg_color      = get_term_meta( $term_id, 'tm_robin_page_title_bg_color', true );
					$page_title_overlay_color = get_term_meta( $term_id, 'tm_robin_page_title_overlay_color', true );
					$page_title_bg_image      = get_term_meta( $term_id, 'tm_robin_page_title_bg_image', true );
				}

				// Page title on product category page
				if ( is_tax( 'product_cat' ) ) {
					$term_id                  = get_term_by( 'slug',
						get_query_var( 'product_cat' ),
						'product_cat' )->term_id;
					$page_title_text_color    = get_term_meta( $term_id, 'tm_robin_page_title_text_color', true );
					$page_subtitle_color      = get_term_meta( $term_id, 'tm_robin_page_subtitle_color', true );
					$page_title_bg_color      = get_term_meta( $term_id, 'tm_robin_page_title_bg_color', true );
					$page_title_overlay_color = get_term_meta( $term_id, 'tm_robin_page_title_overlay_color', true );
					$page_title_bg_image      = get_term_meta( $term_id, 'tm_robin_page_title_bg_image', true );
				}

				$output .= '.page-title.page-title-bg_color{background-color:' . $page_title_bg_color . '}';
				$output .= '.page-title.page-title-bg_image{background-image:url(\'' . $page_title_bg_image . '\') !important}';
				$output .= '.page-title:before{background-color:' . $page_title_overlay_color . ' !important}';
				$output .= '.page-title h1,
					.page-title a,
					.page-title .site-breadcrumbs,
					.page-title .site-breadcrumbs ul li:after,
					.page-title .site-breadcrumbs .insight_core_breadcrumb a, 
					.page-title .site-breadcrumbs .insight_core_breadcrumb a:hover{color:' . $page_title_text_color . ' !important}';
				$output .= '.page-title .page-subtitle{color:' . $page_subtitle_color . ' !important}';
			}

			// Custom color for meta box of page
			if ( $id && ( is_page() || is_home() || ( function_exists( 'is_shop' ) && is_shop() ) || ( function_exists( 'is_product_taxonomy' ) && is_product_taxonomy() ) ) ) {

				$topbar_color   = get_post_meta( $id, 'tm_robin_topbar_color', true );
				$topbar_bgcolor = get_post_meta( $id, 'tm_robin_topbar_bgcolor', true );
				$topbar_bdcolor = get_post_meta( $id, 'tm_robin_topbar_bdcolor', true );

				$header_bgcolor = get_post_meta( $id, 'tm_robin_header_bgcolor', true );
				$header_bdcolor = get_post_meta( $id, 'tm_robin_header_bdcolor', true );

				$search_color            = get_post_meta( $id, 'tm_robin_search_color', true );
				$wishlist_icon_color     = get_post_meta( $id, 'tm_robin_wishlist_icon_color', true );
				$wishlist_text_color     = get_post_meta( $id, 'tm_robin_wishlist_text_color', true );
				$wishlist_count_bg_color = get_post_meta( $id, 'tm_robin_wishlist_count_bg_color', true );
				$minicart_icon_color     = get_post_meta( $id, 'tm_robin_minicart_icon_color', true );
				$minicart_text_color     = get_post_meta( $id, 'tm_robin_minicart_text_color', true );
				$minicart_count_bg_color = get_post_meta( $id, 'tm_robin_minicart_count_bg_color', true );

				$site_menu_bgcolor              = get_post_meta( $id, 'tm_robin_site_menu_bgcolor', true );
				$site_menu_items_color          = get_post_meta( $id, 'tm_robin_site_menu_items_color', true );
				$site_menu_items_color_hover    = get_post_meta( $id, 'tm_robin_site_menu_items_color_hover', true );
				$site_menu_subitems_color       = get_post_meta( $id, 'tm_robin_site_menu_subitems_color', true );
				$site_menu_subitems_color_hover = get_post_meta( $id, 'tm_robin_site_menu_subitems_color_hover', true );

				$footer_bgcolor                    = get_post_meta( $id, 'tm_robin_footer_bgcolor', true );
				$footer_color                      = get_post_meta( $id, 'tm_robin_footer_color', true );
				$footer_accent_color               = get_post_meta( $id, 'tm_robin_footer_accent_color', true );
				$footer_copyright_bgcolor          = get_post_meta( $id, 'tm_robin_footer_copyright_bgcolor', true );
				$footer_copyright_color            = get_post_meta( $id, 'tm_robin_footer_copyright_color', true );
				$footer_copyright_link_color       = get_post_meta( $id, 'tm_robin_footer_copyright_link_color', true );
				$footer_copyright_link_color_hover = get_post_meta( $id,
					'tm_robin_footer_copyright_link_color_hover',
					true );

				// topbar
				if ( $topbar_color ) {
					$output .= '.topbar, .topbar a{color:' . $topbar_color . '!important;}';
				}

				// header
				if ( $topbar_bgcolor || $topbar_bdcolor ) {
					$output .= '.topbar{';

					if ( $topbar_bgcolor ) {
						$output .= 'background-color:' . $topbar_bgcolor . '!important;';
					}

					if ( $topbar_bdcolor ) {
						$output .= 'border-color:' . $topbar_bdcolor . '!important;';
					}

					$output .= '}';
				}

				if ( $header_bgcolor || $header_bdcolor ) {
					$output .= '.site-header{';

					if ( $header_bgcolor ) {
						$output .= 'background-color:' . $header_bgcolor . '!important;';
					}

					if ( $header_bdcolor ) {
						$output .= 'border-color:' . $header_bdcolor . '!important;';
					}

					$output .= '}';
				}

				if ( $header == 'menu-bottom' || $header == 'menu-botttom-wide' ) {

					if ( $site_menu_bgcolor ) {
						$output .= '.site-header .site-menu-wrap{background-color:' . $site_menu_bgcolor . '!important;}';
					}

				}

				if ( $search_color ) {
					$output .= '.site-header .header-search > a.toggle{color:' . $search_color . '}';
				}

				if ( $wishlist_icon_color ) {
					$output .= '.site-header .header-wishlist > a.toggle > i{color:' . $wishlist_icon_color . '}';
				}

				if ( $wishlist_text_color || $wishlist_count_bg_color ) {

					$output .= '.site-header .header-wishlist > a.toggle > span{';

					if ( $wishlist_text_color ) {
						$output .= 'color:' . $wishlist_text_color . ';';
					}

					if ( $wishlist_count_bg_color ) {
						$output .= 'background-color:' . $wishlist_count_bg_color . ';';
					}

					$output .= '}';
				}

				if ( $minicart_icon_color ) {
					$output .= '.site-header .header-minicart > a.toggle > i{color:' . $minicart_icon_color . '}';
				}

				if ( $minicart_text_color || $wishlist_count_bg_color ) {

					$output .= '.site-header .header-minicart > a.toggle > span{';

					if ( $minicart_text_color ) {
						$output .= 'color:' . $minicart_text_color . ';';
					}

					if ( $minicart_count_bg_color ) {
						$output .= 'background-color:' . $minicart_count_bg_color . ';';
					}

					$output .= '}';
				}

				// site menu
				if ( $site_menu_items_color ) {
					$output .= '.site-header .site-menu .menu > ul > li > a, .site-header .site-menu .menu > li > a{color:' . $site_menu_items_color . '}';
				}

				if ( $site_menu_items_color_hover ) {
					$output .= '.site-header .site-menu .menu > ul > li:hover > a,
								.site-header .site-menu .menu > li:hover > a{color:' . $site_menu_items_color_hover . '!important}';
					$output .= '.site-header .site-menu .menu > ul > li > a:after,
								.site-header .site-menu .menu > li > a:after{background-color:' . $site_menu_items_color_hover . '!important}';
				}

				if ( $site_menu_subitems_color ) {
					$output .= '.site-menu .menu > ul > li .children li.page_item > a, 
						.site-menu .menu > li .sub-menu li.menu-item > a{color:' . $site_menu_subitems_color . '}';
				}

				if ( $site_menu_subitems_color_hover ) {
					$output .= '.site-menu .menu > ul > li .children li.page_item:hover > a,
								.site-menu .menu > li .sub-menu li.menu-item:hover > a,
								.site-menu .menu > ul > li .children li.page_item.current_page_item > a,
								.site-menu .menu > li .sub-menu li.menu-item.current-menu-item > a{color:' . $site_menu_subitems_color_hover . '}';
					$output .= '.site-menu .menu > ul > li .children li.page_item:before,
								.site-menu .menu > li .sub-menu li.menu-item:before{background-color:' . $site_menu_subitems_color_hover . '}';
					$output .= '.site-menu .menu > ul > li .children li.page_item:after,
								.site-menu .menu > li .sub-menu li.menu-item:after{background-color:' . $site_menu_subitems_color_hover . '}';
				}

				// footer
				if ( $footer_bgcolor ) {
					$output .= '.site-footer.site-footer--custom{background-color:' . $footer_bgcolor . '}';
				}

				if ( $footer_color ) {
					$output .= '.site-footer.site-footer--custom a, 
							.site-footer.site-footer--custom{color:' . $footer_color . '}';
				}

				if ( $footer_accent_color ) {
					$output .= '.site-footer.site-footer--custom .widget-title,
							.site-footer.site-footer--custom a:hover,
							.site-footer.site-footer--custom .entry-title a,
							.site-footer.site-footer--custom .widget-title:after{color:' . $footer_accent_color . '}';
				}

				if ( $footer_copyright_bgcolor ) {
					$output .= '.site-footer.site-footer--custom .site-copyright{background-color:' . $footer_copyright_bgcolor . '}';
				}

				if ( $footer_copyright_color ) {
					$output .= '.site-footer.site-footer--custom .site-copyright{color:' . $footer_copyright_color . '}';
				}

				if ( $footer_copyright_link_color ) {
					$output .= '.site-footer.site-footer--custom .site-copyright a{color:' . $footer_copyright_link_color . '}';
				}

				if ( $footer_copyright_link_color_hover ) {
					$output .= '.site-footer.site-footer--custom .site-copyright a:hover{color:' . $footer_copyright_link_color_hover . '}';
				}
			}

			// If Redux is not installed
			if ( ! class_exists( 'Redux' ) ) {

				$bg_404 = tm_robin_get_option( '404_bg' );

				$output .= '.area-404{background-color:#f7f7f7;background-image:url(\'' . $bg_404['background-image'] . '\')';
			}

			if ( $custom_css ) {
				$output .= $custom_css;
			}

			$output = TM_Robin_Helper::text2line( $output );

			wp_add_inline_style( 'tm-robin-main-style', $output );
		}

		/**
		 * Load custom JS
		 */
		public function custom_js() {

			$custom_js = tm_robin_get_option( 'custom_js' );

			if ( $custom_js ) {
				wp_add_inline_script( 'tm-robin-js-main', $custom_js );
			}
		}

	}

	new TM_Robin_Enqueue();
}
