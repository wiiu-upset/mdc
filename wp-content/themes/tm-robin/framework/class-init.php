<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Initial setup for this theme
 *
 * @package   InsightFramework
 */
if ( ! class_exists( 'TM_Robin_Init' ) ) {

	class TM_Robin_Init {

		/**
		 * The constructor.
		 */
		public function __construct() {

			// Adjust the content-width.
			add_action( 'after_setup_theme', array( $this, 'content_width' ), 0 );

			// Load the theme's textdomain.
			add_action( 'after_setup_theme', array( $this, 'load_theme_textdomain' ) );

			// Register navigation menus.
			add_action( 'after_setup_theme', array( $this, 'register_nav_menus' ) );

			// Add theme supports.
			add_action( 'after_setup_theme', array( $this, 'add_theme_supports' ) );

			// Register widget areas.
			add_action( 'widgets_init', array( $this, 'widgets_init' ) );

			// Core filters.
			add_filter( 'insight_core_info', array( $this, 'core_info' ) );

			// Active Revslider
			add_action( 'after_setup_theme', array( $this, 'active_revslider' ) );

			if ( ! class_exists( 'Redux' ) ) {
				$this->load_base_options();
			}

		}

		/**
		 * Registers the Menus.
		 *
		 * @access public
		 */
		public function register_nav_menus() {
			// This theme uses wp_nav_menu() in one location.
			register_nav_menus( array(
				'primary' => esc_html__( 'Primary', 'tm-robin' ),
			) );

			register_nav_menus( array(
				'top_bar' => esc_html__( 'Top Bar Menu', 'tm-robin' ),
			) );

			register_nav_menus( array(
				'language_switcher' => esc_html__( 'Language Switcher', 'tm-robin' ),
			) );

			register_nav_menus( array(
				'currency_switcher' => esc_html__( 'Currency Switcher', 'tm-robin' ),
			) );
		}

		/**
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 *
		 * @access public
		 */
		public function load_theme_textdomain() {
			load_theme_textdomain( 'tm-robin', TM_ROBIN_THEME_DIR . '/languages' );
		}

		/**
		 * Set the content width in pixels, based on the theme's design and stylesheet.
		 *
		 * Priority 0 to make it available to lower priority callbacks.
		 *
		 * @access public
		 * @global int $content_width
		 */
		public function content_width() {
			$GLOBALS['content_width'] = apply_filters( 'content_width', 640 );
		}

		/**
		 * Sets up theme defaults and registers support for various WordPress features.
		 *
		 * Note that this function is hooked into the after_setup_theme hook, which
		 * runs before the init hook. The init hook is too late for some features, such
		 * as indicating support for post thumbnails.
		 *
		 * @access public
		 */
		public function add_theme_supports() {
			/*
			 * Add default posts and comments RSS feed links to head.
			 */
			add_theme_support( 'automatic-feed-links' );

			/*
			 * Let WordPress manage the document title.
			 * By adding theme support, we declare that this theme does not use a
			 * hard-coded <title> tag in the document head, and expect WordPress to
			 * provide it for us.
			 */
			add_theme_support( 'title-tag' );

			/*
			 * Enable support for Post Thumbnails on posts and pages.
			 *
			 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
			 */
			add_theme_support( 'post-thumbnails' );
			add_image_size( 'tm-robin-single-thumb', 1170, 770, true );
			add_image_size( 'tm-robin-misc-thumb', 270, 182, true ); // Content related.
			add_image_size( 'tm-robin-search-thumb', 110, 70, true ); // Content related.
			add_image_size( 'tm-robin-small-thumb', 50, 50, true );
			add_image_size( 'tm-robin-post-navigation', 70, 70, true );
			add_image_size( 'tm-robin-product-categories-square', 400, 400, true );
			add_image_size( 'tm-robin-product-categories-rectangle', 570, 350, true );
			/*
			 * Switch default core markup for search form, comment form, and comments
			 * to output valid HTML5.
			 */
			add_theme_support( 'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			) );

			/*
			 * Enable support for Post Formats.
			 * See https://developer.wordpress.org/themes/functionality/post-formats/
			 */
			add_theme_support( 'post-formats', array(
				'gallery',
				'video',
				'audio',
				'quote',
			) );

			/*
			 * Set up the WordPress core custom background feature.
			 */
			add_theme_support( 'custom-background', apply_filters( 'custom_background_args', array(
				'default-color' => '#ffffff',
				'default-image' => '',
			) ) );

			/*
			 * Support woocommerce
			 */
			add_theme_support( 'woocommerce' );

			/*
			 * Support selective refresh for widget
			 */
			add_theme_support( 'customize-selective-refresh-widgets' );

			/*
			 * Add theme support
			 */

			add_theme_support( 'custom-header' );

			add_theme_support( 'insight-core' );

			add_theme_support( 'insight-megamenu' );

			add_theme_support( 'insight-popup' );

			add_theme_support( 'insight-swatches' );

			add_theme_support( 'insight-sidebars' );

			/* @since 1.2 */
			if (TM_Robin_Helper::get_option('product_zoom_on')) {
				add_theme_support('wc-product-gallery-zoom');
			}
		}

		/**
		 * Register widget area.
		 *
		 * @access public
		 * @link   https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
		 */
		public function widgets_init() {

			register_sidebar( array(
				'id'            => 'sidebar',
				'name'          => esc_html__( 'Sidebar', 'tm-robin' ),
				'description'   => esc_html__( 'Add widgets here.', 'tm-robin' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );

			/* Shop Widget Area */
			register_sidebar( array(
				'id'            => 'sidebar-shop',
				'name'          => esc_html__( 'Sidebar for Shop', 'tm-robin' ),
				'description'   => esc_html__( 'Add widgets here.', 'tm-robin' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );

			/* Header Left Widget Area */
			register_sidebar( array(
				'id'            => 'sidebar-header-left',
				'name'          => esc_html__( 'Header Left Widget Area', 'tm-robin' ),
				'description'   => esc_html__( 'Only available with Header Menu Bottom Wide.', 'tm-robin' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );

			/* Header Right Widget Area */
			register_sidebar( array(
				'id'            => 'sidebar-header-right',
				'name'          => esc_html__( 'Header Right Widget Area', 'tm-robin' ),
				'description'   => esc_html__( 'Only available with Header Wide, Header Menu Bottom and Header Menu Bottom Wide.', 'tm-robin' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );

			/* Search Widget Area */
			register_sidebar( array(
				'id'            => 'sidebar-search',
				'name'          => esc_html__( 'Search Widget Area', 'tm-robin' ),
				'description'   => esc_html__( 'Add widgets here.', 'tm-robin' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );

			/* Footer Sidebar Area 1*/
			register_sidebar( array(
				'id'            => 'sidebar-footer-1',
				'name'          => esc_html__( 'Footer | #1', 'tm-robin' ),
				'description'   => esc_html__( 'This is footer area column 1.', 'tm-robin' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4 class="widget-title">',
				'after_title'   => '</h4>',
			) );

			/* Footer Sidebar Area 2*/
			register_sidebar( array(
				'id'            => 'sidebar-footer-2',
				'name'          => esc_html__( 'Footer | #2', 'tm-robin' ),
				'description'   => esc_html__( 'This footer area column 2.', 'tm-robin' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4 class="widget-title">',
				'after_title'   => '</h4>',
			) );

			/* Footer Sidebar Area 3*/
			register_sidebar( array(
				'id'            => 'sidebar-footer-3',
				'name'          => esc_html__( 'Footer | #3', 'tm-robin' ),
				'description'   => esc_html__( 'This footer area column 3.', 'tm-robin' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4 class="widget-title">',
				'after_title'   => '</h4>',
			) );

			/* Footer Sidebar Area 4 */
			register_sidebar( array(
				'id'            => 'sidebar-footer-4',
				'name'          => esc_html__( 'Footer | #4', 'tm-robin' ),
				'description'   => esc_html__( 'This is footer area column 4.', 'tm-robin' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4 class="widget-title">',
				'after_title'   => '</h4>',
			) );
		}

		/**
		 * Core info
		 *
		 * @param $info
		 *
		 * @return mixed
		 */
		public function core_info( $info ) {
			$info['api']     = TM_Robin_Helper::get_config( 'api' );
			$info['docs']    = TM_Robin_Helper::get_config( 'docs' );
			$info['icon']    = TM_ROBIN_THEME_URI . TM_Robin_Helper::get_config( 'icon' );
			$info['support'] = TM_Robin_Helper::get_config( 'support' );
			$info['tf']      = TM_Robin_Helper::get_config( 'tf' );

			return $info;
		}

		public function active_revslider() {

			if ( ! get_option( 'revslider-valid' ) || get_option( 'revslider-valid' ) === 'false' ) {
				update_option( 'revslider-valid', 'true' );
			}
		}

		private function load_base_options() {

			global $tm_robin_options;

			$tm_robin_options = apply_filters( 'tm_robin_get_base_options', array(

				// Logo
				'logo_width'                  => 15,

				// Header
				'sticky_header'               => false,
				'header_overlap'              => false,
				'right_column_width'          => 10,
				'header'                      => 'wide',
				'header_left_sidebar'         => 'sidebar-header-left',
				'header_right_sidebar'        => 'sidebar-header-right',
				'header_social'               => true,
				'header_bgcolor'              => 'transparent',
				'header_bdcolor'              => 'transparent',
				'header_color_scheme'         => 'dark',
				'header_height'               => 90,
				'search_on'                   => true,
				'search_post_type'            => class_exists( 'WooCommerce' ) ? 'product' : 'post',
				'search_style'                => 'dropdown',
				'search_ajax_on'              => true,
				'search_min_chars'            => 1,
				'search_limit'                => 6,
				'wishlist_on'                 => class_exists( 'YITH_WCWL' ),
				'wishlist_icon'               => 'heart',
				'minicart_on'                 => class_exists( 'WooCommerce' ),
				'minicart_icon'               => 'shopping-basket',

				// Page title
				'page_title_style'            => 'bg_image',
				'page_title_on'               => true,
				'page_title_text_color'       => '#111111',
				'page_subtitle_color'         => '#111111',
				'page_title_bg_color'         => '#ffffff',
				'page_title_overlay_color'    => array(
					'color' => '#000000',
					'rgba'  => 'rgba(0,0,0,0)',
				),
				'page_title_bg_image'         => array(
					'background-image' => TM_ROBIN_IMAGES . '/page-title-bg.jpg',
				),

				// Breadcrumbs
				'breadcrumbs'                 => true,
				'breadcrumbs_position'        => 'inside',

				// Navigation
				'site_menu_items_color'       => array(
					'regular' => '#111111',
					'hover'   => '#fab200',
				),
				'site_menu_subitems_color'    => array(
					'regular' => '#111111',
					'hover'   => '#fab200',
				),

				// Pages
				'page_sidebar_config'         => 'no',
				'search_sidebar_config'       => 'right',
				'archive_sidebar_config'      => 'right',
				'post_sidebar_config'         => 'right',
				'404_bg'                      => array(
					'background-image' => TM_ROBIN_IMAGES . DS . '404-bg.jpg',
				),

				// Blog
				'archive_display_type'        => 'standard',
				'archive_content_output'      => 'content',

				// WooCommerce
				'products_hover'              => 'base',
				'product_buttons_scheme'      => 'dark',
				'products_columns'            => 4,
				'shop_sidebar_config'         => 'no',
				'product_page_layout'         => 'basic',
				'product_thumbnails_position' => 'left',
				'product_sidebar_config'      => 'right',

				// Footer
				'footer_width'                => 'standard',
				'footer_layout'               => '3_4',
			) );
		}

	}

	new TM_Robin_Init();
}
