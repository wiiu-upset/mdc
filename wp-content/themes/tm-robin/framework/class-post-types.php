<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 *
 * Custom Post Type
 *
 * @package Robin
 */

if ( ! class_exists( 'TM_Robin_Post_Types' ) ) {

	class TM_Robin_Post_Types {

		public function __construct() {
			add_action( 'init', array( $this, 'init' ), 9 );
			add_action( 'init', array( $this, 'register_taxonomies' ), 1 );
		}

		public function init() {
			add_filter( 'insight_posttypes', array( $this, 'register_post_types' ) );
			add_filter( 'manage_testimonials_posts_columns', array( $this, 'testimonials_edit_columns' ), 10, 1 );
		}

		public function register_post_types() {

			$post_types = array();

			// Testimonial
			$post_types['testimonials'] = array(
				'labels'            => array(
					'name'               => esc_html__( 'Testimonials', 'tm-robin' ),
					'singular_name'      => esc_html__( 'Testimonial', 'tm-robin' ),
					'add_new'            => esc_html__( 'Add New', 'tm-robin' ),
					'add_new_item'       => esc_html__( 'Add New Testimonial', 'tm-robin' ),
					'edit_item'          => esc_html__( 'Edit Testimonial', 'tm-robin' ),
					'new_item'           => esc_html__( 'New Testimonial', 'tm-robin' ),
					'view_item'          => esc_html__( 'View Testimonial', 'tm-robin' ),
					'search_items'       => esc_html__( 'Search Testimonials', 'tm-robin' ),
					'not_found'          => esc_html__( 'No testimonials have been added yet', 'tm-robin' ),
					'not_found_in_trash' => esc_html__( 'Nothing found in Trash', 'tm-robin' ),
					'parent_item_colon'  => '',
				),
				'public'            => false,
				'has_archive'       => false,
				'show_ui'           => true,
				'show_in_menu'      => true,
				'show_in_nav_menus' => false,
				'menu_icon'         => 'dashicons-format-quote',
				'rewrite'           => false,
				'supports'          => array(
					'title',
					'editor',
					'custom-fields',
					'excerpt',
					'revisions',
				),
			);

			return $post_types;
		}

		public function register_taxonomies() {

			register_taxonomy( 'testimonials-category',
				'testimonials',
				array(
					'labels'            => esc_html__( 'Testimonial Categories', 'tm-robin' ),
					'hierarchical'      => true, // Like categories.
					'public'            => false,
					'show_ui'           => true,
					'show_admin_column' => true,
					'show_in_nav_menus' => false,
					'rewrite'           => false,
					'query_var'         => true,
				) );
		}

		public function testimonials_edit_columns( $columns ) {
			$columns = array(
				'cb'                             => '<input type="checkbox" />',
				'title'                          => esc_html__( 'Testimonial', 'tm-robin' ),
				'taxonomy-testimonials-category' => esc_html__( 'Categories', 'tm-robin' ),
			);

			return $columns;
		}

	}

	new TM_Robin_Post_Types();
}
