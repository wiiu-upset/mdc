<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Custom functions for WooCommerce
 *
 * @package   InsightFramework
 */
if ( ! class_exists( 'TM_Robin_Woo' ) ) {

	class TM_Robin_Woo {

		/**
		 * The constructor.
		 */
		public function __construct() {

			add_action( 'wp_enqueue_scripts',
				array(
					$this,
					'dequeue_woo_scripts',
				) );

			// Remove default WooCommerce style
			add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

			/*****************************************************************************************
			 * AJAX Registration
			 *****************************************************************************************/
			// Wishlist AJAX
			add_action( 'wp_ajax_tm_robin_get_wishlist_fragments',
				array(
					$this,
					'get_wishlist_fragments',
				) );
			add_action( 'wp_ajax_nopriv_tm_robin_get_wishlist_fragments',
				array(
					$this,
					'get_wishlist_fragments',
				) );
			add_action( 'wp_ajax_tm_robin_remove_wishlist_item',
				array(
					$this,
					'remove_wishlist_item',
				) );
			add_action( 'wp_ajax_nopriv_tm_robin_remove_wishlist_item',
				array(
					$this,
					'remove_wishlist_item',
				) );
			add_action( 'wp_ajax_tm_robin_undo_remove_wishlist_item',
				array(
					$this,
					'undo_remove_wishlist_item',
				) );
			add_action( 'wp_ajax_nopriv_tm_robin_undo_remove_wishlist_item',
				array(
					$this,
					'undo_remove_wishlist_item',
				) );

			// Mini cart AJAX
			add_filter( 'woocommerce_add_to_cart_fragments',
				array(
					$this,
					'get_cart_fragments',
				),
				10 );
			add_action( 'wp_ajax_tm_robin_remove_cart_item',
				array(
					$this,
					'remove_cart_item',
				) );
			add_action( 'wp_ajax_nopriv_tm_robin_remove_cart_item',
				array(
					$this,
					'remove_cart_item',
				) );
			add_action( 'wp_ajax_tm_robin_undo_remove_cart_item',
				array(
					$this,
					'undo_remove_cart_item',
				) );
			add_action( 'wp_ajax_nopriv_tm_robin_undo_remove_cart_item',
				array(
					$this,
					'undo_remove_cart_item',
				) );

			// Quick view AJAX
			add_action( 'wp_ajax_tm_robin_quick_view',
				array(
					$this,
					'quick_view',
				) );
			add_action( 'wp_ajax_nopriv_tm_robin_quick_view',
				array(
					$this,
					'quick_view',
				) );
			add_action( 'tm_robin_after_page_container',
				array(
					$this,
					'add_quick_view_container',
				) );
			add_action( 'wp_ajax_tm_robin_ajax_add_to_cart',
				array(
					$this,
					'ajax_add_to_cart',
				) );
			add_action( 'wp_ajax_nopriv_tm_robin_ajax_add_to_cart',
				array(
					$this,
					'ajax_add_to_cart',
				) );

			// Enqueue scripts for the quick view
			add_action( 'wp_enqueue_scripts',
				function () {
					wp_enqueue_script( 'wc-add-to-cart' );
					wp_enqueue_script( 'woocommerce' );
					wp_enqueue_script( 'wc-single-product' );
					wp_enqueue_script( 'wc-add-to-cart-variation' );
				} );

			/******************************************************************************************
			 * Shop Page (Product Archive Page)
			 *****************************************************************************************/
			remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
			remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

			// Remove breadcrumb
			remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

			// remove content wrapper
			remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper' );
			remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end' );

			// Change subcategory count HTML
			add_filter( 'woocommerce_subcategory_count_html',
				array(
					$this,
					'subcategory_count_html',
				),
				10,
				2 );

			// Change thumbnail size for subcategory within loop
			add_filter( 'subcategory_archive_thumbnail_size',
				function () {
					return 'full';
				} );

			/******************************************************************************************
			 * Product Loop Items
			 *
			 * @see woocommerce_template_loop_product_link_open()
			 * @see woocommerce_template_loop_product_link_close()
			 * @see woocommerce_template_loop_add_to_cart()
			 * @see woocommerce_template_loop_product_thumbnail()
			 * @see woocommerce_template_loop_product_title()
			 * @see woocommerce_template_loop_rating()
			 * @see woocommerce_template_loop_price()
			 *****************************************************************************************/
			remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
			remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
			remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

			add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_link_open', 5 );
			add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_link_close', 15 );

			add_filter( 'woocommerce_loop_add_to_cart_args', array( $this, 'add_to_cart_args' ), 10, 2 );

			// Add hover image
			remove_action( 'woocommerce_before_shop_loop_item_title',
				'woocommerce_template_loop_product_thumbnail',
				10 );
			remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
			add_action( 'woocommerce_before_shop_loop_item_title',
				array(
					$this,
					'template_loop_product_thumbnail',
				),
				10 );

			// Add link to the product title
			remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
			add_action( 'woocommerce_shop_loop_item_title',
				array(
					$this,
					'template_loop_product_title',
				),
				10 );

			// Swap rating & price
			remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
			remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
			add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 10 );
			add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 5 );
			add_action( 'woocommerce_after_shop_loop_item_title',
				array(
					$this,
					'template_loop_product_excerpt',
				),
				15 );

			// Hide default wishlist button
			add_filter( 'yith_wcwl_positions',
				function () {
					return array(
						'add-to-cart' => array(
							'hook'     => '',
							'priority' => 0,
						),
						'thumbnails'  => array(
							'hook'     => '',
							'priority' => 0,
						),
						'summary'     => array(
							'hook'     => '',
							'priority' => 0,
						),
					);
				} );

			// Hide default compare button
			add_filter( 'yith_woocompare_remove_compare_link_by_cat',
				function () {
					return true;
				} );

			/******************************************************************************************
			 * Single Product
			 *
			 * @see woocommerce_output_product_data_tabs()
			 * @see woocommerce_upsell_display()
			 * @see woocommerce_output_related_products()
			 *****************************************************************************************/
			remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
			remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
			remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
			add_action( 'tm_robin_after_page_container',
				array(
					$this,
					'photoswipe_template',
				),
				999 );
			add_filter( 'woocommerce_review_gravatar_size',
				function () {
					return '100';
				} );

			// Change number of Related product
			add_filter( 'woocommerce_output_related_products_args',
				function ( $args ) {

					$args['posts_per_page'] = tm_robin_get_option( 'product_related' );
					$args['columns']        = 4;

					return $args;
				} );

			/**
			 * Product List Widget
			 */
			add_filter( 'woocommerce_before_widget_product_list',
				function () {
					return '<div class="product_list_widget">';
				} );
			add_filter( 'woocommerce_after_widget_product_list',
				function () {
					return '</div>';
				} );

			/**
			 * Mini cart
			 */
			add_filter( 'woocommerce_is_attribute_in_product_name',
				array(
					$this,
					'is_attribute_in_product_name',
				),
				10,
				3 );
		}

		public function dequeue_woo_scripts() {

			// remove select2 style & script
			wp_dequeue_style( 'select2' );
			wp_dequeue_script( 'select2' );

			// remove prettyPhoto
			wp_dequeue_script( 'prettyPhoto' );
			wp_dequeue_script( 'prettyPhoto-init' );
			wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
		}

		/**
		 * Wishlist widget
		 */
		private function wishlist_widget() {

			$products = YITH_WCWL()->get_products( array(
				'is_default' => true,
			) );

			$products = array_reverse( $products );

			$wl_link = YITH_WCWL()->get_wishlist_url();


			$classes = array( 'wishlist_items product_list_widget' );

			if ( get_option( 'yith_wcwl_remove_after_add_to_cart' ) == 'yes' ) {
				$classes[] = 'remove_after_add_to_cart';
			}

			?>
			<p class="widget_wishlist_title"><?php esc_html_e( 'Wishlist', 'tm-robin' ); ?>
				<a href="#" class="close-on-mobile hidden-xl-up">&times;</a>
				<span class="undo">
						<?php esc_html_e( 'Item removed.', 'tm-robin' ) ?>
					<a href="#"><?php esc_html_e( 'Undo', 'tm-robin' ); ?></a>
				</span>
			</p>
			<ul class="<?php echo implode( ' ', $classes ); ?>">
				<li class="wishlist_empty_message empty<?php echo empty( $products ) ? '' : ' hidden'; ?>"><?php esc_html_e( 'No products in the wishlist.',
						'tm-robin' ); ?></li>
				<?php
				foreach ( $products as $p ) {

					global $product;

					if ( function_exists( 'wc_get_product' ) ) {
						$product = wc_get_product( $p['prod_id'] );
					} else {
						$product = get_product( $p['prod_id'] );
					}

					if ( ! $product ) {
						continue;
					}

					$product_name  = $product->get_title();
					$thumbnail     = $product->get_image( 'shop_thumbnail' );
					$product_price = $product->get_price_html();
					$remove_url    = add_query_arg( 'remove_from_wishlist', $p['prod_id'], $wl_link );

					?>
					<li class="wishlist_item"
					    data-product_id="<?php echo esc_attr( $p['prod_id'] ); ?>"
					    data-wishlist_id="<?php echo esc_attr( $p['wishlist_id'] ); ?>"
					    data-wishlist_token="<?php echo esc_attr( $p['wishlist_token'] ); ?>">
						<a href="<?php echo esc_url( $remove_url ); ?>" class="remove"
						   title="<?php esc_html_e( 'Remove this product', 'tm-robin' ) ?>">&times;</a>
						<?php if ( ! $product->is_visible() ) { ?>
							<?php echo str_replace( array(
									'http:',
									'https:',
								),
									'',
									$thumbnail ) . '&nbsp;'; ?>
						<?php } else { ?>
							<a href="<?php echo esc_url( $product->get_permalink() ); ?>"
							   class="wishlist_item_product_image">
								<?php echo str_replace( array(
										'http:',
										'https:',
									),
										'',
										$thumbnail ) . '&nbsp;'; ?>
							</a>
						<?php } ?>
						<div class="wishlist_item_right">
							<h5 class="product-title">
								<a href="<?php echo esc_url( $product->get_permalink() ); ?>"><?php echo esc_html( $product_name ); ?></a>
							</h5>
							<?php echo wp_kses_post( $product_price ); ?>
							<?php if ( ! $product->is_in_stock() ) { ?>
								<p class="outofstock"><?php esc_html_e( 'Out of stock', 'tm-robin' ); ?></p>
							<?php } ?>
							<?php if ( $product->is_in_stock() && tm_robin_get_option( 'wishlist_add_to_cart_on' ) ) {
								if ( function_exists( 'woocommerce_template_loop_add_to_cart' ) ) {
									woocommerce_template_loop_add_to_cart();
								} else {
									wc_get_template( 'loop/add-to-cart.php' );
								}
							} ?>
						</div>
					</li>
				<?php }
				?>
			</ul>


			<?php

			if ( ! empty( $products ) ) {

				$target = '';

				if ( tm_robin_get_option( 'wishlist_target' ) ) {
					$target = '_blank';
				}
				?>
				<a href="<?php echo esc_url( $wl_link ); ?>"
				   target="<?php echo esc_attr( $target ); ?>"
				   class="button btn-view-wishlist"><?php esc_html_e( 'View Wishlist', 'tm-robin' ); ?></a>
				<?php
			}
		}

		public function get_wishlist_fragments() {

			if ( ! function_exists( 'wc_setcookie' ) || ! class_exists( 'YITH_WCWL' ) ) {
				return;
			}

			$products = YITH_WCWL()->get_products( array(
				'is_default' => true,
			) );

			ob_start();

			$this->wishlist_widget();

			$wl       = ob_get_clean();
			$wl_count = YITH_WCWL()->count_products();

			// Fragments and wishlist are returned
			$data = array(
				'fragments' => array(
					'span.wishlist-count'         => '<span class="wishlist-count">' . $wl_count . '</span>',
					'div.widget_wishlist_content' => '<div class="widget_wishlist_content">' . $wl . '</div>',
					'tm-wishlist'                 => array(
						'count' => $wl_count,
					),
				),
				'wl_hash'   => md5( json_encode( $products ) ),
			);

			wp_send_json( $data );
		}

		public function remove_wishlist_item() {
			if ( ! function_exists( 'wc_setcookie' ) || ! class_exists( 'YITH_WCWL' ) ) {
				return;
			}

			$item = isset( $_POST['item'] ) ? $_POST['item'] : '';

			if ( ! $item ) {
				return;
			}

			YITH_WCWL()->details['remove_from_wishlist'] = $item['remove_from_wishlist'];
			YITH_WCWL()->details['wishlist_id']          = $item['wishlist_id'];

			if ( is_user_logged_in() ) {
				YITH_WCWL()->details['user_id'] = get_current_user_id();
			}

			if ( YITH_WCWL()->remove() ) {
				$this->get_wishlist_fragments();
			} else {
				wp_send_json( array( 'success' => false ) );
			}
		}

		public function undo_remove_wishlist_item() {
			if ( ! function_exists( 'wc_setcookie' ) || ! class_exists( 'YITH_WCWL' ) ) {
				return;
			}

			$item = isset( $_POST['item'] ) ? $_POST['item'] : '';

			if ( ! $item ) {
				return;
			}

			YITH_WCWL()->details['add_to_wishlist'] = $item['add_to_wishlist'];
			YITH_WCWL()->details['wishlist_id']     = $item['wishlist_id'];

			if ( is_user_logged_in() ) {
				YITH_WCWL()->details['user_id'] = get_current_user_id();
			}

			if ( YITH_WCWL()->add() ) {
				$this->get_wishlist_fragments();
			} else {
				wp_send_json( array( 'success' => false ) );
			}
		}

		public static function get_cart_count() {

			ob_start();

			?>
			<span class="minicart-items-count"><?php echo WC()->cart->cart_contents_count; ?></span>
			<?php

			return ob_get_clean();
		}

		public static function get_cart_total() {

			ob_start();
			?>

			<span class="minicart-total"><?php echo WC()->cart->get_cart_total(); ?></span>

			<?php

			return ob_get_clean();
		}

		private function get_formated_cart_total( $price, $args = array() ) {

			extract( apply_filters( 'wc_price_args',
				wp_parse_args( $args,
					array(
						'ex_tax_label'       => false,
						'currency'           => '',
						'decimal_separator'  => wc_get_price_decimal_separator(),
						'thousand_separator' => wc_get_price_thousand_separator(),
						'decimals'           => wc_get_price_decimals(),
						'price_format'       => get_woocommerce_price_format(),
					) ) ) );

			$negative = $price < 0;
			$price    = apply_filters( 'raw_woocommerce_price', floatval( $negative ? $price * - 1 : $price ) );
			$price    = apply_filters( 'formatted_woocommerce_price',
				number_format( $price, $decimals, $decimal_separator, $thousand_separator ),
				$price,
				$decimals,
				$decimal_separator,
				$thousand_separator );

			if ( apply_filters( 'woocommerce_price_trim_zeros', false ) && $decimals > 0 ) {
				$price = wc_trim_zeros( $price );
			}

			$formatted_price = ( $negative ? '-' : '' ) . sprintf( $price_format,
					'<span class="woocommerce-Price-currencySymbol">' . get_woocommerce_currency_symbol( $currency ) . '</span>',
					$price );

			return $formatted_price;
		}

		public function get_cart_fragments( $fragments ) {

			$count = $this->get_cart_count();
			$total = $this->get_cart_total();

			$fragments['span.minicart-items-count'] = $count;
			$fragments['span.minicart-total']       = $total;
			$fragments['tm-minicart']               = array(
				'total' => $this->get_formated_cart_total( WC()->cart->subtotal ),
				'count' => WC()->cart->cart_contents_count,
			);

			return $fragments;

		}

		public function refresh_cart_fragments() {

			$cart_ajax = new WC_AJAX();
			$cart_ajax->get_refreshed_fragments();

			exit();
		}

		public function remove_cart_item() {
			$item = isset( $_POST['item'] ) ? $_POST['item'] : '';

			if ( $item ) {
				WC()->instance()->cart->remove_cart_item( $item );
			}

			$this->refresh_cart_fragments();
		}

		public function undo_remove_cart_item() {

			$item = isset( $_POST['item'] ) ? $_POST['item'] : '';

			if ( $item ) {
				WC()->cart->restore_cart_item( $item );
			}

			$this->refresh_cart_fragments();
		}

		/**
		 * Product categories menu on the product archive page
		 *
		 * @return string|void
		 */
		public static function product_categories_menu() {

			$args = array(
				'hide_empty'         => 0,
				'menu_order'         => 'asc',
				'show_option_all'    => '',
				'show_option_none'   => '',
				'taxonomy'           => 'product_cat',
				'title_li'           => '',
				'use_desc_for_title' => 1,
			);

			$current_cat = false;

			if ( is_tax( 'product_cat' ) ) {
				$current_cat = get_queried_object();
			}

			// Show children of current category
			if ( $current_cat ) {

				// Direct children are wanted
				$include = get_terms( 'product_cat',
					array(
						'fields'       => 'ids',
						'parent'       => $current_cat->term_id,
						'hierarchical' => true,
						'hide_empty'   => false,
					) );

				$args['include']          = implode( ',', $include );
				$args['current_category'] = ( $current_cat ) ? $current_cat->term_id : '';

				$link = ( $current_cat->parent ) ? get_category_link( $current_cat->parent ) : get_permalink( wc_get_page_id( 'shop' ) );

				if ( empty( $include ) ) { ?>
					<?php if ( is_product_category() || is_product_tag() ) { ?>
						<div class="shop-menu">
							<ul class="product-categories-menu">
								<li class="cat-item shop-back-link">
									<a href="<?php echo esc_url( $link ); ?>"><span>&larr;</span><?php esc_html_e( 'Back',
											'tm-robin' ) ?>
									</a>
								</li>
							</ul>
						</div>
						<?php
					}

					return;
				}

			} else {
				$args['child_of']     = 0;
				$args['depth']        = 1;
				$args['hierarchical'] = 1;
			}

			$args = apply_filters( 'tm_robin_product_categories_menu_args', $args );

			include_once( WC()->plugin_path() . '/includes/walkers/class-product-cat-list-walker.php' );

			ob_start();
			?>
			<div class="shop-menu">
				<a href="#" class="show-categories-menu"><?php esc_html_e( 'Categories', 'tm-robin' ) ?></a>
				<ul class="product-categories-menu">
					<?php if ( is_product_category() || is_product_tag() ) { ?>
						<?php $link = ( $current_cat->parent ) ? get_category_link( $current_cat->parent ) : get_permalink( wc_get_page_id( 'shop' ) ); ?>
						<li class="cat-item shop-back-link">
							<a href="<?php echo esc_url( $link ); ?>"><span>&larr;</span><?php esc_html_e( 'Back',
									'tm-robin' ) ?>
							</a>
						</li>
					<?php } ?>
					<?php wp_list_categories( $args ); ?>
				</ul>
			</div>

			<?php

			return ob_get_clean();
		}

		public function subcategory_count_html( $mark_class_count_category_count_mark, $category ) {
			if ( $category->count > 0 ) {
				echo ' <mark class="count">(' . $category->count . ' ' . _n( 'item',
						'items',
						$category->count,
						'tm-robin' ) . ')</mark>';
			}
		}

		/**
		 * Calculate sale percentage
		 *
		 * @param $product
		 *
		 * @return float|int
		 */
		public static function get_sale_percentage( $product ) {
			$percentage    = 0;
			$regular_price = $product->get_regular_price();
			$sale_price    = $product->get_sale_price();

			if ( $product->get_regular_price() ) {
				$percentage = - round( ( ( $regular_price - $sale_price ) / $regular_price ) * 100 );
			}

			return $percentage . '%';
		}

		public function add_to_cart_args( $args, $product ) {
			$args['attributes']['data-product_name'] = get_the_title( $product->get_id() );
			return $args;
		}

		/**
		 * Add a hover image for product thumbnail within loops
		 *
		 * @see woocommerce_template_loop_product_thumbnail()
		 */
		public function template_loop_product_thumbnail() {

			global $product;

			$id             = $product->get_id();
			$size           = 'shop_catalog';
			$gallery        = get_post_meta( $id, '_product_image_gallery', true );
			$products_hover = tm_robin_get_option( 'products_hover' );
			$hover_img      = '';

			if ( $products_hover == 'info' ) {
				$size = array(
					apply_filters( 'tm_robin_hover_info_image_width',
						intval( get_option( 'shop_catalog_image_size', array( 'width' => 270 ) )['width'] ) * 1.8 ),
					apply_filters( 'tm_robin_hover_info_image_height',
						intval( get_option( 'shop_catalog_image_size', array( 'height' => 360 ) )['height'] ) * 1.8 ),
				);
			}

			$size = apply_filters( 'tm_robin_single_product_archive_thumbnail_size', $size );

			if ( ! empty( $gallery ) ) {
				$gallery        = explode( ',', $gallery );
				$first_image_id = $gallery[0];
				$hover_img      = wp_get_attachment_image( $first_image_id,
					$size,
					false,
					array( 'class' => 'hover-image' ) );
			}

			$thumb_img = get_the_post_thumbnail( $id, $size, array( 'class' => 'thumb-image' ) );
			if ( ! $thumb_img ) {
				if ( wc_placeholder_img_src() ) {
					$thumb_img = wc_placeholder_img( $size );
				}
			}

			echo '' . $thumb_img;
			echo '' . $hover_img;
		}

		/**
		 * Custom product title instead of default product title
		 *
		 * @see woocommerce_template_loop_product_title()
		 */
		public function template_loop_product_title() {
			echo '<h3 class="product-title"><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h3>';
		}

		/**
		 * Product short description
		 */
		public function template_loop_product_excerpt() {
			global $post;

			echo '<div class="product-description">';
			echo apply_filters( 'woocommerce_short_description', $post->post_excerpt );
			echo '</div>';
		}

		/**
		 * Wishlist button
		 */
		public static function wishlist_button() {
			if ( class_exists( 'YITH_WCWL' ) && 'yes' == get_option( 'yith_wcwl_enabled' ) && tm_robin_get_option( 'shop_wishlist_on' ) ) {
				echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
			}
		}

		/**
		 * Quickview button
		 */
		public static function quick_view_button() {

			ob_start();

			global $product;
			$id = $product->get_id();

			if ( tm_robin_get_option( 'shop_quick_view_on' ) && ! wp_is_mobile() ) { ?>
				<div class="quick-view-btn hint--top hint-bounce"
				     aria-label="<?php esc_html_e( 'Quick View', 'tm-robin' ); ?>"
				     data-pid="<?php echo esc_attr( $id ) ?>"
				     data-pnonce="<?php echo esc_attr( wp_create_nonce( 'tm_robin_quick_view' ) ); ?>">
					<a href="#" aria-label="<?php esc_html_e( 'Quick View', 'tm-robin' ); ?>" rel="nofollow">
						<?php esc_html_e( 'Quick View', 'tm-robin' ); ?>
					</a>
				</div>
			<?php }

			echo ob_get_clean();
		}

		/**
		 * Compare button
		 */
		public static function compare_button() {

			ob_start();

			global $product;
			$id = $product->get_id();

			$button_text = get_option( 'yith_woocompare_button_text', esc_html__( 'Compare', 'tm-robin' ) );

			if ( function_exists( 'yith_wpml_register_string' ) && function_exists( 'yit_wpml_string_translate' ) ) {
				yit_wpml_register_string( 'Plugins', 'plugin_yit_compare_button_text', $button_text );
				$button_text = yit_wpml_string_translate( 'Plugins', 'plugin_yit_compare_button_text', $button_text );
			}

			if ( class_exists( 'YITH_Woocompare' ) && tm_robin_get_option( 'shop_compare_on' ) && ! wp_is_mobile() ) { ?>
				<div class="compare-btn hint--top hint--bounce"
				     aria-label="<?php esc_html_e( 'Compare', 'tm-robin' ); ?>">
					<?php
					printf( '<a href="%s" class="%s" data-product_id="%d" rel="nofollow">%s</a>',
						self::get_compare_add_product_url( $id ),
						'compare',
						$id,
						$button_text );
					?>
				</div>
			<?php }

			echo ob_get_clean();
		}

		/**
		 * Get compare URL
		 */
		private static function get_compare_add_product_url( $product_id ) {

			$action_add = 'yith-woocompare-add-product';

			$url_args = array(
				'action' => $action_add,
				'id'     => $product_id,
			);

			return apply_filters( 'yith_woocompare_add_product_url',
				esc_url_raw( add_query_arg( $url_args ) ),
				$action_add );
		}

		public function quick_view() {

			global $post, $product;

			$post = get_post( $_REQUEST['pid'] );
			setup_postdata( $post );

			$product = wc_get_product( $post->ID );

			ob_start();

			get_template_part( 'woocommerce/quick-view' );

			echo ob_get_clean();

			die();
		}

		public function add_quick_view_container() {
			echo '<div id="woo-quick-view" class="' . ( tm_robin_get_option( 'animated_quick_view_on' ) ? 'animated-quick-view' : '' ) . '"></div>';
		}

		public function ajax_add_to_cart() {

			global $woocommerce;

			$variation_id      = ( isset( $_POST['variation_id'] ) ) ? $_POST['variation_id'] : '';
			$_POST['quantity'] = ( isset( $_POST['quantity'] ) ) ? $_POST['quantity'] : 1;

			$variations = array();

			foreach ( $_POST as $key => $value ) {
				if ( substr( $key, 0, 10 ) == 'attribute_' ) {
					$variations[ $key ] = $value;
				}
			}

			if ( is_array( $_POST['quantity'] ) && ! empty( $_POST['quantity'] ) ) { // grouped product
				$quantity_set = false;

				foreach ( $_POST['quantity'] as $id => $qty ) {

					if ( $qty > 0 ) {

						$quantity_set = true;
						$atc          = $woocommerce->cart->add_to_cart( $id, $qty );

						if ( $atc ) {
							continue;
						} else {
							break;
						}
					}
				}

				if ( ! $quantity_set ) {
					$response            = array( 'result' => 'fail' );
					$response['message'] = esc_html__( 'Please choose the quantity of items you wish to add to your cart.',
						'tm-robin' );
				}

			} else { // simple & variable product
				$atc = $woocommerce->cart->add_to_cart( $_POST['pid'], $_POST['quantity'], $variation_id, $variations );
			}

			if ( $atc ) {
				$this->refresh_cart_fragments();
			} else {

				$sold_indv = get_post_meta( $_POST['pid'], '_sold_individually', true );

				if ( $sold_indv == 'yes' ) {
					$response            = array( 'result' => 'fail' );
					$response['message'] = esc_html__( 'Sorry, that item can only be added once.', 'tm-robin' );
				} else {

					if ( ! is_array( $_POST['quantity'] ) ) {
						$response            = array( 'result' => 'fail' );
						$response['message'] = esc_html__( 'Sorry, something went wrong. Please try again.',
							'tm-robin' );
					}
				}

				$response['post'] = $_POST;

				wp_send_json( $response );
			}
		}

		public function photoswipe_template() {
			if ( is_product() ) {
				wc_get_template_part( 'photo-swipe' );
			}
		}

		/**
		 * Get Instagram image by hashtag for product
		 *
		 * @return string|void
		 */
		public static function product_instagram() {

			$hashtag = get_post_meta( TM_Robin_Helper::get_the_ID(), 'tm_robin_product_hashtag', true );

			if ( ! $hashtag ) {
				return;
			}

			ob_start();

			$number = apply_filters( 'tm_robin_product_instagram_number', 8 );
			$col    = apply_filters( 'tm_robin_product_instagram_columns', 4 );
			$class  = TM_Robin_Helper::get_grid_item_class( array(
				'xl' => $col,
				'lg' => 4,
				'md' => 2,
				'sm' => 1,
			) );

			?>
			<div class="product-instagram">
				<div class="container">
					<p><?php printf( wp_kses( __( 'Tag your photos with <a href="%s">%s</a> on Instagram.',
							'tm-robin' ),
							array( 'a' => array( 'href' => array() ) ) ),
							esc_url( 'https://www.instagram.com/explore/tags/' . $hashtag ),
							$hashtag ); ?></p>
					<?php

					$media_array = TM_Robin_Instagram::get_instance()->scrape_instagram( $hashtag, $number );

					if ( is_wp_error( $media_array ) ) { ?>
						<div class="tm-instagram--error">
							<p><?php echo wp_kses_post( $media_array->get_error_message() ); ?></p>
						</div>
					<?php } else { ?>
						<div class="tm-instagram-pics row">
							<?php foreach ( $media_array as $item ) { ?>
								<div class="item <?php echo esc_attr( $class ) ?>">
									<div class="item-info">
										<span><?php esc_html_e( 'View on Instagram', 'tm-robin' ); ?></span>
									</div>
									<img src="<?php echo esc_url( $item['thumbnail'] ) ?>" alt="" class="item-image"/>
									<?php if ( 'video' == $item['type'] ) { ?>
										<span class="play-button"></span>
									<?php } ?>

									<div class="overlay">
										<a href="<?php echo esc_url( $item['link'] ); ?>"
										   target="<?php echo apply_filters( 'tm_robin_product_instagram_link_target',
											   '' ); ?>"><?php esc_html_e( 'View on Instagram', 'tm-robin' ) ?></a>
									</div>

								</div>
							<?php } ?>
						</div>

					<?php } ?>
				</div>
			</div>
			<?php

			return ob_get_clean();
		}

		/**
		 * Get shop layout
		 *
		 * @return mixed List or Grid
		 */
		public static function get_shop_view_mode() {

			if ( ! isset( $_COOKIE['tm_robin_archive_view_mode'] ) || ! $_COOKIE['tm_robin_archive_view_mode'] ) {
				$layout = tm_robin_get_option( 'shop_view_mode' );
			} else {
				$layout = $_COOKIE['tm_robin_archive_view_mode'];
			}

			return $layout;
		}

		/**
		 * Display variation name
		 * The product must be get by $_product->get_title() instead of $_product->get_name()
		 *
		 * The product in cart will be display like this: http://prntscr.com/fe1ylt
		 * instead of this: http://prntscr.com/fe1yt9
		 *
		 * @param $is_in_name
		 * @param $attribute
		 * @param $name
		 *
		 * @return bool
		 */
		public function is_attribute_in_product_name( $is_in_name, $attribute, $name ) {
			return false;
		}

		/**
		 * Get product by given data source
		 *
		 * @param $data_source
		 * @param $atts array
		 * @param $args array Additional arguments
		 *
		 * @return mixed|WP_Query
		 */
		public static function get_products_by_datasource( $data_source, $atts, $args = array() ) {

			$defaults = array(
				'post_type'           => 'product',
				'status'              => 'published',
				'ignore_sticky_posts' => 1,
				'orderby'             => $atts['orderby'],
				'order'               => $atts['order'],
				'posts_per_page'      => intval( $atts['number'] ) > 0 ? intval( $atts['number'] ) : 1000,
			);

			$args = wp_parse_args( $args, $defaults );

			switch ( $data_source ) {
				case 'featured_products';
					if ( version_compare( WC()->version, '3.0.0', '<' ) ) {
						$args['meta_key']   = '_featured';
						$args['meta_value'] = 'yes';
					} else {
						$args['tax_query'] = array(
							array(
								'taxonomy' => 'product_visibility',
								'field'    => 'name',
								'terms'    => array( 'featured' ),
								'operator' => 'IN',
							),
						);
					}
					break;
				case 'sale_products';
					$product_ids_on_sale   = wc_get_product_ids_on_sale();
					$product_ids_on_sale[] = 0;
					$args['post__in']      = $product_ids_on_sale;
					break;
				case 'best_selling_products';
					$args['meta_key'] = 'total_sales';
					$args['orderby']  = 'meta_value_num';
					$args['order']    = 'DESC';
					break;
				case 'top_rated_products';
					$args['meta_key'] = '_wc_average_rating';
					$args['orderby']  = 'meta_value_num';
					$args['order']    = 'DESC';
					break;
				case 'product_attribute';
					$args['tax_query'] = array(
						array(
							'taxonomy' => strstr( $atts['attribute'],
								'pa_' ) ? sanitize_title( $atts['attribute'] ) : 'pa_' . sanitize_title( $atts['attribute'] ),
							'field'    => 'slug',
							'terms'    => array_map( 'sanitize_title', explode( ',', $atts['filter'] ) ),
						),
					);
					break;
				case 'products';
					if ( $atts['product_ids'] != '' ) {
						$args['post__in'] = explode( ',', $atts['product_ids'] );
					}
					break;
				case 'categories';
					$args['tax_query'] = array(
						'relation' => 'OR',
						array(
							'taxonomy'         => 'product_cat',
							'field'            => 'slug',
							'terms'            => explode( ',', $atts['product_cat_slugs'] ),
							'include_children' => $atts['include_children'],
						),
					);
					break;
				case 'category';
					$args['tax_query'] = array(
						'relation' => 'OR',
						array(
							'taxonomy'         => 'product_cat',
							'field'            => 'slug',
							'terms'            => $atts['category'],
							'include_children' => $atts['include_children'],
						),
					);
					break;
				case 'recent_products';
				default:
					$args['orderby'] = 'date';
					$args['order']   = 'DESC';
					break;
			}

			switch ( $atts['orderby'] ) {
				case 'price':
					$args['meta_key'] = '_price';
					$args['orderby']  = 'meta_value_num';
					break;
				case 'salse':
					$args['meta_key'] = 'total_sales';
					$args['orderby']  = 'meta_value_num';
					break;
				case 'rating':
					$args['meta_key'] = '_wc_average_rating';
					$args['orderby']  = 'meta_value_num';
					break;
			}

			if ( ! empty( $atts['exclude'] ) ) {
				$args['post__not_in'] = explode( ',', $atts['exclude'] );
			}

			$transient_name = 'tm_robin_wc_loop' . substr( md5( json_encode( $args ) . $data_source ),
					28 ) . WC_Cache_Helper::get_transient_version( 'product_query' );
			$query          = get_transient( $transient_name );

			if ( false === $query || ! is_a( $query, 'WP_Query' ) ) {
				$query = new WP_Query( $args );
				set_transient( $transient_name, $query, DAY_IN_SECONDS * 30 );
			}

			return $query;

		}
	}

	new TM_Robin_Woo();
}
