<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Initial OneClick import for this theme
 *
 * @package   InsightFramework
 */
if ( ! class_exists( 'TM_Robin_Import' ) ) {

	class TM_Robin_Import {

		/**
		 * The constructor.
		 */
		public function __construct() {
			// Import Demo.
			add_filter( 'insight_core_import_demos', array( $this, 'import_demos' ) );

			// generate thumbnail
			add_filter( 'insight_core_import_generate_thumb', array( $this, 'generate_thumb' ) );
		}

		/**
		 * Import Demo
		 *
		 * @since 1.0
		 */
		public function import_demos() {
			return array(
				'01' => array(
					'screenshot' => TM_ROBIN_THEME_URI . TM_Robin_Helper::get_config( 'screenshot' ),
					'name'       => TM_ROBIN_THEME_NAME,
					'url'        => TM_Robin_Helper::get_config( 'import_package_url' ),
				),
			);
		}

		public function generate_thumb() {
			return true;
		}
	}

	new TM_Robin_Import();
}
