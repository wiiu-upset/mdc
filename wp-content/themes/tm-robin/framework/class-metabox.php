<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Add Insight
 *
 * @package   InsightFramework
 */
if ( ! class_exists( 'TM_Robin_Metabox' ) ) {

	class TM_Robin_Metabox {

		private $prefix = 'tm_robin_';
		private $transfer_options = array();

		/**
		 * Insight_Metabox constructor.
		 */
		public function __construct() {

			add_action( 'wp', array( $this, 'modify_global_settings' ), 10 );

			// Use CMB2 Meta box for taxonomies & terms
			add_action( 'cmb2_init', array( $this, 'pages_meta_boxes' ) );
			add_action( 'cmb2_init', array( $this, 'post_meta_boxes' ) );
			add_action( 'cmb2_init', array( $this, 'product_meta_boxes' ) );
			add_action( 'cmb2_init', array( $this, 'testimonials_meta_boxes' ) );
			add_action( 'cmb2_init', array( $this, 'post_categories_meta_boxes' ) );
			add_action( 'cmb2_init', array( $this, 'product_categories_meta_boxes' ) );
		}

		/**
		 * Meta boxes for pages
		 */
		public function pages_meta_boxes() {

			$topbar_fields = $logo_fields = $header_fields = $menu_fields = $page_title_fields = $breadcrumbs_fields = $footer_fields = array();

			$page_sidebar_config = $this->redux2metabox( 'page_sidebar_config' );

			$topbar_transfer_options = array(
				'topbar_on',
				'topbar_layout',
				'topbar_text',
				'topbar_width',
				'topbar_social',
				'topbar_menu',
				'topbar_language_switcher_on',
				'topbar_currency_switcher_on',
				'topbar_color',
				'topbar_bgcolor',
				'topbar_bdcolor',
			);

			$logo        = tm_robin_get_option( 'logo' );
			$logo_alt    = tm_robin_get_option( 'logo_alt' );
			$logo_mobile = tm_robin_get_option( 'logo_mobile' );

			$logo_fields = array(

				array(
					'name'    => esc_html__( 'Custom Logo', 'tm-robin' ),
					'id'      => $this->prefix . 'custom_logo',
					'type'    => 'select',
					'desc'    => esc_html__( 'Use custom logo on this page.', 'tm-robin' ),
					'options' => array(
						'on' => esc_html__( 'Yes', 'tm-robin' ),
						''   => esc_html__( 'No', 'tm-robin' ),
					),
					'default' => '',
				),
				array(
					'name'    => esc_html__( 'Logo Image', 'tm-robin' ),
					'id'      => $this->prefix . 'logo',
					'type'    => 'file',
					'default' => ( isset( $logo['url'] ) && $logo['url'] ) ? $logo['url'] : '',
				),
				array(
					'name'    => esc_html__( 'Alternative Logo Image', 'tm-robin' ),
					'id'      => $this->prefix . 'logo_alt',
					'desc'    => esc_html__( 'for the header above the content', 'tm-robin' ),
					'type'    => 'file',
					'default' => ( isset( $logo_alt['url'] ) && $logo_alt['url'] ) ? $logo_alt['url'] : '',
				),
				array(
					'name'    => esc_html__( 'Logo in mobile devices', 'tm-robin' ),
					'id'      => $this->prefix . 'logo_mobile',
					'type'    => 'file',
					'default' => ( isset( $logo_mobile['url'] ) && $logo_mobile['url'] ) ? $logo_mobile['url'] : '',
				),
			);

			$logo_transfer_options = array(
				'logo_width',
			);

			$header_transfer_options = array(
				'header',
				'sticky_header',
				'header_social',
				'header_overlap',
				'right_column_width',
				'header_bgcolor',
				'header_bdcolor',
				'search_color',
				'minicart_icon_color',
				'minicart_text_color',
				'minicart_count_bg_color',
			);

			if ( class_exists( 'YITH_WCWL' ) ) {
				$header_transfer_options = array_merge( $header_transfer_options,
					array(
						'wishlist_icon_color',
						'wishlist_text_color',
						'wishlist_count_bg_color',
					) );
			}

			$menu_transfer_options = array(
				'site_menu_items_color',
				'site_menu_subitems_color',
				'site_menu_bgcolor',
			);

			$page_title_fields = array(
				// custom page title
				array(
					'name' => esc_html__( 'Custom Page Title', 'tm-robin' ),
					'id'   => $this->prefix . 'custom_page_title',
					'type' => 'text',
				),
				// custom subtitle
				array(
					'name' => esc_html__( 'Sub Title', 'tm-shopie' ),
					'id'   => $this->prefix . 'subtitle',
					'type' => 'text',
				),
			);

			$page_title_transfer_options = array(
				'page_title_on',
				'remove_whitespace',
				'page_title_style',
				'page_title_text_color',
				'page_subtitle_color',
				'page_title_bg_color',
				'page_title_overlay_color',
				'page_title_bg_image',
			);

			$breadcrumbs_transfer_options = array(
				'breadcrumbs',
				'breadcrumbs_position',
			);

			// Footer
			$footer_fields[] = array(
				'name'    => esc_html__( 'Footer Visibility', 'tm-robin' ),
				'id'      => $this->prefix . 'disable_footer',
				'type'    => 'select',
				'desc'    => esc_html__( 'Enable or disable footer on this page.', 'tm-robin' ),
				'options' => array(
					''   => esc_html__( 'Enable', 'tm-robin' ),
					'on' => esc_html__( 'Disable', 'tm-robin' ),
				),
				'default' => '',
			);

			$footer_fields[] = array(
				'name'     => esc_html__( 'Widget Area for Footer column #1', 'tm-robin' ),
				'id'       => $this->prefix . 'footer_custom_sidebar_1',
				'type'     => 'select',
				'options'  => TM_Robin_Helper::get_registered_sidebars( true, true ),
				'multiple' => false,
				'default'  => 'sidebar-footer-1',
			);

			$footer_fields[] = array(
				'name'     => esc_html__( 'Widget Area for Footer column #2', 'tm-robin' ),
				'id'       => $this->prefix . 'footer_custom_sidebar_2',
				'type'     => 'select',
				'options'  => TM_Robin_Helper::get_registered_sidebars( true, true ),
				'multiple' => false,
				'default'  => 'sidebar-footer-2',
			);

			$footer_fields[] = array(
				'name'     => esc_html__( 'Widget Area for Footer column #3', 'tm-robin' ),
				'id'       => $this->prefix . 'footer_custom_sidebar_3',
				'type'     => 'select',
				'options'  => TM_Robin_Helper::get_registered_sidebars( true, true ),
				'multiple' => false,
				'default'  => 'sidebar-footer-3',
			);

			$footer_fields[] = array(
				'name'     => esc_html__( 'Widget Area for Footer column #4', 'tm-robin' ),
				'id'       => $this->prefix . 'footer_custom_sidebar_4',
				'type'     => 'select',
				'options'  => TM_Robin_Helper::get_registered_sidebars( true, true ),
				'multiple' => false,
				'default'  => 'sidebar-footer-4',
			);

			$footer_transfer_options = array(
				'footer_layout',
				'footer_width',
				'footer_color_scheme',
				'footer_bgcolor',
				'footer_color',
				'footer_accent_color',
				'footer_copyright_bgcolor',
				'footer_copyright_color',
				'footer_copyright_link_color',
				'footer_copyright',
			);

			foreach ( $topbar_transfer_options as $option ) {
				$topbar_fields[] = $this->redux2metabox( $option );
			}

			foreach ( $logo_transfer_options as $option ) {
				$logo_fields[] = $this->redux2metabox( $option );
			}

			foreach ( $header_transfer_options as $option ) {
				$header_fields[] = $this->redux2metabox( $option );
			}

			foreach ( $menu_transfer_options as $option ) {
				$menu_fields[] = $this->redux2metabox( $option );

				if ( $option == 'site_menu_items_color' ) {

					$site_menu_items_color = tm_robin_get_option( 'site_menu_items_color' );

					$menu_fields[] = array(
						'name'    => esc_html__( 'Menu Item Color on hover', 'tm-robin' ),
						'desc'    => esc_html__( 'Pick color for the menu items on hover', 'tm-robin' ),
						'id'      => $this->prefix . 'site_menu_items_color_hover',
						'type'    => 'colorpicker',
						'default' => isset( $site_menu_items_color['hover'] ) ? $site_menu_items_color['hover'] : '',
					);
				}

				if ( $option == 'site_menu_subitems_color' ) {

					$site_menu_subitems_color = tm_robin_get_option( 'site_menu_subitems_color' );

					$menu_fields[] = array(
						'name'    => esc_html__( 'Menu Sub Item Color on hover', 'tm-robin' ),
						'desc'    => esc_html__( 'Pick color for the menu sub items on hover', 'tm-robin' ),
						'id'      => $this->prefix . 'site_menu_subitems_color_hover',
						'type'    => 'colorpicker',
						'default' => isset( $site_menu_subitems_color['hover'] ) ? $site_menu_subitems_color['hover'] : '',
					);
				}
			}

			foreach ( $page_title_transfer_options as $option ) {
				$page_title_fields[] = $this->redux2metabox( $option );
			}

			foreach ( $breadcrumbs_transfer_options as $option ) {
				$breadcrumbs_fields[] = $this->redux2metabox( $option );
			}

			foreach ( $footer_transfer_options as $option ) {
				$footer_fields[] = $this->redux2metabox( $option );

				if ( $option == 'footer_copyright_link_color' ) {
					$footer_copyright_link_color = tm_robin_get_option( 'footer_copyright_link_color' );

					$footer_fields[] = array(
						'name'    => esc_html__( 'Copyright link Color on hover', 'tm-robin' ),
						'id'      => $this->prefix . 'footer_copyright_link_color_hover',
						'type'    => 'colorpicker',
						'default' => isset( $footer_copyright_link_color['hover'] ) ? $footer_copyright_link_color['hover'] : '',
					);
				}
			}

			$box_options = array(
				'id'           => $this->prefix . 'page_meta_box',
				'title'        => esc_html__( 'Page Settings (custom metabox from theme)', 'tm-robin' ),
				'object_types' => array( 'page' ),
			);

			// tabs
			$tabs = array(
				'config' => $box_options,
				'layout' => 'vertical',
				'tabs'   => array(),
			);

			// Top bar
			$tabs['tabs'][] = array(
				'id'     => 'tab1',
				'title'  => esc_html__( 'Top Bar', 'tm-robin' ),
				'fields' => $topbar_fields,
			);

			// Logo
			$tabs['tabs'][] = array(
				'id'     => 'tab2',
				'title'  => esc_html__( 'Custom Logo', 'tm-robin' ),
				'fields' => $logo_fields,
			);

			// Header
			$header_fields[] = array(
				'name'     => esc_html__( 'Left Sidebar', 'tm-robin' ),
				'id'       => $this->prefix . 'header_left_sidebar',
				'type'     => 'select',
				'options'  => TM_Robin_Helper::get_registered_sidebars( true, true ),
				'multiple' => false,
				'std'      => 'sidebar',
				'default'  => 'sidebar-header-left',
			);
			$header_fields[] = array(
				'name'     => esc_html__( 'Right Sidebar', 'tm-robin' ),
				'id'       => $this->prefix . 'header_right_sidebar',
				'type'     => 'select',
				'options'  => TM_Robin_Helper::get_registered_sidebars( true, true ),
				'multiple' => false,
				'std'      => 'sidebar',
				'default'  => 'sidebar-header-right',
			);

			$tabs['tabs'][] = array(
				'id'     => 'tab3',
				'title'  => esc_html__( 'Header', 'tm-robin' ),
				'fields' => $header_fields,
			);

			// Menu
			$tabs['tabs'][] = array(
				'id'     => 'tab4',
				'title'  => esc_html__( 'Menu', 'tm-robin' ),
				'fields' => $menu_fields,
			);

			// Page title
			$tabs['tabs'][] = array(
				'id'     => 'tab5',
				'title'  => esc_html__( 'Page Title', 'tm-robin' ),
				'fields' => $page_title_fields,
			);

			// Breadcrumb
			$tabs['tabs'][] = array(
				'id'     => 'tab6',
				'title'  => esc_html__( 'Breadcrumbs', 'tm-robin' ),
				'fields' => $breadcrumbs_fields,
			);

			// Sidebar
			$tabs['tabs'][] = array(
				'id'     => 'tab7',
				'title'  => esc_html__( 'Sidebar Options', 'tm-robin' ),
				'fields' => array(
					$page_sidebar_config,

					// Custom sidebar.
					array(
						'name'     => esc_html__( 'Custom sidebar for this page', 'tm-robin' ),
						'id'       => $this->prefix . 'page_custom_sidebar',
						'type'     => 'select',
						'options'  => TM_Robin_Helper::get_registered_sidebars(),
						'multiple' => false,
						'std'      => 'sidebar',
					),
				),
			);

			$tabs['tabs'][] = array(
				'id'     => 'tab8',
				'title'  => esc_html__( 'Footer', 'tm-robin' ),
				'fields' => $footer_fields,
			);

			// Page Meta
			$tabs['tabs'][] = array(
				'id'     => 'tab9',
				'title'  => esc_html__( 'Page Meta', 'tm-robin' ),
				'fields' => array(
					// Extra Page Class.
					array(
						'name' => esc_html__( 'Page extra class name', 'tm-robin' ),
						'id'   => $this->prefix . 'page_extra_class',
						'type' => 'text',
						'desc' => esc_html__( 'If you wish to add extra classes to the body class of the page (for custom css use), then please add the class(es) here.',
							'tm-robin' ),
					),
				),
			);


			$cmb = new_cmb2_box( $box_options );

			$cmb->add_field( array(
				'id'   => $this->prefix . 'page_tabs',
				'type' => 'tabs',
				'tabs' => $tabs,
			) );

			$this->transfer_options = array_merge( $this->transfer_options,
				$topbar_transfer_options,
				$logo_transfer_options,
				$header_transfer_options,
				$menu_transfer_options,
				$page_title_transfer_options,
				$breadcrumbs_transfer_options,
				$footer_transfer_options,
				array(
					'page_sidebar_config',
				) );
		}

		/**
		 * Meta boxes for posts
		 */
		public function post_meta_boxes() {

			$breadcrumbs_fields = array();

			$post_sidebar_config = $this->redux2metabox( 'post_sidebar_config' );

			$post_fields = array(
				// Show the post title on top
				array(
					'name'        => esc_html__( 'Display the Post title on top', 'tm-robin' ),
					'id'          => $this->prefix . 'post_title_on',
					'type'        => 'checkbox',
					'description' => esc_html__( 'Enabling this option will display the title of this post on top',
						'tm-robin' ),
				),
				// custom page title
				array(
					'name' => esc_html__( 'Custom Page Title', 'tm-robin' ),
					'id'   => $this->prefix . 'custom_page_title',
					'type' => 'text',
				),
				// custom subtitle
				array(
					'name' => esc_html__( 'Sub Title', 'tm-shopie' ),
					'id'   => $this->prefix . 'subtitle',
					'type' => 'text',
				),
			);

			$post_transfer_options = array(
				'page_title_on',
				'page_title_style',
				'page_title_text_color',
				'page_subtitle_color',
				'page_title_bg_color',
				'page_title_overlay_color',
				'page_title_bg_image',
			);

			$breadcrumbs_transfer_options = array(
				'breadcrumbs',
				'breadcrumbs_position',
			);

			foreach ( $post_transfer_options as $option ) {
				$post_fields[] = $this->redux2metabox( $option );
			}

			foreach ( $breadcrumbs_transfer_options as $option ) {
				$breadcrumbs_fields[] = $this->redux2metabox( $option );
			}

			$box_options = array(
				'id'           => $this->prefix . 'post_meta_box',
				'title'        => esc_html__( 'Post Settings (custom metabox from theme)', 'tm-robin' ),
				'object_types' => array( 'post' ),
			);

			// tabs
			$tabs = array(
				'config' => $box_options,
				'layout' => 'vertical',
				'tabs'   => array(),
			);

			$tabs['tabs'][] = array(
				'id'     => 'tab1',
				'title'  => esc_html__( 'Page Title', 'tm-robin' ),
				'fields' => $post_fields,
			);

			$tabs['tabs'][] = array(
				'id'     => 'tab2',
				'title'  => esc_html__( 'Breadcrumbs', 'tm-robin' ),
				'fields' => $breadcrumbs_fields,
			);

			$tabs['tabs'][] = array(
				'id'     => 'tab3',
				'title'  => esc_html__( 'Sidebar Options', 'tm-robin' ),
				'fields' => array(
					$post_sidebar_config,

					// Custom sidebar.
					array(
						'name'     => esc_html__( 'Custom sidebar for this page', 'tm-robin' ),
						'id'       => $this->prefix . 'post_custom_sidebar',
						'type'     => 'select',
						'options'  => TM_Robin_Helper::get_registered_sidebars(),
						'multiple' => false,
						'std'      => 'sidebar',
					),
				),
			);

			$cmb = new_cmb2_box( $box_options );

			$cmb->add_field( array(
				'id'   => $this->prefix . 'post_tabs',
				'type' => 'tabs',
				'tabs' => $tabs,
			) );

			$this->transfer_options = array_merge( $this->transfer_options,
				$post_transfer_options,
				$breadcrumbs_transfer_options,
				array(
					'post_sidebar_config',
				) );
		}

		/**
		 * Meta boxes for Testimonials
		 *
		 * @return array
		 */
		public function testimonials_meta_boxes() {

			new_cmb2_box( array(
				'id'           => $this->prefix . 'testimonial_meta_box',
				'title'        => esc_html__( 'Testimonial Settings (custom metabox from theme)', 'tm-robin' ),
				'object_types' => array( 'testimonials' ),
				'fields'       => array(
					array(
						'name' => __( 'Testimonial Cite', 'tm-robin' ),
						'id'   => $this->prefix . 'testimonial_cite',
						'desc' => __( 'Enter the cite name for the testimonial.', 'tm-robin' ),
						'type' => 'text',
					),

					array(
						'name' => __( 'Testimonial Cite Subtext', 'tm-robin' ),
						'id'   => $this->prefix . 'testimonial_cite_subtext',
						'desc' => __( 'Enter the cite subtext for the testimonial (optional).', 'tm-robin' ),
						'type' => 'text',
					),

					array(
						'name' => __( 'Testimonial Cite Image', 'tm-robin' ),
						'desc' => __( 'Enter the cite image for the testimonial (optional).', 'tm-robin' ),
						'id'   => $this->prefix . 'testimonial_cite_image',
						'type' => 'file',
					),
				),
			) );

		}

		/**
		 * Meta boxes for product
		 */
		public function product_meta_boxes() {

			$product_sidebar_config = $this->redux2metabox( 'product_sidebar_config' );

			$product_title_fields = array(
				// Show the post title on top
				array(
					'name'        => esc_html__( 'Display the Product title on top', 'tm-robin' ),
					'id'          => $this->prefix . 'product_title_on',
					'type'        => 'checkbox',
					'description' => esc_html__( 'Enabling this option will display the title of this product on top',
						'tm-robin' ),
				),
				// custom page title
				array(
					'name' => esc_html__( 'Custom Page Title', 'tm-robin' ),
					'id'   => $this->prefix . 'custom_page_title',
					'type' => 'text',
				),
			);

			$product_fields = array(

				// Hide Related Products.
				array(
					'name'    => esc_html__( 'Hide Related Products', 'tm-robin' ),
					'id'      => $this->prefix . 'hide_related_products',
					'type'    => 'checkbox',
					'desc'    => esc_html__( 'Check to hide related products on this page', 'tm-robin' ),
					'default' => false,
				),

				// Instagram hash tag.
				array(
					'name' => esc_html__( 'Instagram Hashtag', 'tm-robin' ),
					'id'   => $this->prefix . 'product_hashtag',
					'type' => 'text',
					'desc' => __( 'Enter hashtag will be used to display images from Instagram. (For example: <strong>#women</strong>)',
						'tm-robin' ),
				),
			);

			$product_transfer_options = array(
				'product_thumbnails_position',
				'product_page_layout',
			);

			$product_title_transfer_options = array(
				'page_title_on',
				'page_title_style',
				'page_title_text_color',
				'page_subtitle_color',
				'page_title_bg_color',
				'page_title_overlay_color',
				'page_title_bg_image',
			);

			$breadcrumbs_transfer_options = array(
				'breadcrumbs',
				'breadcrumbs_position',
			);

			foreach ( $product_transfer_options as $option ) {
				$product_fields[] = $this->redux2metabox( $option, '', true );
			}

			foreach ( $product_title_transfer_options as $option ) {
				$product_title_fields[] = $this->redux2metabox( $option );
			}

			foreach ( $breadcrumbs_transfer_options as $option ) {
				$breadcrumbs_fields[] = $this->redux2metabox( $option );
			}

			$box_options = array(
				'id'           => $this->prefix . 'product_meta_box',
				'title'        => esc_html__( 'Product Settings (custom metabox from theme)', 'tm-robin' ),
				'object_types' => array( 'product' ),
			);

			// tabs
			$tabs = array(
				'config' => $box_options,
				'layout' => 'vertical',
				'tabs'   => array(),
			);

			$tabs['tabs'][] = array(
				'id'     => 'tab1',
				'title'  => esc_html__( 'General', 'tm-robin' ),
				'fields' => $product_fields,
			);

			$tabs['tabs'][] = array(
				'id'     => 'tab2',
				'title'  => esc_html__( 'Page Title', 'tm-robin' ),
				'fields' => $product_title_fields,
			);

			$tabs['tabs'][] = array(
				'id'     => 'tab3',
				'title'  => esc_html__( 'Breadcrumbs', 'tm-robin' ),
				'fields' => $breadcrumbs_fields,
			);

			$tabs['tabs'][] = array(
				'id'     => 'tab4',
				'title'  => esc_html__( 'Sidebar Options', 'tm-robin' ),
				'fields' => array(
					$product_sidebar_config,

					// Custom sidebar.
					array(
						'name'     => esc_html__( 'Custom sidebar for this post', 'tm-robin' ),
						'id'       => $this->prefix . 'product_custom_sidebar',
						'type'     => 'select',
						'options'  => TM_Robin_Helper::get_registered_sidebars(),
						'multiple' => false,
						'std'      => 'sidebar-shop',
					),
				),
			);

			$cmb = new_cmb2_box( $box_options );

			$cmb->add_field( array(
				'id'   => $this->prefix . 'product_tabs',
				'type' => 'tabs',
				'tabs' => $tabs,
			) );

			$this->transfer_options = array_merge( $this->transfer_options,
				$product_transfer_options,
				$product_title_transfer_options,
				$breadcrumbs_transfer_options,
				array(
					'product_sidebar_config',
				) );

		}

		/**
		 * Meta boxes for post categories
		 */
		public function post_categories_meta_boxes() {

			$archive_fields = array();

			$archive_transfer_options = array(
				'archive_display_type',
				'page_title_on',
				'page_title_style',
				'page_title_text_color',
				'page_title_bg_color',
				'page_title_overlay_color',
				'page_title_bg_image',
			);

			foreach ( $archive_transfer_options as $option ) {
				$archive_fields[] = $this->redux2metabox( $option );
			}

			new_cmb2_box( array(
				'id'           => $this->prefix . 'post_categories_meta_box',
				'title'        => esc_html__( 'Category Meta Box', 'tm-robin' ),
				'object_types' => array( 'term' ),
				'taxonomies'   => array( 'category' ),
				'fields'       => $archive_fields,
			) );

			$this->transfer_options = array_merge( $this->transfer_options,
				$archive_transfer_options );
		}

		/**
		 * Meta box for product categories
		 */
		public function product_categories_meta_boxes() {

			$archive_fields = array(
				array(
					'name' => esc_html__( 'Thumbnail for \'Black Overlay\' item style', 'tm-robin' ),
					'desc' => esc_html__( 'Use for \'Product Categories\' element of Visual Composer', 'tm-robin' ),
					'id'   => $this->prefix . 'product_cat_thumbnail_black_overlay',
					'type' => 'file',
				),
				array(
					'name' => esc_html__( 'Thumbnail for Masonry layout', 'tm-robin' ),
					'desc' => esc_html__( 'Use for \'Product Categories\' of Visual Composer', 'tm-robin' ),
					'id'   => $this->prefix . 'product_cat_thumbnail_masonry',
					'type' => 'file',
				),
			);

			$archive_transfer_options = array(
				'page_title_on',
				'page_title_style',
				'page_title_text_color',
				'page_title_bg_color',
				'page_title_overlay_color',
				'page_title_bg_image',
			);

			foreach ( $archive_transfer_options as $option ) {
				$archive_fields[] = $this->redux2metabox( $option );
			}

			new_cmb2_box( array(
				'id'           => $this->prefix . 'product_categories_meta_box',
				'title'        => esc_html__( 'Product Category Meta Box', 'tm-robin' ),
				'object_types' => array( 'term' ),
				'taxonomies'   => array( 'product_cat' ),
				'fields'       => $archive_fields,
			) );

			$this->transfer_options = array_merge( $this->transfer_options,
				$archive_transfer_options );

		}

		/**
		 * Convert function from redux to CMB2
		 *
		 * @param string $field   field slug in Redux options
		 * @param string $type    field type
		 * @param string $default default value
		 * @param array  $unset   unset options
		 *
		 * @return array  $cmb_field  CMB compatible field config array
		 */
		private function redux2metabox( $field, $type = '', $default = '', $unset = array() ) {

			if ( ! class_exists( 'Redux' ) ) {

				return array(
					'id'      => '',
					'type'    => '',
					'name'    => '',
					'desc'    => '',
					'options' => '',
					'std'     => 'default',
					'default' => 'default',
				);
			}

			$field = Redux::getField( TM_Robin_Redux::$opt_name, $field );

			$options = $settings = array();

			switch ( $field['type'] ) {

				case 'image_select':

					$type    = $type ? $type : 'select';
					$default = $default ? $default : 'default';

					$options = ( ! empty( $field['options'] ) ) ? array_merge( array(
						'default' => array(
							'title' => esc_html__( 'Default',
								'tm-robin' ),
						),
					),
						$field['options'] ) : array();
					foreach ( $options as $key => $option ) {
						$options[ $key ] = ( isset( $options[ $key ]['alt'] ) ) ? $options[ $key ]['alt'] : $options[ $key ]['title'];

						foreach ( $unset as $u ) {
							unset( $options[ $u ] );
						}
					}

					break;

				case 'button_set':

					$type    = $type ? $type : 'select';
					$default = $default ? $default : 'default';

					$options['default'] = esc_html__( 'Default', 'tm-robin' );
					foreach ( $field['options'] as $key => $value ) {
						$options[ $key ] = $value;

						foreach ( $unset as $u ) {
							unset( $options[ $u ] );
						}
					}

					break;

				case 'select':

					$type    = $type ? $type : 'select';
					$default = $default ? $default : 'default';

					$options['default'] = esc_html__( 'Default', 'tm-robin' );

					foreach ( $field['options'] as $key => $value ) {
						$options[ $key ] = $value;

						foreach ( $unset as $u ) {
							unset( $options[ $u ] );
						}
					}

					break;

				case 'switch':

					$type    = $type ? $type : 'select';
					$default = $default ? $default : 'default';

					$options['default'] = esc_html__( 'Default', 'tm-robin' );
					$options['on']      = esc_html__( 'On', 'tm-robin' );
					$options['off']     = esc_html__( 'Off', 'tm-robin' );

					break;

				case 'slider':

					$type = 'slider';

					$settings = array(
						'min'  => isset( $field['min'] ) ? $field['min'] : 0,
						'max'  => isset( $field['max'] ) ? $field['max'] : 100,
						'step' => isset( $field['step'] ) ? $field['step'] : 1,
					);

					$default = tm_robin_get_option( $field['id'] );

					break;

				case 'color':

					$type = 'colorpicker';

					if ( tm_robin_get_option( $field['id'] ) == 'transparent' ) {
						$default = '';
					} else {
						$default = tm_robin_get_option( $field['id'] );
					}

					break;

				case 'link_color':

					$type = 'colorpicker';

					if ( tm_robin_get_option( $field['id'] ) == 'transparent' ) {
						$default = '';
					} else {
						$default = tm_robin_get_option( $field['id'] );

						if ( is_array( $default ) && isset( $default['regular'] ) ) {
							$default = $default['regular'];
						} else {
							$default = '';
						}
					}

					break;

				case 'color_rgba':

					$type = 'rgba_colorpicker';
					$val  = tm_robin_get_option( $field['id'] );

					if ( isset( $val['rgba'] ) && $val['rgba'] ) {
						$default = $val['rgba'];
					}

					break;

				case 'media':

					$type = 'file';
					$val  = tm_robin_get_option( $field['id'] );

					if ( isset( $val['url'] ) && $val['url'] ) {
						$default = $val['url'];
					}

					break;

				case 'background':

					$type = 'file';

					if ( isset( $field['default']['background-image'] ) && $field['default']['background-image'] ) {
						$default = $field['default']['background-image'];
					}

					break;

				default:
					$type    = $type ? $type : $field['type'];
					$default = $default ? $default : tm_robin_get_option( $field['id'] );

					break;
			}

			$mb_field = array_merge( array(
				'id'      => $this->prefix . $field['id'],
				'type'    => $type,
				'name'    => $field['title'],
				'desc'    => isset( $field['subtitle'] ) ? $field['subtitle'] : '',
				'options' => $options,
				'default' => $default,
			),
				$settings );

			return $mb_field;
		}

		/**
		 * Modify global $tm_robin_options variables
		 */
		public function modify_global_settings() {

			global $tm_robin_options;

			if ( ! empty( $this->transfer_options ) ) {
				foreach ( $this->transfer_options as $field ) {
					$meta = get_post_meta( TM_Robin_Helper::get_the_ID(), $this->prefix . $field, true );

					if ( isset( $meta ) && $meta != '' && $meta != 'inherit' && $meta != 'default' ) {

						if ( $meta == 'on' ) {
							$meta = true;
						} elseif ( $meta == 'off' ) {
							$meta = false;
						}

					} else {
						if ( isset( $tm_robin_options[ $field ] ) ) {
							$meta = $tm_robin_options[ $field ];
						}
					}

					$tm_robin_options[ $field ] = $meta;
				}
			}
		}

	}

	new TM_Robin_Metabox();
}
