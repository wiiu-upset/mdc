<?php

/**
 * Instagram
 *
 * @package   InsightFramework
 */
if ( ! class_exists( 'TM_Robin_Instagram' ) ) {

	class TM_Robin_Instagram {

		const CLIENT_ID = 'd5d0868a929d453281a9f5dfb324d510';
		const CLINE_SECRET = 'ddc3dbc54b2e478a9e1feb54376cc49e';
		const ACCESS_TOKEN = '2102623126.d5d0868.e18524f9c81d49c3992f3ee0ce3e41ee';

		//    !!! IMPORTANT: DO NOT CHANGE THESE URL!!!!
		const INSTAGRAM_API_URL = 'https://api.instagram.com/v1';
		const FIND_USER_URL = self::INSTAGRAM_API_URL;

		public static function get_instance() {
			static $instance;
			$class = __CLASS__;

			if ( ! $instance instanceof $class ) {
				$instance = new $class;
			}

			return $instance;
		}

		public static function scrape_instagram( $username, $slice, $square = true ) {

			$username   = trim( strtolower( $username ) );
			$by_hashtag = substr( $username, 0, 1 ) == '#';

			if ( false === ( $instagram = get_transient( 'instagram-media-new-' . sanitize_title_with_dashes( $username . '-' . $square ) ) ) ) {

				$request_param = $by_hashtag ? 'explore/tags/' . substr( $username, 1 ) : $username;

				$remote = wp_remote_get( sprintf( 'http://instagram.com/%s', $request_param ) );

				if ( is_wp_error( $remote ) ) {
					return new WP_Error( 'site_down', esc_html__( 'Unable to communicate with Instagram.', 'tm-robin' ) );
				}

				if ( 200 != wp_remote_retrieve_response_code( $remote ) ) {
					return new WP_Error( 'invalid_response', esc_html__( 'Instagram did not return a 200.', 'tm-robin' ) );
				}

				$shards     = explode( 'window._sharedData = ', $remote['body'] );
				$insta_json = explode( ';</script>', $shards[1] );
				$insta_arr  = json_decode( $insta_json[0], true );

				if ( ! $insta_arr ) {
					return new WP_Error( 'bad_json', esc_html__( 'Instagram has returned invalid data.', 'tm-robin' ) );
				}

				// old style
				if ( isset( $insta_arr['entry_data']['UserProfile'][0]['userMedia'] ) ) {
					$media_arr = $insta_arr['entry_data']['UserProfile'][0]['userMedia'];
					$type      = 'old';
					// new style
				} elseif ( isset( $insta_arr['entry_data']['ProfilePage'][0]['user']['media']['nodes'] ) ) {
					$media_arr = $insta_arr['entry_data']['ProfilePage'][0]['user']['media']['nodes'];
					$type      = 'new';
					//} elseif ( $by_hashtag && isset( $insta_arr['entry_data']['TagPage'][0]['tag']['media']['nodes'] ) && count( $insta_arr['entry_data']['TagPage'][0]['tag']['media']['nodes'] ) ) {
					//	$media_arr = $insta_arr['entry_data']['TagPage'][0]['tag']['media']['nodes'];
					//	$type      = 'new';
				} elseif ( $by_hashtag && isset( $insta_arr['entry_data']['TagPage'][0]['tag']['top_posts']['nodes'] ) && count( $insta_arr['entry_data']['TagPage'][0]['tag']['top_posts']['nodes'] ) ) {
					$media_arr = $insta_arr['entry_data']['TagPage'][0]['tag']['top_posts']['nodes'];
					$type      = 'new';
				} else {
					return new WP_Error( 'bad_json_2', esc_html__( 'Instagram has returned invalid data.', 'tm-robin' ) );
				}

				if ( ! is_array( $media_arr ) ) {
					return new WP_Error( 'bad_array', esc_html__( 'Instagram has returned invalid data.', 'tm-robin' ) );
				}

				$instagram = array();

				switch ( $type ) {
					case 'old':
						foreach ( $media_arr as $media ) {

							if ( $media['user']['username'] == $username ) {

								$media['link']                          = preg_replace( "/^http:/i", "", $media['link'] );
								$media['images']['thumbnail']           = preg_replace( "/^http:/i", "", $media['images']['thumbnail'] );
								$media['images']['standard_resolution'] = preg_replace( "/^http:/i", "", $media['images']['standard_resolution'] );
								$media['images']['low_resolution']      = preg_replace( "/^http:/i", "", $media['images']['low_resolution'] );

								$instagram[] = array(
									'description' => $media['caption']['text'],
									'link'        => $media['link'],
									'time'        => $media['created_time'],
									'comments'    => self::roundNumber( $media['comments']['count'] ),
									'likes'       => self::roundNumber( $media['likes']['count'] ),
									'thumbnail'   => $media['images']['thumbnail']['url'],
									'large'       => $media['images']['standard_resolution']['url'],
									'small'       => $media['images']['low_resolution']['url'],
									'type'        => $media['type'],
								);
							}
						}
						break;
					default:
						foreach ( $media_arr as $media ) {

							$image_src = ( $square ) ? 'thumbnail_src' : 'display_src';

							$media[ $image_src ] = preg_replace( "/^http:/i", "", $media[ $image_src ] );

							if ( $media['is_video'] == true ) {
								$type = 'video';
							} else {
								$type = 'image';
							}


							$instagram[] = array(
								'description' => isset( $media['caption'] ) ? $media['caption'] : '',
								'link'        => '//instagram.com/p/' . $media['code'],
								'time'        => $media['date'],
								'comments'    => self::roundNumber( $media['comments']['count'] ),
								'likes'       => self::roundNumber( $media['likes']['count'] ),
								'thumbnail'   => $media[ $image_src ],
								'type'        => $type,
							);
						}
						break;
				}

				// do not set an empty transient - should help catch private or empty accounts
				if ( ! empty( $instagram ) ) {
					$instagram = insight_core_base_encode( serialize( $instagram ) );
					set_transient( 'instagram-media-new-' . sanitize_title_with_dashes( $username . '-' . $square ), $instagram, apply_filters( 'null_instagram_cache_time', HOUR_IN_SECONDS * 2 ) );
				}
			}

			if ( ! empty( $instagram ) ) {

				$instagram = unserialize( insight_core_base_decode( $instagram ) );

				return array_slice( $instagram, 0, $slice );

			} else {

				return new WP_Error( 'no_images', esc_html__( 'Instagram did not return any images.', 'tm-robin' ) );

			}
		}

		/**
		 * Generate rounded number
		 * Example: 11200 --> 11K
		 *
		 * @param $number
		 *
		 * @return string
		 */
		public static function roundNumber( $number ) {
			if ( $number > 999 && $number <= 999999 ) {
				$result = floor( $number / 1000 ) . ' K';
			} elseif ( $number > 999999 ) {
				$result = floor( $number / 1000000 ) . ' M';
			} else {
				$result = $number;
			}

			return $result;
		}

		/**
		 * Get media via OAuth 2 (use for future if scrape_instagram can't be used
		 * Alway return square media
		 *
		 * @param $username
		 * @param $number_items
		 *
		 * @return array|WP_Error
		 */
		public function oauth_instagram( $username, $number_items ) {

			$username = trim( strtolower( $username ) );

			if ( false === ( $instagram = get_transient( 'instagram-media-new-' . sanitize_title_with_dashes( $username ) ) ) ) {
				// find user by name
				$user_remote = wp_remote_get( sprintf( self::INSTAGRAM_API_URL . '/users/search/?q=%s&access_token=%s&count=1', $username, self::ACCESS_TOKEN ) );

				if ( true === $this->isValidRemote( $user_remote ) ) {

					if ( ! empty( $user_remote['body'] ) ) {
						$user_arr = json_decode( $user_remote['body'], true );

						if ( ! $user_arr ) {
							return new WP_Error( 'bad_json', esc_html__( 'Instagram has returned invalid data.', 'tm-robin' ) );
						}

						// get user ID.
						if ( isset( $user_arr['data'][0]['id'] ) ) {
							$userID = $user_arr['data'][0]['id'];

							// Get the most recent media published by a user.
							if ( ! empty( $userID ) ) {
								$insta_remote = wp_remote_get( sprintf( self::INSTAGRAM_API_URL . '/users/%s/media/recent/?access_token=%s&count=%s', $userID, self::ACCESS_TOKEN, $number_items ) );
								if ( true === $this->isValidRemote( $insta_remote ) ) {
									$insta_arr = json_decode( $insta_remote['body'], true );

									if ( ! $insta_arr ) {
										return new WP_Error( 'bad_json_2', esc_html__( 'Instagram has returned invalid data.', 'tm-robin' ) );
									}

									$media_arr = $insta_arr['data'];

									if ( ! is_array( $media_arr ) ) {
										return new WP_Error( 'bad_array', esc_html__( 'Instagram has returned invalid data.', 'tm-robin' ) );
									}

									$instagram = $this->get_media_old_method( $username, $media_arr );

									// do not set an empty transient - should help catch private or empty accounts
									if ( ! empty( $instagram ) ) {
										$instagram = insight_core_base_encode( serialize( $instagram ) );
										set_transient( 'instagram-media-new-' . sanitize_title_with_dashes( $username ), $instagram, apply_filters( 'null_instagram_cache_time', HOUR_IN_SECONDS * 2 ) );
									}
								}
							}
						} else {
							return new WP_Error( 'bad_json_4', esc_html__( 'Instagram has returned invalid data.', 'tm-robin' ) );
						}
					}
				}
			}

			if ( ! empty( $instagram ) ) {

				$instagram = unserialize( insight_core_base_encode( $instagram ) );

				return $instagram;

			} else {

				return new WP_Error( 'no_images', esc_html__( 'Instagram did not return any images.', 'tm-robin' ) );

			}
		}

		/**
		 * Check Remote
		 *
		 * @param $remote
		 *
		 * @return bool|WP_Error
		 */
		public function isValidRemote( $remote ) {
			if ( is_wp_error( $remote ) ) {
				return new WP_Error( 'site_down', esc_html__( 'Unable to communicate with Instagram.', 'tm-robin' ) );
			}

			if ( 200 != wp_remote_retrieve_response_code( $remote ) ) {
				return new WP_Error( 'invalid_response', esc_html__( 'Instagram did not return a 200.', 'tm-robin' ) );
			}

			return true;
		}
	}
}
