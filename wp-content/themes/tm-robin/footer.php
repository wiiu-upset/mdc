<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Robin
 */

?>
<?php

do_action( 'tm_robin_main_container_bottom' );

$pageID         = TM_Robin_Helper::get_the_ID();
$disable_footer = get_post_meta( $pageID, 'tm_robin_disable_footer', true );

$footer_width = get_post_meta( $pageID, 'tm_robin_footer_width', true );

$footer_color_scheme = get_post_meta( $pageID, 'tm_robin_footer_color_scheme', true );

$footer_sidebar_1 = get_post_meta( $pageID, 'tm_robin_footer_custom_sidebar_1', true );
$footer_sidebar_2 = get_post_meta( $pageID, 'tm_robin_footer_custom_sidebar_2', true );
$footer_sidebar_3 = get_post_meta( $pageID, 'tm_robin_footer_custom_sidebar_3', true );
$footer_sidebar_4 = get_post_meta( $pageID, 'tm_robin_footer_custom_sidebar_4', true );

if ( ! $footer_sidebar_1 ) {
	$footer_sidebar_1 = 'sidebar-footer-1';
}

if ( ! $footer_sidebar_2 ) {
	$footer_sidebar_2 = 'sidebar-footer-2';
}

if ( ! $footer_sidebar_3 ) {
	$footer_sidebar_3 = 'sidebar-footer-3';
}

if ( ! $footer_sidebar_4 ) {
	$footer_sidebar_4 = 'sidebar-footer-4';
}

$footer_sidebars = array(
	$footer_sidebar_1,
	$footer_sidebar_2,
	$footer_sidebar_3,
	$footer_sidebar_4,
);

if ( $footer_width == 'default' || ! $footer_width ) {
	$footer_width = tm_robin_get_option( 'footer_width' );
};

if ( $footer_color_scheme == 'default' || ! $footer_color_scheme ) {
	$footer_color_scheme = tm_robin_get_option( 'footer_color_scheme' );
};

$layout1 = get_post_meta( TM_Robin_Helper::get_the_ID(), 'tm_robin_footer_layout', true );
$layout  = explode( '_', get_post_meta( TM_Robin_Helper::get_the_ID(), 'tm_robin_footer_layout', true ) );
if ( $layout1 == 'default' || ! $layout1 ) {
	$layout = explode( '_', tm_robin_get_option( 'footer_layout' ) );
}

?>

</div>

<?php if ( ! $disable_footer ) : ?>

	<div id="footer" class="site-footer site-footer--<?php echo esc_attr( $footer_color_scheme ); ?> ">
		<div class="container<?php echo ( $footer_width == 'wide' ) ? ' wide' : ''; ?>">
			<div class="row">
				<?php

				$columns = $layout[0];
				$col_xl  = $layout[1];
				$col_lg  = $columns > 1 ? 6 : 12;

				?>

				<?php for ( $i = 0; $i < $columns; $i ++ ) : ?>
					<div
						class="col-xs-12 col-lg-<?php echo esc_attr( $col_lg ); ?> col-xl-<?php echo esc_attr( $col_xl ); ?>">
						<?php if ( is_active_sidebar( $footer_sidebars[ $i ] ) ) : ?>
							<?php dynamic_sidebar( $footer_sidebars[ $i ] ); ?>
						<?php endif; ?>
					</div>
				<?php endfor; ?>

			</div>
		</div>

		<?php if ( tm_robin_get_option( 'footer_copyright' ) ): ?>
			<div class="site-copyright">
				<div class="container<?php echo ( $footer_width == 'wide' ) ? ' wide' : ''; ?>">
					<div class="row flex-items-xs-middle">
						<div class="col-xs-12 text-xs-center">
							<?php echo wp_kses_post( tm_robin_get_option( 'footer_copyright_text' ) ); ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>

	</div>
<?php endif; ?>

<?php do_action( 'tm_robin_site_bottom' ); ?>

<?php echo TM_Robin_Templates::go_to_top() ?>
<?php echo TM_Robin_Templates::cookie_notice() ?>

</div>

<?php do_action( 'tm_robin_after_page_container' ); ?>
<?php wp_footer(); ?>

</body>

</html>
