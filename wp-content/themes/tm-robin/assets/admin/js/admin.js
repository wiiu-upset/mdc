'use strict'

var tmRobinAdmin;

(function( $ ) {

	tmRobinAdmin = (
		function() {

			var // Top bar
				$topBarOn                 = $( '#tm_robin_topbar_on' ),
				$topBarLayout             = $( '#tm_robin_topbar_layout' ),
				$topBarLayoutRow          = $( '.cmb-row.cmb2-id-tm-robin-topbar-layout' ),
				$topbarText               = $( '.cmb-row.cmb2-id-tm-robin-topbar-text' ),
				$topbarWidth              = $( '.cmb-row.cmb2-id-tm-robin-topbar-width' ),
				$topbarSocial             = $( '.cmb-row.cmb2-id-tm-robin-topbar-social' ),
				$topbarMenu               = $( '.cmb-row.cmb2-id-tm-robin-topbar-menu' ),
				$topbarLanguageSwitcher   = $( '.cmb-row.cmb2-id-tm-robin-topbar-language-switcher-on' ),
				$topbarCurrencySwitcher   = $( '.cmb-row.cmb2-id-tm-robin-topbar-currency-switcher-on' ),
				$topbarColor              = $( '.cmb-row.cmb2-id-tm-robin-topbar-color' ),
				$topbarBgColor            = $( '.cmb-row.cmb2-id-tm-robin-topbar-bgcolor' ),
				$topbarBdColor            = $( '.cmb-row.cmb2-id-tm-robin-topbar-bdcolor' ),
				topBarOnVal               = $topBarOn.val(),
				topBarLayout              = $topBarLayout.val(),

				// logo
				$customLogo               = $( '#tm_robin_custom_logo' ),
				customLogo                = $customLogo.val(),
				$logo                     = $( '.cmb-row.cmb2-id-tm-robin-logo' ),
				$logoAlt                  = $( '.cmb-row.cmb2-id-tm-robin-logo-alt' ),
				$logoMobile               = $( '.cmb-row.cmb2-id-tm-robin-logo-mobile' ),

				// Header
				$headerType               = $( '#tm_robin_header' ),
				$headerSocial             = $( '.cmb-row.cmb2-id-tm-robin-header-social' ),
				$headerOverlap            = $( '.cmb-row.cmb2-id-tm-robin-header-overlap' ),
				$headerMenuBg             = $( '.cmb-row.cmb2-id-tm-robin-site-menu-bgcolor' ),
				$headerLogoWidth          = $( '.cmb-row.cmb2-id-tm-robin-logo-width' ),
				$headerRightColumnWidth   = $( '.cmb-row.cmb2-id-tm-robin-right-column-width' ),
				$headerLeftSidebar        = $( '.cmb-row.cmb2-id-tm-robin-header-left-sidebar' ),
				$headerRightSidebar       = $( '.cmb-row.cmb2-id-tm-robin-header-right-sidebar' ),
				headerTypeVal             = $( '#tm_robin_header' ).val(),

				// Page Title
				$pageTitleOn              = $( '#tm_robin_page_title_on' ),
				$pageTitleStyle           = $( '#tm_robin_page_title_style' ),
				$removeWhiteSpaceRow      = $( '.cmb-row.cmb2-id-tm-robin-remove-whitespace' ),
				$pageTitleStyleRow        = $( '.cmb-row.cmb2-id-tm-robin-page-title-style' ),
				$pageTitleTextColorRow    = $( '.cmb-row.cmb2-id-tm-robin-page-title-text-color' ),
				$pageSubTitleColorRow     = $( '.cmb-row.cmb2-id-tm-robin-page-subtitle-color' ),
				$pageTitleBgColorRow      = $( '.cmb-row.cmb2-id-tm-robin-page-title-bg-color' ),
				$pageTitleOverlayColorRow = $( '.cmb-row.cmb2-id-tm-robin-page-title-overlay-color' ),
				$pageTitleBgImageRow      = $( '.cmb-row.cmb2-id-tm-robin-page-title-bg-image' ),
				pageTitleOnVal            = $pageTitleOn.val(),
				pageTitleStyleVal         = $pageTitleStyle.val(),

				// Breadcrumbs
				$breadCrumbsOn            = $( '#tm_robin_breadcrumbs' ),
				$breadCrumbsPosition      = $( '.cmb-row.cmb2-id-tm-robin-breadcrumbs-position' ),
				breadCrumbsOnVal          = $( '#tm_robin_breadcrumbs' ).val(),

				// Sidebar
				$pageSideBarPosition      = $( '#tm_robin_page_sidebar_config' ),
				$customSideBar            = $( '.cmb-row.cmb2-id-tm-robin-page-custom-sidebar' ),
				pageSideBarPositionVal    = $( '#tm_robin_page_sidebar_config' ).val(),

				// Footer
				$footerColorScheme        = $( '#tm_robin_footer_color_scheme' ),
				$footerBackgroundColor    = $( '.cmb-row.cmb2-id-tm-robin-footer-bgcolor' ),
				$footerTextColor          = $( '.cmb-row.cmb2-id-tm-robin-footer-color' ),
				$footerAccentColor        = $( '.cmb-row.cmb2-id-tm-robin-footer-accent-color' ),
				$copyrightBackgroundColor = $( '.cmb-row.cmb2-id-tm-robin-footer-copyright-bgcolor' ),
				$copyrightColor           = $( '.cmb-row.cmb2-id-tm-robin-footer-copyright-color' ),
				$copyrightLinkColor       = $( '.cmb-row.cmb2-id-tm-robin-footer-copyright-link-color' ),
				$copyrightLinkColorHover  = $( '.cmb-row.cmb2-id-tm-robin-footer-copyright-link-color-hover' ),
				footerColorSchemeVal      = $footerColorScheme.val();

			return {

				init: function() {

					this.toggleTopbarSettings( topBarOnVal == 'off' );

					this.toggleTopbarLayout( topBarLayout );

					this.toggleLogoSettings( customLogo == 'on' );

					this.toggleHeaderType( headerTypeVal );

					this.togglePageTitleStyle( pageTitleStyleVal );

					this.togglePageTitleSettings( pageTitleOnVal == 'off' );

					this.toggleBreadCrumbsSettings( breadCrumbsOnVal == 'off' );

					this.toggleSidebarPosition( pageSideBarPositionVal );

					this.toggleFooterColorSchemeSettings( footerColorSchemeVal == 'custom' );

					this.events();
				},

				events: function() {

					$topBarOn.on( 'change', function() {
						tmRobinAdmin.toggleTopbarSettings( $( this ).val() == 'off' );
					} );

					$topBarLayout.on( 'change', function() {
						tmRobinAdmin.toggleTopbarLayout( $( this ).val() );
					} );

					$customLogo.on( 'change', function() {
						tmRobinAdmin.toggleLogoSettings( $( this ).val() == 'on' );
					} );

					$headerType.on( 'change', function() {
						tmRobinAdmin.toggleHeaderType( $( this ).val() );
					} );

					$pageTitleOn.on( 'change', function() {
						var val = $( this ).val();
						tmRobinAdmin.togglePageTitleSettings( val == 'off' );
					} );

					$pageTitleStyle.on( 'change', function() {
						tmRobinAdmin.togglePageTitleStyle( $( this ).val() );
					} );

					$breadCrumbsOn.on( 'change', function() {
						tmRobinAdmin.toggleBreadCrumbsSettings( $breadCrumbsOn.val() == 'off' );
					} );

					$pageSideBarPosition.on( 'change', function() {
						tmRobinAdmin.toggleSidebarPosition( $pageSideBarPosition.val() );
					} );

					$footerColorScheme.on( 'change', function() {
						tmRobinAdmin.toggleFooterColorSchemeSettings( $footerColorScheme.val() == 'custom' );
					} );
				},

				toggleTopbarSettings: function( off ) {

					var val = $topBarLayout.val();

					if ( off ) {

						$topBarLayoutRow.slideUp();
						$topbarText.slideUp();
						$topbarWidth.slideUp();
						$topbarSocial.slideUp();
						$topbarMenu.slideUp();
						$topbarLanguageSwitcher.slideUp();
						$topbarCurrencySwitcher.slideUp();
						$topbarColor.slideUp();
						$topbarBgColor.slideUp();
						$topbarBdColor.slideUp();

					} else {

						$topBarLayoutRow.slideDown();
						$topbarText.slideDown();
						$topbarWidth.slideDown();
						$topbarColor.slideDown();
						$topbarBgColor.slideDown();
						$topbarBdColor.slideDown();

						if ( val != 'default' ) {
							$topbarSocial.slideDown();
							$topbarMenu.slideDown();
							$topbarLanguageSwitcher.slideDown();
							$topbarCurrencySwitcher.slideDown();
						}
					}
				},

				toggleTopbarLayout: function( layout ) {

					if ( layout == '2' ) {
						$topbarSocial.slideDown();
						$topbarMenu.slideDown();
						$topbarLanguageSwitcher.slideDown();
						$topbarCurrencySwitcher.slideDown();
					} else {
						$topbarSocial.slideUp();
						$topbarMenu.slideUp();
						$topbarLanguageSwitcher.slideUp();
						$topbarCurrencySwitcher.slideUp();
					}
				},

				toggleLogoSettings: function( on ) {
					if ( on ) {
						$logo.slideDown();
						$logoAlt.slideDown();
						$logoMobile.slideDown();
					} else {
						$logo.slideUp();
						$logoAlt.slideUp();
						$logoMobile.slideUp();
					}
				},

				toggleHeaderType: function( type ) {

					if ( type == 'default' ) {
						$headerSocial.slideUp();
						$headerOverlap.slideUp();
						$headerMenuBg.slideUp();
						$headerLeftSidebar.slideUp();
						$headerRightSidebar.slideUp();
						$headerLogoWidth.slideUp();
						$headerRightColumnWidth.slideUp();
					} else if ( type == 'menu-bottom' || type == 'menu-bottom-wide' ) {
						$headerSocial.slideDown();
						$headerOverlap.slideUp();
						$headerMenuBg.slideDown();
						$headerLeftSidebar.slideDown();
						$headerRightSidebar.slideDown();
						$headerLogoWidth.slideUp();
						$headerRightColumnWidth.slideUp();
					} else {
						$headerSocial.slideUp();
						$headerOverlap.slideDown();
						$headerMenuBg.slideUp();
						$headerLeftSidebar.slideUp();
						$headerLogoWidth.slideDown();
						$headerRightColumnWidth.slideDown();

						if ( type == 'wide' ) {
							$headerRightSidebar.slideDown();
						} else {
							$headerLeftSidebar.slideUp();
							$headerRightSidebar.slideUp();
						}
					}
				},

				togglePageTitleSettings: function( off ) {

					var pageTitleStyle = $pageTitleStyle.val();

					if ( off ) {
						$removeWhiteSpaceRow.slideDown();
						$pageTitleStyleRow.slideUp();
						$pageTitleTextColorRow.slideUp();
						$pageSubTitleColorRow.slideUp();
						$pageTitleBgColorRow.slideUp();
						$pageTitleOverlayColorRow.slideUp();
						$pageTitleBgImageRow.slideUp();
					} else {
						$removeWhiteSpaceRow.slideUp();
						$pageTitleStyleRow.slideDown();
						tmRobinAdmin.togglePageTitleStyle( pageTitleStyle );
					}
				},

				togglePageTitleStyle: function( style ) {

					if ( style == 'bg_color' ) {
						$pageTitleTextColorRow.slideDown();
						$pageSubTitleColorRow.slideDown();
						$pageTitleBgColorRow.slideDown();
						$pageTitleOverlayColorRow.slideUp();
						$pageTitleBgImageRow.slideUp();
					} else if ( style == 'bg_image' ) {
						$pageTitleTextColorRow.slideDown();
						$pageSubTitleColorRow.slideDown();
						$pageTitleBgColorRow.slideUp();
						$pageTitleOverlayColorRow.slideDown();
						$pageTitleBgImageRow.slideDown();
					} else {
						$pageTitleTextColorRow.slideUp();
						$pageSubTitleColorRow.slideUp();
						$pageTitleBgColorRow.slideUp();
						$pageTitleOverlayColorRow.slideUp();
						$pageTitleBgImageRow.slideUp();
					}
				},

				toggleBreadCrumbsSettings: function( off ) {

					if ( off ) {
						$breadCrumbsPosition.slideUp();
					} else {
						$breadCrumbsPosition.slideDown();
					}
				},

				toggleSidebarPosition: function( position ) {

					if ( position === 'default' || position === 'no' ) {
						$customSideBar.slideUp();
					} else {
						$customSideBar.slideDown();
					}
				},

				toggleFooterColorSchemeSettings: function( show ) {

					if ( show ) {
						$footerBackgroundColor.slideDown();
						$footerTextColor.slideDown();
						$footerAccentColor.slideDown();
						$copyrightBackgroundColor.slideDown();
						$copyrightColor.slideDown();
						$copyrightLinkColor.slideDown();
						$copyrightLinkColorHover.slideDown();
					} else {
						$footerBackgroundColor.slideUp();
						$footerTextColor.slideUp();
						$footerAccentColor.slideUp();
						$copyrightBackgroundColor.slideUp();
						$copyrightColor.slideUp();
						$copyrightLinkColor.slideUp();
						$copyrightLinkColorHover.slideUp();
					}
				},
			};
		}()
	);

})( jQuery );

jQuery( document ).ready( function() {
	tmRobinAdmin.init();
} );

var tmRobinProductAttributeFilterDependencyCallback = function() {
	(function( $, that ) {
		var $filterDropdown,
			$empty;

		$filterDropdown = $( '[data-vc-shortcode-param-name="filter"]', that.$content );
		$filterDropdown.removeClass( 'vc_dependent-hidden' );
		$empty = $( '#filter-empty', $filterDropdown );

		var $settings = JSON.parse( $filterDropdown.attr( 'data-param_settings' ) );

		if ( $empty.length ) {
			$empty.parent().remove();
			$( '.edit_form_line', $filterDropdown )
				.prepend( $( '<div class="vc_checkbox-label"><span>No values found</span></div>' ) );
		}

		$( 'select[name="attribute"]', that.$content ).change( function() {
			$( '.vc_checkbox-label', $filterDropdown ).remove();
			$filterDropdown.removeClass( 'vc_dependent-hidden' );

			$.ajax( {
				type    : 'POST',
				dataType: 'json',
				url     : window.ajaxurl,
				data    : {
					action   : 'vc_woocommerce_get_attribute_terms',
					attribute: this.value,
					_vcnonce : window.vcAdminNonce
				}
			} ).done( function( data ) {
				if ( 0 < data.length ) {
					$( '.edit_form_line', $filterDropdown ).prepend( $( data ) );
				} else {
					$( '.edit_form_line', $filterDropdown )
						.prepend( $( '<div class="vc_checkbox-label"><span>No values found</span></div>' ) );
				}
			} );
		} );

		if ( typeof $settings.dependency !== 'undefined' ) {

			var $select = $( 'select[name="' + $settings.dependency.element + '"]' );

			if ( $settings.dependency.value.indexOf( $select.val() ) < 0 ) {
				$filterDropdown.hide();
			} else {
				$filterDropdown.show();
			}

			$select.on( 'change', function() {

				if ( $settings.dependency.value.indexOf( $( this ).val() ) < 0 ) {
					$filterDropdown.hide();
				} else {
					$filterDropdown.show();
				}
			} );
		}

	}( window.jQuery, this ));
};
