<?php
/**
 * Product loop sale flash
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/sale-flash.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see           https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       1.6.4
 */

if ( ! defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

global $post, $product;

echo '<div class="product-badges">';

if ( ! $product->is_in_stock()) {
	echo '<span class="outofstock">' . esc_html__('Out of stock', 'tm-robin') . '</span>';
}

// New
$postdate = get_the_time('Y-m-d', $post->ID);
$timestamp = strtotime($postdate);
$newdays = apply_filters('tm_robin_shop_new_days', tm_robin_get_option('shop_new_days'));

if ((time() - $timestamp < 60 * 60 * 24 * $newdays) && tm_robin_get_option('shop_new_badge_on')) {
	echo '<span class="new">' . esc_html__('New', 'tm-robin') . '</span>';
}

// Sale
if ($product->is_on_sale() && tm_robin_get_option('shop_sale_badge_on')) {

	$str = esc_html__('Sale', 'tm-robin');

	if (tm_robin_get_option('shop_sale_percent_on') && ($product->is_type('simple') || $product->is_type('external'))) {
		$str = TM_Robin_Woo::get_sale_percentage($product);
	}

	echo apply_filters('woocommerce_sale_flash', '<span class="onsale">' . $str . '</span>', $post, $product);
}

// Featured (Hot)
if ($product->is_featured() && tm_robin_get_option('shop_hot_badge_on')) {
	echo '<span class="hot">' . esc_html__('Hot', 'tm-robin') . '</span>';
}

echo '</div>';
