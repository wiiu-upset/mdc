<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see           https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' );

// Sidebar config
$page_wrap_class = $content_class = '';
$sidebar_config  = tm_robin_get_option( 'shop_sidebar_config' );
$full_width_shop = tm_robin_get_option( 'full_width_shop' );
$column_switcher = tm_robin_get_option( 'column_switcher' );
$breadcrumbs_on  = tm_robin_get_option( 'breadcrumbs' );

if ( $sidebar_config == 'left' ) {
	$page_wrap_class = 'has-sidebar-left row';
	$content_class   = ( $full_width_shop ) ? 'col-xs-12 col-md-8 col-lg-10' : 'col-xs-12 col-md-8 col-lg-9';
} elseif ( $sidebar_config == 'right' ) {
	$page_wrap_class = 'has-sidebar-right row';
	$content_class   = ( $full_width_shop ) ? 'col-xs-12 col-md-8 col-lg-10' : 'col-xs-12 col-md-8 col-lg-9';
} else {
	$page_wrap_class = 'has-no-sidebars row';
	$content_class   = 'col-xs-12';
}

$sidebar = TM_Robin_Helper::get_active_sidebar( true );

if ( ! $sidebar ) {
	$page_wrap_class = 'has-no-sidebars row';
	$content_class   = 'col-xs-12';
}

?>

<div class="container<?php echo $full_width_shop ? ' wide' : ''; ?>">
	<div class="inner-page-wrap <?php echo esc_attr( $page_wrap_class ); ?>">
		<div id="main" class="site-content <?php echo esc_attr( $content_class ); ?>" role="main">
			<?php
			/**
			 * woocommerce_before_main_content hook.
			 *
			 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
			 * @hooked woocommerce_breadcrumb - 20
			 * @hooked WC_Structured_Data::generate_website_data() - 30
			 */
			do_action( 'woocommerce_before_main_content' );
			?>

			<?php
			/**
			 * woocommerce_archive_description hook.
			 *
			 * @hooked woocommerce_taxonomy_archive_description - 10
			 * @hooked woocommerce_product_archive_description - 10
			 */
			do_action( 'woocommerce_archive_description' );
			?>

			<?php if ( have_posts() ) :
				/**
				 * Hook: woocommerce_before_shop_loop.
				 *
				 * @hooked wc_print_notices - 10
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );

				?>
				<div class="shop-loop-head row">
					<div class="shop-display col-xl-7 col-lg-6">
						<?php

						if ( $breadcrumbs_on ) {
							?>
							<div class="site-breadcrumbs">
								<?php echo TM_Robin_Templates::breadcrumbs(); ?>
							</div>
							<?php
						}

						woocommerce_result_count();

						?>

						<?php if ( woocommerce_products_will_display() && tm_robin_get_option( 'products_hover' ) == 'base' ) { ?>
							<div class="switch-view">
								<span id="switch-view-grid" class="switcher grid hint--top hint--bounce" rel="grid"
								      aria-label="<?php esc_html_e( 'Grid', 'tm-robin' ); ?>">
									<i class="fa fa-th"></i>
								</span>
								<span id="switch-view-list" class="switcher list hint--top hint--bounce" rel="list"
								      aria-label="<?php esc_html_e( 'List', 'tm-robin' ); ?>">
									<i class="fa fa-list"></i>
								</span>
							</div>
						<?php } ?>

					</div>
					<div class="shop-filter col-xl-5 col-lg-6">
						<?php if ( $column_switcher && TM_Robin_Woo::get_shop_view_mode() == 'grid' ) {

							$columns = apply_filters( 'tm_robin_shop_products_columns',
								array(
									'xs' => 2,
									'sm' => 2,
									'md' => 3,
									'lg' => 3,
									'xl' => get_option( 'woocommerce_catalog_columns', 3 ),
								) );

							?>
							<div class="col-switcher hint--top hint-bounce"
							     aria-label="<?php esc_html_e( 'View Options', 'tm-robin' ); ?>"
							     data-cols="<?php echo esc_attr( json_encode( $columns ) ); ?>">
								<div class="wrapper">
									<i class="fa fa-eye"></i>
									<a href="#" data-col="1">1</a>
									<a href="#" data-col="2">2</a>
									<a href="#" data-col="3">3</a>
									<a href="#" data-col="4">4</a>
									<a href="#" data-col="5">5</a>
									<a href="#" data-col="6">6</a>
									<span><?php esc_html_e( 'columns', 'tm-robin' ); ?></span>
								</div>
							</div>
						<?php } ?>
						<?php woocommerce_catalog_ordering(); ?>
					</div>
				</div>

				<?php

				$layout        = tm_robin_get_option( 'categories_layout' );
				$data_carousel = ' data-carousel="' . tm_robin_get_option( 'categories_columns' ) . '"';

				$categories_row = '<div class="categories row';

				if ( $layout == 'carousel' ) {
					$categories_row .= ' categories-carousel"';
					$categories_row .= $data_carousel . '>';
				} else {
					$categories_row .= '">';
				}

				$categories_row .= '</div>';

				echo wp_kses( $categories_row,
					array(
						'div' => array(
							'class'         => array(),
							'data-carousel' => array(),
						),
					) );

				woocommerce_product_loop_start();

				if ( wc_get_loop_prop( 'total' ) ) {
					while ( have_posts() ) {
						the_post();

						/**
						 * Hook: woocommerce_shop_loop.
						 *
						 * @hooked WC_Structured_Data::generate_product_data() - 10
						 */
						do_action( 'woocommerce_shop_loop' );

						wc_get_template_part( 'content', 'product' );
					}
				}

				woocommerce_product_loop_end();

				/**
				 * Hook: woocommerce_after_shop_loop.
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
				?>

			<?php else: ?>

				<?php
				/**
				 * woocommerce_no_products_found hook.
				 *
				 * @hooked wc_no_products_found - 10
				 */
				do_action( 'woocommerce_no_products_found' );
				?>

			<?php endif; ?>

			<?php
			/**
			 * woocommerce_after_main_content hook.
			 *
			 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
			 */
			do_action( 'woocommerce_after_main_content' );
			?>
		</div>
		<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
		?>
	</div>
</div>

<?php get_footer( 'shop' ); ?>
