<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

$classes = array(
	'product',
	'product-loop',
);

$classes[] = TM_Robin_Helper::get_grid_item_class( apply_filters( 'tm_robin_shop_products_columns', array(
	'xs' => 2,
	'sm' => 2,
	'md' => 3,
	'lg' => 3,
	'xl' => get_option( 'woocommerce_catalog_columns', 3 ),
) ) );

$products_hover = apply_filters( 'tm_robin_products_hover', tm_robin_get_option( 'products_hover' ) );
$classes[]      = 'product-style-hover--' . $products_hover;

$other_classes = apply_filters( 'tm_robin_shop_products_classes', '' );
$classes[]     = $other_classes;

$shop_view_mode = TM_Robin_Woo::get_shop_view_mode();

$buttons_class   = array( 'product-buttons--' . apply_filters( 'tm_robin_product_buttons_scheme', tm_robin_get_option( 'product_buttons_scheme' ) ) );
$buttons_class[] = wp_is_mobile() ? 'mobile' : '';

?>
<div <?php post_class( $classes ); ?>>
	<?php woocommerce_show_product_loop_sale_flash(); ?>
	<div class="product-thumb">
		<?php

		TM_Robin_Woo::wishlist_button();

		/**
		 * woocommerce_before_shop_loop_item_title hook.
		 *
		 * @hooked woocommerce_template_loop_product_link_open - 5
		 * @hooked woocommerce_template_loop_product_thumbnail - 10
		 * @hooked woocommerce_template_loop_product_link_close - 15
		 */
		do_action( 'woocommerce_before_shop_loop_item_title' );
		?>
		<div
			class="product-buttons product-buttons--<?php echo esc_attr( tm_robin_get_option( 'product_buttons_scheme' ) ); ?><?php echo wp_is_mobile() ? ' mobile' : ''; ?>">

			<?php
			TM_Robin_Woo::quick_view_button();
			woocommerce_template_loop_add_to_cart();
			TM_Robin_Woo::compare_button();
			?>

		</div>
		<?php if ( $products_hover == 'info' ) { ?>
			<div class="product-info">

				<?php

				/**
				 * woocommerce_shop_loop_item_title hook.
				 *
				 * @hooked woocommerce_template_loop_product_title - 10
				 */
				do_action( 'woocommerce_shop_loop_item_title' );

				/**
				 * woocommerce_after_shop_loop_item_title hook.
				 *
				 * @hooked woocommerce_template_loop_rating - 5
				 * @hooked woocommerce_template_loop_price - 10
				 */
				do_action( 'woocommerce_after_shop_loop_item_title' );

				?>
				<div
					class="product-buttons product-buttons--<?php echo esc_attr( tm_robin_get_option( 'product_buttons_scheme' ) ); ?><?php echo wp_is_mobile() ? ' mobile' : ''; ?>">

					<?php
					TM_Robin_Woo::quick_view_button();
					woocommerce_template_loop_add_to_cart();
					TM_Robin_Woo::compare_button();
					?>

				</div>
			</div>
		<?php } ?>
	</div>

	<?php if ( $products_hover == 'base' ) { ?>
		<div class="product-info">

			<?php

			/**
			 * woocommerce_shop_loop_item_title hook.
			 *
			 * @hooked woocommerce_template_loop_product_title - 10
			 */
			do_action( 'woocommerce_shop_loop_item_title' );

			/**
			 * woocommerce_after_shop_loop_item_title hook.
			 *
			 * @hooked woocommerce_template_loop_rating - 5
			 * @hooked woocommerce_template_loop_price - 10
			 */
			do_action( 'woocommerce_after_shop_loop_item_title' );

			TM_Robin_Woo::wishlist_button();
			?>
			<div
				class="product-buttons product-buttons--<?php echo esc_attr( tm_robin_get_option( 'product_buttons_scheme' ) ); ?><?php echo wp_is_mobile() ? ' mobile' : ''; ?>">

				<?php
				TM_Robin_Woo::quick_view_button();
				woocommerce_template_loop_add_to_cart();
				TM_Robin_Woo::compare_button();
				?>

			</div>
		</div>
	<?php } ?>
</div>
