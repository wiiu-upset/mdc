<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see           https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post, $product;

$attachment_count  = count( $product->get_gallery_image_ids() );
$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
$full_size_image   = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
$placeholder       = has_post_thumbnail() ? 'with-images' : 'without-images';
$wrapper_classes   = apply_filters( 'woocommerce_single_product_image_gallery_classes',
	array(
		'row',
		'woocommerce-product-gallery',
		'woocommerce-product-gallery--' . $placeholder,
		'woocommerce-product-gallery--columns-' . absint( $columns ),
		'images',
	) );

// Thumbnail Position
$thumbnails_position = get_post_meta( TM_Robin_Helper::get_the_ID(), 'tm_robin_product_thumbnails_position', true );

if ( $thumbnails_position == 'default' || ! $thumbnails_position ) {
	$thumbnails_position = tm_robin_get_option( 'product_thumbnails_position' );
}

// Product page layout
$product_page_layout = get_post_meta( TM_Robin_Helper::get_the_ID(), 'tm_robin_product_page_layout', true );

if ( $product_page_layout == 'default' || ! $product_page_layout ) {
	$product_page_layout = tm_robin_get_option( 'product_page_layout' );
}

if ( $product_page_layout == 'sticky' || $product_page_layout == 'sticky-fullwidth' ) {
	$thumbnails_position = 'bottom';
}

$wrapper_classes[] = 'thumbnails-' . $thumbnails_position;
$attachment_ids    = $product->get_gallery_image_ids();

?>
<div class="<?php echo esc_attr( implode( ' ', array_map( 'sanitize_html_class', $wrapper_classes ) ) ); ?>">
	<div class="col-xs-12<?php if ( $thumbnails_position == 'left' && $attachment_ids ) : ?> col-md-9<?php endif; ?>">
		<?php
		// Sale flash
		woocommerce_show_product_sale_flash();

		$attributes = array(
			'title'                   => get_post_field( 'post_title', $post_thumbnail_id ),
			'data-caption'            => get_post_field( 'post_excerpt', $post_thumbnail_id ),
			'data-src'                => $full_size_image[0],
			'data-large_image'        => $full_size_image[0],
			'data-large_image_width'  => $full_size_image[1],
			'data-large_image_height' => $full_size_image[2],
		);

		if ( has_post_thumbnail() ) {

			$size              = 'shop_single';
			$lightbox_btn_html = '';

			// light box button
			if ( tm_robin_get_option( 'product_zoom_on' ) ) {
				$lightbox_btn_html = '<a href="#" class="hint--left hint--bounce lightbox-btn" aria-label="' . esc_html__( 'Open Lightbox',
						'tm-robin' ) . '">' . esc_html__( 'Open Lightbox',
						'tm-robin' ) . '<i class="pe-7s-expand1"></i></a>';
			}

			if ( $product_page_layout == 'fullwidth' ) {
				$size = array(
					intval( get_option( 'shop_single_image_size', array( 'width' => 675 ) )['width'] ) * 1.25,
					intval( get_option( 'shop_single_image_size', array( 'height' => 910 ) )['height'] ) * 1.25,
				);
			}

			$html = '<div data-thumb="' . get_the_post_thumbnail_url( $post->ID,
					'shop_thumbnail' ) . '" class="woocommerce-product-gallery__image"><a href="' . esc_url( $full_size_image[0] ) . '" class="woocommerce-main-image image-link" data-width="' . $full_size_image[1] . '" data-height="' . $full_size_image[2] . '">';
			$html .= get_the_post_thumbnail( $post->ID,
				apply_filters( 'single_product_large_thumbnail_size', $size ),
				$attributes );
			$html .= '</a>';
			$html .= $lightbox_btn_html;
			$html .= '</div>';
		} else {
			$html = '<div class="woocommerce-product-gallery__image--placeholder">';
			$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />',
				esc_url( wc_placeholder_img_src() ),
				esc_html__( 'Awaiting product image', 'tm-robin' ) );
			//$html .= $lightbox_btn_html;
			$html .= '</div>';
		}

		echo apply_filters( 'woocommerce_single_product_image_thumbnail_html',
			$html,
			get_post_thumbnail_id( $post->ID ) );
		?>
	</div>
	<div
		class="col-xs-12<?php if ( $thumbnails_position == 'left' && $attachment_ids ) : ?> col-md-3 flex-md-first<?php endif; ?>">
		<?php do_action( 'woocommerce_product_thumbnails' ); ?>
	</div>
</div>
