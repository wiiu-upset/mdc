<?php
/**
 * The template for displaying product category thumbnails within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product_cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $woocommerce_loop;
$classes = array( 'category-grid-item' );

if ( isset( $woocommerce_loop['hide_text'] ) && $woocommerce_loop['hide_text'] ) {
	$classes[] = 'hide-text';
}

$categories_columns = tm_robin_get_option( 'categories_columns' );

$classes[] = TM_Robin_Helper::get_grid_item_class( apply_filters( 'tm_robin_shop_categories_columns', array(
	'xl' => $categories_columns,
	'lg' => $categories_columns == 1 ? 1 : 3,
	'md' => $categories_columns == 1 ? 1 : 2,
	'sm' => 1,
) ) );

?>
<div <?php wc_product_cat_class( $classes, $category ); ?>>

	<?php

	if ( isset( $item_style ) && $item_style == 'black-overlay' ) {
		echo '<div class="category-link"><a href="' . get_term_link( $category, 'product_cat' ) . '">' . esc_html__( 'Shop now', 'tm-robin' ) . '</a></div>';
	}

	?>

	<div class="product-category-thumbnail">
		<?php

		/**
		 * woocommerce_before_subcategory hook.
		 *
		 * @hooked woocommerce_template_loop_category_link_open - 10
		 */
		do_action( 'woocommerce_before_subcategory', $category );
		/**
		 * woocommerce_before_subcategory_title hook.
		 *
		 * @hooked woocommerce_subcategory_thumbnail - 10
		 */
		do_action( 'woocommerce_before_subcategory_title', $category );

		/**
		 * woocommerce_after_subcategory hook.
		 *
		 * @hooked woocommerce_template_loop_category_link_close - 10
		 */
		do_action( 'woocommerce_after_subcategory', $category ); ?>

		<?php if ( isset( $layout ) && $layout == 'masonry' ) { ?>

			<div class="product-category-content">

				<?php

				/**
				 * woocommerce_shop_loop_subcategory_title hook.
				 *
				 * @hooked woocommerce_template_loop_category_title - 10
				 */
				do_action( 'woocommerce_shop_loop_subcategory_title', $category );

				/**
				 * woocommerce_after_subcategory_title hook.
				 */
				do_action( 'woocommerce_after_subcategory_title', $category );

				if ( isset( $item_style ) && $item_style == 'grey-border' ) {
					echo '<div class="category-link"><a href="' . get_term_link( $category, 'product_cat' ) . '">' . esc_html__( 'Shop now', 'tm-robin' ) . '</a></div>';
				}
				/**
				 * woocommerce_after_subcategory hook.
				 *
				 * @hooked woocommerce_template_loop_category_link_close - 10
				 */
				do_action( 'woocommerce_after_subcategory', $category ); ?>

			</div>
		<?php } ?>

	</div>

	<?php if ( ! isset( $layout ) || ( isset( $layout ) && $layout != 'masonry' ) ) { ?>
		<div class="product-category-content">

			<?php

			/**
			 * woocommerce_shop_loop_subcategory_title hook.
			 *
			 * @hooked woocommerce_template_loop_category_title - 10
			 */
			do_action( 'woocommerce_shop_loop_subcategory_title', $category );

			/**
			 * woocommerce_after_subcategory_title hook.
			 */
			do_action( 'woocommerce_after_subcategory_title', $category );

			if ( isset( $item_style ) && $item_style == 'grey-border' ) {
				echo '<div class="category-link"><a href="' . get_term_link( $category, 'product_cat' ) . '">' . esc_html__( 'Shop now', 'tm-robin' ) . '</a></div>';
			}
			/**
			 * woocommerce_after_subcategory hook.
			 *
			 * @hooked woocommerce_template_loop_category_link_close - 10
			 */
			do_action( 'woocommerce_after_subcategory', $category ); ?>
		</div>
	<?php } ?>
</div>
