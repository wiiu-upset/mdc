<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Robin
 */

global $tm_robin_options;

$tm_robin_options['sticky_header']     = false;
$tm_robin_options['topbar_on']         = false;
$tm_robin_options['page-header-style'] = 'hidden';
$tm_robin_options['search_on']         = false;
$tm_robin_options['wishlist_on']       = false;
$tm_robin_options['minicart_on']       = false;

get_header(); ?>

	<div id="main" class="site-content" role="main">

		<div class="area-404">
			<div class="container">
				<div class="area-404__content row flex-items-xs-middle">
					<div class="col-xs-12 col-xl-6 col-404">
						<h1 class="area-404__heading"><?php esc_html_e('Oops!', 'tm-robin'); ?></h1>
						<h1 class="area-404__sub-heading"><?php esc_html_e('Something is wrong', 'tm-robin'); ?></h1>
						<p class="area-404__content-heading"><?php esc_html_e( "The page you are looking for does not exist. Return to the home page. ", 'tm-robin' ); ?></p>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="button 404-button"><?php esc_html_e( 'Go home', 'tm-robin' ); ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php
get_footer();
