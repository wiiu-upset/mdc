<?php
$container_classes   = array( '' );
$container_classes[] = 'container ' . tm_robin_get_option( 'topbar_width' );

$topbar_bgcolor = tm_robin_get_option( 'topbar_bgcolor' );
$topbar_layout  = tm_robin_get_option( 'topbar_layout' );

if ( is_page() ) {
	$topbar_bgcolor = get_post_meta( TM_Robin_Helper::get_the_ID(), 'tm_robin_topbar_bgcolor' );
}

$topbar_left_classes = array( 'topbar-left col-xs-12' );

if ( $topbar_layout == '2' ) {
	$topbar_left_classes[] = 'col-lg-6';
} else {
	$topbar_left_classes[] = 'text-xs-center';
}

?>
<!-- Top bar -->
<div class="topbar<?php if ( $topbar_bgcolor == 'transparent' || empty( $topbar_bgcolor ) ) {
	echo esc_attr( ' topbar-transparent' );
} ?>">
	<div class="<?php echo implode( ' ', $container_classes ) ?>">
		<div class="row">
			<div class="<?php echo implode( ' ', $topbar_left_classes ); ?>">
				<?php
				if ($topbar_layout == '2') {
					echo TM_Robin_Templates::language_switcher();
					echo TM_Robin_Templates::currency_switcher();
				}
				?>
				<div class="topbar-text">
					<?php echo tm_robin_get_option( 'topbar_text' ); ?>
				</div>
			</div>
			<?php if ( $topbar_layout == '2' ) { ?>
				<div class="topbar-right col-xs-12 col-lg-6 hidden-md-down">

					<?php
					if ( tm_robin_get_option( 'topbar_social' ) ) {
						echo TM_Robin_Templates::social_links();
					}
					?>

					<?php

					if ( tm_robin_get_option( 'topbar_menu' ) ) {
						// Top bar menu.
						$args = array(
							'theme_location'  => 'top_bar',
							'menu_id'         => 'topbar-menu',
							'container_class' => 'topbar-menu',
							'fallback_cb'     => false,
							'walker'          => new TM_Robin_Walker_Nav_Menu(),
						);

						if ( is_user_logged_in() ) {
							$args['menu'] = tm_robin_get_option( 'topbar_logged_in_menu' );
						}

						wp_nav_menu( $args );
					}

					?>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
<!-- / End top bar -->
