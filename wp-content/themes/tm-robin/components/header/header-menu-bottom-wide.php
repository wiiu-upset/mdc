<?php

$pageID = TM_Robin_Helper::get_the_ID();

$left_widget_area  = $pageID ? get_post_meta( $pageID, 'tm_robin_header_left_sidebar', true ) : 'sidebar-header-left';
$right_widget_area = $pageID ? get_post_meta( $pageID, 'tm_robin_header_right_sidebar', true ) : 'sidebar-header-right';

if ( $left_widget_area == 'default' || ! $left_widget_area ) {
	$left_widget_area = 'sidebar-header-left';
}

if ( $right_widget_area == 'default' || ! $right_widget_area ) {
	$right_widget_area = 'sidebar-header-right';
}

?>
<div class="container wide">
	<div class="row">
		<div class="header-left">
			<?php if ( tm_robin_get_option( 'header_social' ) ) {
				echo TM_Robin_Templates::social_links();
			} ?>
			<div class="header-widget header-widget-left">
				<?php
				if ( $left_widget_area != 'no' ) {
					dynamic_sidebar( $left_widget_area );
				} ?>
			</div>
		</div>
		<?php echo TM_Robin_Templates::header_block_logo(); ?>
		<div class="header-right header-tools">
			<div class="header-widget header-widget-right">
				<?php
				if ( $right_widget_area != 'no' ) {
					dynamic_sidebar( $right_widget_area );
				} ?>
			</div>
			<?php echo TM_Robin_Templates::header_block_search(); ?>
			<?php echo TM_Robin_Templates::header_block_wishlist(); ?>
			<?php echo TM_Robin_Templates::header_block_cart(); ?>
			<?php echo TM_Robin_Templates::header_block_mobile_btn(); ?>
		</div>
	</div>
</div>
<div class="site-menu-wrap">
	<div class="container">
		<?php echo TM_Robin_Templates::header_block_site_menu(); ?>
	</div>
</div>
