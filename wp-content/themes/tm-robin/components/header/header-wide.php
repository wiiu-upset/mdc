<?php

$pageID            = TM_Robin_Helper::get_the_ID();
$right_widget_area = $pageID ? get_post_meta( $pageID, 'tm_robin_header_right_sidebar', true ) : 'sidebar-header-right';

if ( $right_widget_area == 'default' || ! $right_widget_area ) {
	$right_widget_area = 'sidebar-header-right';
}

?>
<div class="container wide">
	<div class="row">
		<?php echo TM_Robin_Templates::header_block_logo(); ?>
		<?php echo TM_Robin_Templates::header_block_site_menu(); ?>
		<div class="header-tools">
			<div class="header-widget">
				<?php
				if ( $right_widget_area != 'no' ) {
					dynamic_sidebar( $right_widget_area );
				}
				?>
			</div>
			<?php echo TM_Robin_Templates::header_block_search(); ?>
			<?php echo TM_Robin_Templates::header_block_wishlist(); ?>
			<?php echo TM_Robin_Templates::header_block_cart(); ?>
			<?php echo TM_Robin_Templates::header_block_mobile_btn(); ?>
		</div>
	</div>
</div>
