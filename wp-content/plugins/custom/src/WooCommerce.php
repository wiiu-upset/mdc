<?php

/**
 * @file
 * Contains \Mdc\Custom\WooCommerce.
 */

namespace Mdc\Custom;

/**
 * Main front-end functionality.
 */
class WooCommerce {

  /**
   * Set minimum order amount if product "cassetta" is not added to the cart.
   *
   * @implements woocommerce_checkout_process
   * @implements woocommerce_before_cart
   */
  public static function minimumOrderAmount() {
    $minimum = 20;
    $cassetta_product_id = 3337;
    $cassetta_product_cart_id = WC()->cart->generate_cart_id($cassetta_product_id);
    if (WC()->cart->total < $minimum && !WC()->cart->find_product_in_cart($cassetta_product_cart_id)) {
      if (is_cart()) {
        wc_print_notice('<strong>Attenzione: l\'importo minimo di spesa è di 20,00€.</strong>', 'error');
      }
      else {
        wc_add_notice('<strong>Attenzione: l\'importo minimo di spesa è di 20,00€.</strong>' , 'error');
      }
    }
  }

  /**
   * Sets default postalcode and city in order to show the lower shipping cost.
   *
   * @implements woocommerce_default_address_fields
   */
  public static function woocommerce_default_address_fields($address_fields) {
    $address_fields['postcode']['default'] = '80136';
    $address_fields['city']['default'] = 'Napoli';
    return $address_fields;
  }

  /**
   * Hides shipping on cart page.
   *
   * @implements woocommerce_cart_ready_to_calc_shipping
   */
  public static function woocommerce_cart_ready_to_calc_shipping($show_shipping) {
    if (is_cart()) {
      return FALSE;
    }
    return $show_shipping;
  }

}
