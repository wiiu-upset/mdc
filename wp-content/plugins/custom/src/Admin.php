<?php

/**
 * @file
 * Contains \Mdc\Custom\Admin.
 */

namespace Mdc\Custom;

/**
 * Administrative back-end functionality.
 */
class Admin {

  /**
   * @implements admin_init
   */
  public static function init() {
    add_action('admin_head', __CLASS__ . '::admin_head');
  }

  /**
   * Adds favicon on backend pages, including login one.
   *
   * @implements admin_head
   */
  public static function admin_head() {
    echo '<link rel="shortcut icon" href="' . get_stylesheet_directory_uri() . '/dist/images/favicon.png" />';
  }

}
