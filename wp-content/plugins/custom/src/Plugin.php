<?php

/**
 * @file
 * Contains \Mdc\Custom\Plugin.
 */

namespace Mdc\Custom;

/**
 * Main front-end functionality.
 */
class Plugin {

  /**
   * Prefix for naming.
   *
   * @var string
   */
  const PREFIX = 'custom';

  /**
   * Gettext localization domain.
   *
   * @var string
   */
  const L10N = self::PREFIX;

  /**
   * @var string
   */
  private static $baseUrl;

  /**
   * @implements init
   */
  public static function preInit() {
    // Remove WordPress promotion on /wp-login.php.
    add_action('login_enqueue_scripts', __CLASS__ . '::login_enqueue_scripts');
    add_action('login_head', __NAMESPACE__ . '\Admin::admin_head');
    add_filter('login_headerurl', 'site_url', 10, 0);
    add_filter('login_headertitle', __CLASS__ . '::login_headertitle');
  }

  /**
   * @implements init
   */
  public static function init() {
    if (is_admin()) {
      return;
    }

    add_action('woocommerce_checkout_process', __NAMESPACE__ . '\WooCommerce::minimumOrderAmount');
    add_action('woocommerce_before_cart', __NAMESPACE__ . '\WooCommerce::minimumOrderAmount');
    add_filter('woocommerce_default_address_fields', __NAMESPACE__ . '\WooCommerce::woocommerce_default_address_fields');
    add_filter('woocommerce_cart_ready_to_calc_shipping', __NAMESPACE__ . '\WooCommerce::woocommerce_cart_ready_to_calc_shipping');
  }

  /**
   * @implements login_enqueue_scripts
   */
  public static function login_enqueue_scripts() {
    if (file_exists(get_stylesheet_directory() . '/dist/styles/login.css')) {
      wp_enqueue_style('theme/login', get_stylesheet_directory_uri() . '/dist/styles/login.css');
    }
    else {
      $url = get_stylesheet_directory_uri() . '/dist/images/logo.png';
      echo <<<EOD
<style>
.login h1 a {
  width: 236px !important;
  height: 60px !important;
  background-image: url("$url") !important;
  background-size: 236px !important;
}
</style>

EOD;
    }
  }

  /**
   * @implements login_headertitle
   */
  public static function login_headertitle($login_header_title) {
    return get_bloginfo('name');
  }

  /**
   * Loads the plugin textdomain.
   */
  public static function loadTextdomain() {
    load_plugin_textdomain(static::L10N, FALSE, static::L10N . '/languages/');
  }

  /**
   * The base URL path to this plugin's folder.
   *
   * Uses plugins_url() instead of plugin_dir_url() to avoid a trailing slash.
   */
  public static function getBaseUrl() {
    if (!isset(static::$baseUrl)) {
      static::$baseUrl = plugins_url('', static::getBasePath() . '/custom.php');
    }
    return static::$baseUrl;
  }

  /**
   * The absolute filesystem base path of this plugin.
   *
   * @return string
   */
  public static function getBasePath() {
    return dirname(__DIR__);
  }

  /**
   * Renders a given plugin template, optionally overridden by the theme.
   *
   * WordPress offers no built-in function to allow plugins to render templates
   * with custom variables, respecting possibly existing theme template overrides.
   * Inspired by Drupal (5-7).
   *
   * @param array $template_subpathnames
   *   An prioritized list of template (sub)pathnames within the plugin/theme to
   *   discover; the first existing wins.
   * @param array $variables
   *   An associative array of template variables to provide to the template.
   *
   * @throws \InvalidArgumentException
   *   If none of the $template_subpathnames files exist in the plugin itself.
   */
  public static function renderTemplate(array $template_subpathnames, array $variables = []) {
    $template_pathname = locate_template($template_subpathnames, FALSE, FALSE);
    extract($variables, EXTR_SKIP | EXTR_REFS);
    if ($template_pathname !== '') {
      include $template_pathname;
    }
    else {
      while ($template_pathname = current($template_subpathnames)) {
        if (file_exists($template_pathname = static::getBasePath() . '/' . $template_pathname)) {
          include $template_pathname;
          return;
        }
        next($template_subpathnames);
      }
      throw new \InvalidArgumentException("Missing template '$template_pathname'");
    }
  }

}
