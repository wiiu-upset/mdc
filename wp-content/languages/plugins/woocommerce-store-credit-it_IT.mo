��          �      |      �     �     �       ,   '     T  "   b     �     �     �  
   �     �     �  '   �             ;   .  ,   j  C   �  .   �  5   
     @  3  _     �     �     �  ?   �     #  -   3     a     q     �     �     �     �  :   �            7   )  9   a  Q   �  ;   �  3   )	  .   ]	                                   
       	                                                       Credit Amount Default coupon options Delete after use Display store credit on the My Account page. Email Address Generate coupon and email customer Individual use Invalid amount. Invalid email address. My Account Send Store Credit Store Credit Store Credit coupons applied before tax Store credit sent. Store credit: The following options are specific to store credit coupons. There is no credit remaining on this coupon. To redeem your store credit use the following code during checkout: When the credit is used up, delete the coupon. You do not have any store credit on your account yet. You have been given %s credit  Project-Id-Version: mailchimp-for-wp
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=2; plural=(n != 1);
POT-Creation-Date: 
PO-Revision-Date: 
Language-Team: 
X-Generator: Poedit 2.0.6
Last-Translator: 
Language: it
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Ammontare del buono regalo Opzioni coupon predefinite Elimina dopo l’utilizzo Visualizza il tuo buono regalo sulla pagina «Il mio account». Indirizzo email Genera buono regalo ed invia email al cliente Uso individuale Credito non valido. Indirizzo email non valido. Il mio account Buoni regalo Buono regalo Applica il buono regalo prima che siano calcolate le tasse Buono regalo inviato. Buono regalo: Le seguenti opzioni sono specifiche per i buoni regalo. Non c’è più credito disponibile sul tuo buono regalo. Per riscattare il buono regalo, utilizza il seguente codice in fase di check out: Quando il credito è esaurito, elimina questo buono regalo. Non hai ancora nessun buono regalo sul tuo account. Ti è stato accreditato un buono regalo di %s  